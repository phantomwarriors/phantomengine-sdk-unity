Shader "Hidden/RotateTexture" {

    Properties{
        _MainTex("Texture", 2D) = "white" {}
        _ScaleFactor("Scaling Factor", Vector) = (1.0, 1.0, 0, 0)
    }

    SubShader{

        Pass {

            CGPROGRAM
            #pragma vertex vert
            #pragma fragment frag

            struct appdata {
                float4 vertex : POSITION;
                float2 uv : TEXCOORD0;
            };

            struct v2f {
                float4 vertex : SV_POSITION;
                float2 uv : TEXCOORD0;
            };

            sampler2D_float _MainTex;
            uniform float2 _ScaleFactor;

            v2f vert(appdata v) {
                v2f o;
                o.vertex = UnityObjectToClipPos(v.vertex);
                o.uv = v.uv;

                return o;
            }

            fixed4 frag(v2f i) : SV_Target {
                fixed2 scaledIndex = ((fixed2(i.uv.y, 1.0 - i.uv.x) - 0.5) * _ScaleFactor.yx) + 0.5;
                return tex2D(_MainTex, scaledIndex);
            }

            ENDCG
        }
    }
}