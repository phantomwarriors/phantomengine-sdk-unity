Shader "Hidden/CameraDepth" {

    SubShader{

        Cull Off ZWrite Off ZTest Always

        Pass {

            CGPROGRAM

            #pragma vertex vert
            #pragma fragment frag

            #include "UnityCG.cginc"

            struct appdata {
                float4 vertex : POSITION;
                float2 uv : TEXCOORD0;
            };

            struct v2f {
                float2 uv      : TEXCOORD0;
                float4 viewDir : TEXCOORD1;
                float4 vertex  : SV_POSITION;
            };

            v2f vert(appdata v) {
                v2f o;
                o.uv = v.uv;
                o.viewDir = mul(unity_CameraInvProjection, float4((float2(1.0 - o.uv.y, o.uv.x) * 2.0) - 1.0, 1.0, 1.0));
                o.vertex = UnityObjectToClipPos(v.vertex);
                return o;
            }

            uniform sampler2D_float _CameraDepthTexture;

            float frag(v2f i) : SV_Target {

                // Get the depth of the pixel that would be rendered.
                const float planarDepth = Linear01Depth(tex2D(_CameraDepthTexture, float2(1.0 - i.uv.y, i.uv.x)));

                /* Courtesy of "Namey5": https://forum.unity.com/threads/converting-depth-values-to-distances-from-z-buffer.921929/ */

                // Find the view-space position of the current pixel by multiplying viewDir by depth
                const float3 sphericalDepth = (i.viewDir.xyz / i.viewDir.w) * planarDepth;

                // Return length of sphericalDepth is the raw distance to the camera
                return length(sphericalDepth);
            }

            ENDCG
        }
    }
}