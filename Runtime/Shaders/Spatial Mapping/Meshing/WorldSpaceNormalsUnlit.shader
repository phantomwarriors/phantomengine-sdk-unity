Shader "Unlit/WorldSpaceNormalsUnlit" {

    Properties {
        _MainTex("Main Texture", Cube) = "" {}
    }

    SubShader {

        Cull Off
        ZWrite On

        Pass {

            CGPROGRAM

            #pragma vertex vert
            #pragma fragment frag

            #include "UnityCG.cginc"

            struct v2f {
                half3 worldNormal : TEXCOORD0;
                float4 pos : SV_POSITION;
            };

            samplerCUBE_half _MainTex;

            v2f vert (float4 vertex : POSITION, float4 normal : NORMAL) {

                v2f o;
                o.pos         = UnityObjectToClipPos(vertex);
                o.worldNormal = UnityObjectToWorldNormal(normal);

                // Flips the normal for cracked (inverted) faces.
                o.worldNormal *= dot(o.worldNormal, WorldSpaceViewDir(vertex));

                return o;
            }
            
            fixed4 frag(v2f i) : SV_Target{
                return texCUBE(_MainTex, i.worldNormal);
            }

            ENDCG
        }
    }
}