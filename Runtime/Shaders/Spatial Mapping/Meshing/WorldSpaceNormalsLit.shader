Shader "Unlit/WorldSpaceNormalsLit" {

    Properties {
        _MainTex("Main Texture", Cube) = "" {}
        _ShadowStrength("Shadow Strength", Range(0.0, 1.0)) = 0.5
    }

    SubShader {

        Cull Off
        ZWrite On
        
        Pass {

            Tags { 
                "Queue"      = "Geometry"
                "RenderType" = "Opaque"
                "LightMode"  = "ForwardBase" }

            CGPROGRAM

            #pragma vertex vert
            #pragma fragment frag

            #pragma multi_compile_fog
            #pragma multi_compile_fwdadd_fullshadows

            #include "UnityCG.cginc"
            #include "AutoLight.cginc"

            struct v2f {
                half3 worldNormal : TEXCOORD0;
                SHADOW_COORDS(1)
                UNITY_FOG_COORDS(2)
                float4 pos : SV_POSITION; 
            };

            samplerCUBE_half _MainTex;
            float _ShadowStrength;

            v2f vert (float4 vertex : POSITION, float4 normal : NORMAL) {

                v2f o;
                o.pos         = UnityObjectToClipPos(vertex);
                o.worldNormal = UnityObjectToWorldNormal(normal);

                // Flips the normal for cracked (inverted) faces.
                o.worldNormal *= dot(o.worldNormal, WorldSpaceViewDir(vertex));

                UNITY_TRANSFER_FOG(o, o.pos);

                //Only works with Vulcan, which itself causes editor to lag behind 
                //on inputs - Neepo
                //TRANSFER_SHADOW(o)

                return o;
            }
            
            fixed4 frag(v2f i) : SV_Target {

                fixed4 col = texCUBE(_MainTex, i.worldNormal) * (1.0 - ((1.0 - SHADOW_ATTENUATION(i)) * _ShadowStrength));

                UNITY_APPLY_FOG(i.fogCoord, col);

                return col;
            }

            ENDCG
        }
    }

Fallback "Diffuse"

}