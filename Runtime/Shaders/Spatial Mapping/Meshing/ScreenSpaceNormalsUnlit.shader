Shader "Unlit/ScreenSpaceNormalsUnlit" {

    Properties {
        _MainTex("Main Texture", Cube) = "" {}
    }

    SubShader {

        Cull Off
        ZWrite On

        Pass {

            CGPROGRAM

            #pragma vertex vert
            #pragma fragment frag

            #include "UnityCG.cginc"

            struct v2f {
                half3 screenNormal : TEXCOORD0;
                float4 pos : SV_POSITION;
            };

            samplerCUBE_half _MainTex;

            v2f vert (float4 vertex : POSITION, float4 normal : NORMAL) {

                v2f o;
                o.pos = UnityObjectToClipPos(vertex);

                float3 worldNormal = UnityObjectToWorldNormal(normal);

                o.screenNormal = mul((float3x3)UNITY_MATRIX_V, worldNormal); // WorldSpaceViewDir(vertex);
                o.screenNormal *= dot(worldNormal, WorldSpaceViewDir(vertex));

                return o;
            }
            
            fixed4 frag(v2f i) : SV_Target{
                return texCUBE(_MainTex, i.screenNormal);
            }

            ENDCG
        }
    }
}