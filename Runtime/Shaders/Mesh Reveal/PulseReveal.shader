Shader "Hidden/PulseReveal" {
    
    Properties {
        _Texture ("Texture", Cube) = "white" {}
        _Glossiness ("Smoothness", Range(0,1)) = 0.5
        _Metallic ("Metallic", Range(0,1)) = 0.0
        _Lightness("Lightness", Range(0,1)) = 0.0
        _Opacity("Opacity", Range(0,1)) = 0.0
        _PulseSpeed("Pulse Speed", float) = 0.0
        _PulseSize("Pulse Size", float) = 0.0
        _Exponent("Exponent", Range(0.5,2)) = 2.0
    }
    
    SubShader {
        Tags { "RenderType"="Transparent" "RenderQueue" = "Transparent" }
        LOD 200

        CGPROGRAM
        // Physically based Standard lighting model, and enable shadows on all light types
        #pragma surface surf Standard alpha:fade

        // Use shader model 3.0 target, to get nicer looking lighting
        #pragma target 3.0

        samplerCUBE_float _Texture;

        struct Input {
            float2 uv_Texture;
            float3 worldPos;
            float3 worldNormal;
        };

        half _Glossiness;
        half _Metallic;
        float4 _EmissionPoint;
        float _TimeSincePulse;
        float _Lightness;
        float _Opacity;
        float _PulseSpeed;
        float _PulseSize;
        float _Exponent;

        // Add instancing support for this shader. You need to check 'Enable Instancing' on materials that use the shader.
        // See https://docs.unity3d.com/Manual/GPUInstancing.html for more information about instancing.
        // #pragma instancing_options assumeuniformscaling
        UNITY_INSTANCING_BUFFER_START(Props)
            // put more per-instance properties here
        UNITY_INSTANCING_BUFFER_END(Props)

        void surf (Input IN, inout SurfaceOutputStandard o) {
            fixed4 c = texCUBE(_Texture, IN.worldNormal) * _Lightness;
            
            o.Albedo = c.rgb;
            o.Metallic = _Metallic;
            o.Smoothness = _Glossiness;
            
            const float distanceFromEmission = distance(IN.worldPos, _EmissionPoint);
            o.Alpha = 
                sin(
                    clamp(
                        (pow(_TimeSincePulse * _PulseSpeed, _Exponent) - distanceFromEmission / _PulseSize) * (UNITY_PI / 180),
                        0,
                        UNITY_PI
                    )
                ) * _Opacity;
        }
        ENDCG
    }
    FallBack "Diffuse"
}
