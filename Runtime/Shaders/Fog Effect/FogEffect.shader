Shader "Hidden/PhantomTech/FogEffect" {

    Properties {
        _MainTex ("Texture", 2D) = "white" {}
        _FogStart ("Start Distance", Float) = 1
        _FogEnd ("End Distance", Float) = 5
        _Colour ("Colour", COLOR) = (1, 1, 1, 1)
        _Opacity ("Opacity", Float) = 1
    }

    SubShader {
        ZWrite Off ZTest Always

        Pass {

            CGPROGRAM
            #pragma vertex vert
            #pragma fragment frag

            #include "UnityCG.cginc"

            struct appdata {
                float4 vertex : POSITION;
                float2 uv : TEXCOORD0;
            };

            struct v2f {
                float2 uv : TEXCOORD0;
                float4 vertex : SV_POSITION;
            };

            v2f vert (appdata v) {
                v2f o;
                o.vertex = UnityObjectToClipPos(v.vertex);
                o.uv = v.uv;

                return o;
            }

            sampler2D _MainTex;
            uniform sampler2D _HybridDepth;

            float _FogStart;
            float _FogEnd;
            float4 _Colour;
            float _Opacity;

            fixed4 frag (v2f i) : SV_Target {

                float fogRange = _FogEnd - _FogStart;

                float fogVal = 1 - tex2D(_HybridDepth, i.uv).w;
                fogVal *= (_ProjectionParams.z / fogRange);
                fogVal = saturate(fogVal - (_FogStart / fogRange));
                fogVal *= fogVal;
                fogVal *= _Opacity;

                fixed4 col = (tex2D(_MainTex, i.uv) * (1 - fogVal)) + (_Colour * fogVal);
                
                return col;
            }
            ENDCG
        }
    }
}
