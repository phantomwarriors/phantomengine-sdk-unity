Shader "Hidden/PhantomTech/UnifyDepth" {

    Properties{
        _MainTex("Texture", 2D) = "white" {}
        _WorldDepth("World Depth", 2D) = "black" {}
    }

    SubShader {
        ZTest Always

        Pass {
            CGPROGRAM
            #pragma vertex vert
            #pragma fragment frag

            #include "UnityCG.cginc"

            struct appdata {
                float4 vertex : POSITION;
                float2 uv : TEXCOORD0;
            };

            struct v2f {
                float2 uv : TEXCOORD0;
                float4 viewDir : TEXCOORD1;
                float4 vertex : SV_POSITION;
            };

            v2f vert (appdata v) {
                v2f o;
                o.uv = v.uv;
                o.vertex = UnityObjectToClipPos(v.vertex);
                o.viewDir = mul(unity_CameraInvProjection, float4(o.uv * 2.0 - 1.0, 1.0, 1.0));

                return o;
            }

            sampler2D _WorldDepth;
            sampler2D _CameraDepthTexture;
            
            float4 frag (v2f i) : SV_Target {

                float4 col = float4(0, 0, 0, 0);
                
                // Get the depth of the pixel that would be rendered.
                const half planarDepth = Linear01Depth(tex2D(_CameraDepthTexture, i.uv).x);

                /* Courtesy of "Namey5": https://forum.unity.com/threads/converting-depth-values-to-distances-from-z-buffer.921929/ */

                // Find the view-space position of the current pixel by multiplying viewDir by depth
                const half3 sphericalDepth = (i.viewDir.xyz / i.viewDir.w) * planarDepth;

                col.x = length(sphericalDepth);
                
                //Real World Depth Map
                col.y = tex2D(_WorldDepth, i.uv).x;
                
                //Merged Depth Maps
                col.z = min(col.x, col.y);

                //Blended Merged Maps
                col.w = smoothstep(0, 0.02, col.x - col.y);

                return col;
            }
            ENDCG
        }
    }
}
