Shader "Hidden/PhantomTech/Occlusion" {

	Properties {
		_MainTex("Texture", 2D) = "white" {}
        _CameraFeed("Camera Feed", 2D) = "white" {}
        _Blend("Blend Value", Float) = 0.02
	}

	SubShader {

        ZTest Always

		Pass {

			CGPROGRAM
			#pragma vertex vert
			#pragma fragment frag

			#include "UnityCG.cginc"

			struct appdata {
				float4 vertex : POSITION;
				float2 uv : TEXCOORD0;
			};

			struct v2f {
				float2 uv : TEXCOORD0;
				float4 vertex : SV_POSITION;
			};

			v2f vert(appdata v) {
				v2f o;
				o.vertex = UnityObjectToClipPos(v.vertex);
				o.uv = v.uv;

				return o;
			}

			sampler2D _MainTex;
			sampler2D _CameraFeed;
            float _Blend;

            uniform sampler2D _HybridDepth;

			fixed4 frag(v2f i) : SV_Target {

                float4 dSample = tex2D(_MainTex, i.uv);
                float4 wSample = tex2D(_CameraFeed, i.uv);
                float4 depthValues = tex2D(_HybridDepth, i.uv);

				/*If world depth value is less than digital depth value, draw world sample. blend if the difference is between 0 and _Blend value.
				Otherwise default to drawing digital camera sample */
                float4 col = lerp(dSample, wSample, smoothstep(0, _Blend, depthValues.x - depthValues.y));

                return col;
			}

			ENDCG
		}
	}
}