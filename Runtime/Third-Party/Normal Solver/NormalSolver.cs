/* 
 * The following code was taken from: http://schemingdeveloper.com
 *
 * Visit our game studio website: http://stopthegnomes.com
 *
 * License: You may use this code however you see fit, as long as you include this notice
 *          without any modifications.
 *
 *          You may not publish a paid asset on Unity store if its main function is based on
 *          the following code, but you may publish a paid asset that uses this code.
 *
 *          If you intend to use this in a Unity store asset or a commercial project, it would
 *          be appreciated, but not required, if you let me know with a link to the asset. If I
 *          don't get back to you just go ahead and use it anyway!
 */

using PhantomTech;

using System.Collections.Generic;
using System.Runtime.CompilerServices;

namespace StopTheGnomes { 

    public static class NormalSolver {

        /* NOTE (Loui @ PhantomTech): Modified and optimised code by removing vector normalisation functionality and angle discrimination.
         * Changed FNV system to work with Vector3Ds and eliminated an unneccesary casting operation. */

        /// <summary>
        /// Recalculate the normals of a mesh based on an angle threshold. This takes into account distinct vertices that have the same position.
        /// </summary>
        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        public static void RecalculateNormals(PhantomMesh _mesh) {

            var vertices   = _mesh.m_Vertices;
            var triangles  = _mesh.m_Triangles;

            var triNormals = new Vector3D[triangles.Length / 3];
            var normals    = new Vector3D[vertices.Length];

            // Holds the normal of each triangle in each sub mesh.
            var dictionary = new Dictionary<VertexKey, List<VertexEntry>>(vertices.Length);
            
            for (var i = 0; i < triangles.Length; i += 3) {

                int i3 = triangles[i + 2];
                int i2 = triangles[i + 1];
                int i1 = triangles[  i  ];

                int triIndex = i / 3;

                // Calculate the normal of the triangle.
                triNormals[triIndex] = PhantomTech.Math.Cross(
                    vertices[i3] - vertices[i1],
                    vertices[i2] - vertices[i1]
                );
                
                VertexKey key;
                List<VertexEntry> entry;

                if (!dictionary.TryGetValue(key = new VertexKey(vertices[i3]), out entry)) {
                    entry = new List<VertexEntry>(4);
                    dictionary.Add(key, entry);
                }
                entry.Add(new VertexEntry(triIndex, i3));

                if (!dictionary.TryGetValue(key = new VertexKey(vertices[i2]), out entry)) {
                    entry = new List<VertexEntry>(4);
                    dictionary.Add(key, entry);
                }
                entry.Add(new VertexEntry(triIndex, i2));

                if (!dictionary.TryGetValue(key = new VertexKey(vertices[i1]), out entry)) {
                    entry = new List<VertexEntry>(4);
                    dictionary.Add(key, entry);
                }
                entry.Add(new VertexEntry(triIndex, i1));
            }

            // Each entry in the dictionary represents a unique vertex position.
            foreach (var vertList in dictionary.Values) {

                for (var i = 0; i < vertList.Count; ++i) {
                    for (var j = 0; j < vertList.Count; ++j) {
                        normals[vertList[i].VertexIndex] -= triNormals[vertList[j].TriangleIndex];
                    }
                }
            }

            _mesh.SetNormals(normals);
        }

        private struct VertexKey {

            private readonly long _x;
            private readonly long _y;
            private readonly long _z;

            // Change this if you require a different precision.
            private const float Tolerance = 100000;

            // Magic FNV values. Do not change these.
            private const long FNV32Init  = 0x811c9dc5;
            private const long FNV32Prime = 0x01000193;

            public VertexKey(Vector3D position) {
                _x = (long)(position.x * Tolerance);
                _y = (long)(position.y * Tolerance);
                _z = (long)(position.z * Tolerance);
            }

            public override bool Equals(object obj) {
                var key = (VertexKey)obj;

                return _x == key._x && _y == key._y && _z == key._z;
            }

            public override int GetHashCode() {

                long rv = FNV32Init;

                rv ^= _x;
                rv *= FNV32Prime;
                rv ^= _y;
                rv *= FNV32Prime;
                rv ^= _z;
                rv *= FNV32Prime;

                return rv.GetHashCode();
            }
        }

        private struct VertexEntry {

            public int TriangleIndex;
            public int   VertexIndex;

            public VertexEntry(int triIndex, int vertIndex) {
                TriangleIndex =  triIndex;
                  VertexIndex = vertIndex;
            }
        }
    }
}