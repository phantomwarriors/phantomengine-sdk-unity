﻿using System.Collections;
using System.Collections.Generic;

using UnityEngine;
using UnityEngine.Rendering;

using Unity.Jobs;
using Unity.Collections;

using System;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using System.Collections.Concurrent;
using System.Runtime.CompilerServices;

using AmazingAssets.WireframeShader;

using StopTheGnomes;

using static PhantomTech.PhantomSettings;

namespace PhantomTech.Spatial {

    /// <summary>
    /// Script handling the meshing of spatial data stored within the OctreeManager during runtime.
    /// </summary>
    public sealed class Meshing : MonoBehaviour {

        public static Meshing Instance;

        /// <summary>
        /// Dictionary containing all meshes spawned in scene and their positions.
        /// </summary>
        [HideInInspector] public ConcurrentDictionary<Vector3D, MeshFilter> m_Meshes = new ConcurrentDictionary<Vector3D, MeshFilter>();

        /// <summary>
        /// Dictionary containing colliders for all meshes in the scene, indexed by position.
        /// </summary>
        [HideInInspector] public Dictionary<Vector3D, MeshCollider> m_Colliders = new Dictionary<Vector3D, MeshCollider>();

        /// <summary>
        /// Pool of meshes currently being generated.
        /// </summary>
        private ConcurrentDictionary<Vector3D, PhantomMesh> m_MeshPool = new ConcurrentDictionary<Vector3D, PhantomMesh>();

        private ConcurrentBag<Octree> m_ChangedOctrees = new ConcurrentBag<Octree>();

        private bool m_IsGenerating = false; // Has the generation process finished?
        private bool m_EventRaised  = true;

        private float m_GenerateStartTime = 0.0f;

        private ParticleSystem m_ScanningParticles;

        /* EVENTS */
        public static MeshGeneratedHandler MeshGenerated;
        public delegate void MeshGeneratedHandler(MeshGeneratedArgs _args);

        public class MeshGeneratedArgs : EventArgs {

            public MeshGeneratedArgs(ConcurrentBag<Octree> _changedOctrees) {
                changedOctrees = _changedOctrees;
            }

            public ConcurrentBag<Octree> changedOctrees { get; private set; }
        }

        private void Awake() {
            Scanning.FrameScanned += OnFrameScanned;

            var particlesPrefab = PhantomSettings.Instance.m_ScanningSettings.m_MeshGeneration.m_ScanningParticles;

            if (particlesPrefab != null) { 
                m_ScanningParticles = GameObject.Instantiate(particlesPrefab).GetComponent<ParticleSystem>();
                m_ScanningParticles.transform.parent = transform;
            }

            Instance = this;
        }

        private void OnFrameScanned(Scanning.FrameScannedArgs _args) {

            if (enabled) {

                if (_args != null) {

                    if (Time.time - m_GenerateStartTime >= 10.0f) {

                        PhantomTech.Diagnostics.Debug.Log("Generating operation timeout. Resetting...");

                        StopAllCoroutines();

                        m_IsGenerating = false;
                    }

                    if (m_IsGenerating == false) {

                        // Do threading in coroutine so we can safely wait for threads to complete without hanging the main thread.
                        m_GenerateStartTime = Time.time;

                        StartCoroutine(Generate());
                    }
                }
            }
        }

        private void Update() {

            if (m_EventRaised == false) {
                m_EventRaised = true;

                MeshGenerated?.Invoke(new MeshGeneratedArgs(m_ChangedOctrees));
            }
        }

        private void OnDestroy() {
            Dispose();
            Destroy(gameObject);
        }

        private IEnumerator Generate() {

            m_IsGenerating = true;

            lock (m_ChangedOctrees) {
                m_ChangedOctrees = new ConcurrentBag<Octree>();
            }

            // Get references to all octrees in the scene during this instant where the octree's difference is above the threshold for remeshing.
            var octrees = OctreeManager.Instance.m_Octrees.Values.Where(octree => octree.m_Difference >= SpatialMappingSettings.m_MeshGeneration.m_CellDeltaTolerance).ToArray();

            if (octrees.Length != 0) {

                if (m_ScanningParticles != null) {

                    var particlePoints = new List<Vector3D>();

                    foreach (var item in octrees) {
                        particlePoints.AddRange(item.GetVoxels().Select((node) => node.m_Position));
                    }

                    if (particlePoints.Count != 0) {
                        
                        const MeshUpdateFlags ignoreValidation = MeshUpdateFlags.DontNotifyMeshUsers | MeshUpdateFlags.DontValidateIndices | MeshUpdateFlags.DontRecalculateBounds | MeshUpdateFlags.DontResetBoneBounds;

                        var mesh = new Mesh();

                        /* Assign Vertices */
                        mesh.SetVertexBufferParams(particlePoints.Count(), new VertexAttributeDescriptor(VertexAttribute.Position, VertexAttributeFormat.Float32, 3));
                        mesh.SetVertexBufferData(particlePoints, 0, 0, particlePoints.Count(), 0, ignoreValidation);

                        var sh = m_ScanningParticles.shape;
                        sh.shapeType = ParticleSystemShapeType.Mesh;
                        sh.mesh = mesh;

                        float factor = PhantomTech.Math.ClampMax((float)m_ScanningParticles.main.maxParticles / particlePoints.Count(), 1.0f);

                        m_ScanningParticles.Emit((int)(particlePoints.Count() * factor));
                    }
                }

                var sw = new System.Diagnostics.Stopwatch();
                sw.Start();

                Task t = Task.Factory.StartNew(() => {

                    Parallel.ForEach(octrees, new ParallelOptions() { MaxDegreeOfParallelism = 3 }, octree => {

                        Thread.CurrentThread.Priority = System.Threading.ThreadPriority.Highest;

                        MeshJob(octree);
                    });
                });

                yield return new WaitUntil(() => t.IsCompleted && Time.timeScale != 0.0f);

                PhantomTech.Diagnostics.Debug.Log("Marching Cubes (3 Threads): " + sw.ElapsedMilliseconds + "ms");

                sw.Reset();
                sw.Start();

                // Spawn the meshes generated by the meshing threads (using coroutines to spread the workload over multiple frames):
                bool lastIteration = false,
                     doIncremental = SpatialMappingSettings.m_MeshGeneration.m_UseIncrementalMeshing && m_MeshPool.Count >= SpatialMappingSettings.m_MeshGeneration.m_MinIncrementalWorkload;

                int meshIncrement = doIncremental == false ? 
                    m_MeshPool.Count : 
                    PhantomTech.Math.ClampMin(PhantomTech.Math.Ceiling(m_MeshPool.Count, SpatialMappingSettings.m_MeshGeneration.m_IncrementalMeshingIterations), 1);

                for (int i = 0; i < m_MeshPool.Count; i += meshIncrement) {

                    if (i + meshIncrement >= m_MeshPool.Count) {
                        meshIncrement = m_MeshPool.Count - i;

                        lastIteration = true;
                    }

                    for (int j = 0; j != meshIncrement; j++) {
                        var kvp = m_MeshPool.ElementAt(i + j);

                        SpawnMesh(kvp.Value, kvp.Key);
                    }

                    if (doIncremental == true && 
                        lastIteration == false) {
                        yield return null;
                    }
                }

                sw.Stop();

                PhantomTech.Diagnostics.Debug.Log("Spawning Meshes: " + sw.ElapsedMilliseconds + "ms");
            }

            m_MeshPool.Clear();

            yield return new WaitForSecondsRealtime(1.0f / SpatialMappingSettings.m_MeshGeneration.m_MeshGenerationFPS);

            m_EventRaised  = false;
            m_IsGenerating = false;
        }

        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        private void MeshJob(Octree _octree) {

            lock (_octree) { 
            
                m_ChangedOctrees.Add(_octree);

                // Reset the _octree's difference value.
                _octree.m_Difference = 0;

                // Get a pooled instance of the marching cubes algorithm.
                var marchingCubes = new MarchingCubes() {
                    m_UseColor         = SpatialMappingSettings.m_MeshGeneration.m_VertexColors,
                    m_UseInterpolation = SpatialMappingSettings.m_MeshGeneration.m_Interpolation,
                    m_IsoLevel         = 0.5f
                };

                PhantomMesh mesh;

                if (marchingCubes.TryGenerate(_octree, out mesh)) {

                    if (SpatialMappingSettings.m_MeshGeneration.m_SmoothShading) {
                        NormalSolver.RecalculateNormals(mesh);
                    }
                    else {
                        mesh.CalculateNormals(false);
                    }

                    /* Calculate bounds: */
                    if (SpatialMappingSettings.m_MeshGeneration.m_FastBounds == false) {
                        mesh.CalculateBounds();
                    }
                    else {
                        float halfSize = _octree.m_Size * 0.5f;

                        mesh.SetBounds(new Vector3D(halfSize, halfSize, halfSize));
                    }
                }
                else {
                    mesh = new PhantomMesh();
                }

                // Assign the mesh to a pool of meshes to be spawned.
                m_MeshPool.TryAdd(_octree.m_Position, mesh);
            }
        }

        /// <summary>
        /// Spawns a PhantomMesh in the scene at the provided position.
        /// </summary>
        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        public void SpawnMesh(PhantomMesh _mesh, Vector3D _position) {

            if (_mesh.m_Vertices.Length != 0 && _mesh.m_Triangles.Length != 0) {

                /* Has to be called from Unity thread... */

                Mesh meshOutput;

                MeshFilter   meshFilter;
                MeshCollider meshCollider = null;

                if (m_Meshes.TryGetValue(_position, out meshFilter)) {
                    meshOutput = meshFilter.mesh;

                    if (SpatialMappingSettings.m_MeshGeneration.m_GenerateColliders) {
                        meshCollider = m_Colliders[_position];
                    }
                }
                else {

                    /* Spawn Object */
                    var outputObj = new GameObject(_mesh.m_Name);
                    outputObj.transform.parent = transform;
                    outputObj.layer = LayerMask.NameToLayer(SpatialMappingSettings.m_MeshGeneration.m_Layer);
                    outputObj.tag = SpatialMappingSettings.m_MeshGeneration.m_Tag;

                    var meshRenderer = outputObj.AddComponent<MeshRenderer>();
                    meshRenderer.sharedMaterials = SpatialMappingSettings.m_MeshGeneration.m_Materials;

#if UNITY_EDITOR || UNITY_STANDALONE
                    DisableComplexRendering(meshRenderer, ShadowCastingMode.TwoSided, true);
#else
                    DisableComplexRendering(meshRenderer, ShadowCastingMode.Off, false);
#endif

                    meshOutput = new Mesh();
                    meshOutput.MarkDynamic();

                    meshFilter = outputObj.AddComponent<MeshFilter>();
                    meshFilter.mesh = meshOutput;

                    /* Do Bounds */
                    meshOutput.bounds = new Bounds() {
                        center  = _position,
                        extents = _mesh.m_Bounds, 
                        max     = _position + _mesh.m_Bounds,
                        min     = _position - _mesh.m_Bounds,
                        size    = _mesh.m_Bounds * 2 
                    };

                    /* Add to mesh pool */
                    m_Meshes.TryAdd(_position, meshFilter);

                    if (SpatialMappingSettings.m_MeshGeneration.m_GenerateColliders) {

                        /* Add to collider pool */
                        meshCollider = outputObj.AddComponent<MeshCollider>();

                        m_Colliders.Add(_position, meshCollider);
                    }
                }

                if (meshCollider != null) {
                    meshCollider.enabled = false;
                }

                const MeshUpdateFlags ignoreValidation = MeshUpdateFlags.DontNotifyMeshUsers | MeshUpdateFlags.DontValidateIndices | MeshUpdateFlags.DontRecalculateBounds | MeshUpdateFlags.DontResetBoneBounds;

                var vertexParams = new[] {
                    new VertexAttributeDescriptor(VertexAttribute.Position, VertexAttributeFormat.Float16, 4), //Float16, 4
                    new VertexAttributeDescriptor(VertexAttribute.Normal,   VertexAttributeFormat.Float16, 4)  //Float16, 4
                };

                var vertexData = new PhantomMesh.VertexData[_mesh.m_Vertices.Length];
                for (int i = 0; i != vertexData.Length; ++i) {
                    vertexData[i].point  = new PhantomMesh.VertexData.Vector3DHalf(_mesh.m_Vertices[i]); //new PhantomMesh.VertexData.Vector3DHalf
                    vertexData[i].normal = new PhantomMesh.VertexData.Vector3DHalf(_mesh.m_Normals[i]);  //new PhantomMesh.VertexData.Vector3DHalf
                }

                /* Assign Vertices */
                meshOutput.SetVertexBufferParams(vertexData.Length, vertexParams);
                meshOutput.SetVertexBufferData(vertexData, 0, 0, vertexData.Length, 0, ignoreValidation);

                /* Assign Triangles */
                meshOutput.SetIndexBufferParams(_mesh.m_Triangles.Length, IndexFormat.UInt16); // UInt16
                meshOutput.SetIndexBufferData(_mesh.m_Triangles, 0, 0, _mesh.m_Triangles.Length, ignoreValidation);

                var subMeshDescriptor = new SubMeshDescriptor {
                    indexStart  = 0,
                    indexCount  = _mesh.m_Triangles.Length,
                    baseVertex  = 0,
                    vertexCount = _mesh.m_Vertices.Length,
                    firstVertex = 0,
                    topology = MeshTopology.Triangles
                };

                meshOutput.SetSubMesh(0, subMeshDescriptor, ignoreValidation);

                meshOutput.GenerateWireframeMesh(false, true);
                meshFilter.mesh = meshOutput;

                /* Assign Collider */
                if (SpatialMappingSettings.m_MeshGeneration.m_GenerateColliders) {
                    meshCollider.sharedMesh = meshFilter.mesh;
                    meshCollider.enabled = true;
                }
            }
            else {
                if (m_Meshes.TryRemove(_position, out var meshFilter)) {

                    if (m_Colliders.ContainsKey(_position)) {
                        m_Colliders.Remove(_position);
                    }

                    Destroy(meshFilter.gameObject);
                }
            }
        }

        /// <summary>
        /// Disables many expensive rendering features that are not needed for simple rendering.
        /// </summary>
        /// <param name="_meshRenderer">The target Renderer.</param>
        /// <param name="_shadowCastingMode">Whether this Renderer should still cast shadows.</param>
        /// <param name="_recieveShadows">Whether this Renderer should still recieve shadows.</param>
        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        public static void DisableComplexRendering(Renderer _renderer, ShadowCastingMode _shadowCastingMode = ShadowCastingMode.Off, bool _recieveShadows = false) {

            _renderer.motionVectorGenerationMode = MotionVectorGenerationMode.Camera;
            _renderer.reflectionProbeUsage       = ReflectionProbeUsage.Off;
            _renderer.lightProbeUsage            = LightProbeUsage.Off;

            _renderer.shadowCastingMode = _shadowCastingMode;
            _renderer.receiveShadows    = _recieveShadows;

            _renderer.allowOcclusionWhenDynamic = false;
        }

        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        public void ClearAsync(System.Action _callback) {
            StartCoroutine(Clear(_callback));
        }

        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        private IEnumerator Clear(System.Action _callback) {

            lock (this) {

                enabled = false;

                yield return new WaitUntil(() => m_IsGenerating == false);

                Dispose();

                enabled = true;
            }

            _callback?.Invoke();
        }

        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        private void Dispose() {

            enabled = false;

            try {

                /* DESTROY MESHES */
                lock (m_Meshes) {
                    foreach (var item in m_Meshes) {
                        Destroy(item.Value);
                    }

                    m_Meshes.Clear();
                }

                /* CLEAR COLLIDER REFERENCES */
                lock (m_Colliders) {
                    foreach (var item in m_Colliders) {
                        Destroy(item.Value);
                    }

                    m_Colliders.Clear();
                }

                /* CLEAR MESH POOL */
                lock (m_MeshPool) {
                    m_MeshPool.Clear();
                }

                /* CLEAR CHANGED OCTREES */
                lock (m_ChangedOctrees) {
                    m_ChangedOctrees = new ConcurrentBag<Octree>();
                }

                /* DESTROY CHILDREN */
                var children = new List<GameObject>();

                foreach (Transform child in transform) {
                    children.Add(child.gameObject);
                }

                if (m_ScanningParticles != null) {
                    children.Remove(m_ScanningParticles.gameObject);
                }

                foreach (var item in children) {
                    Destroy(item);
                }
            }
            catch (System.Exception e) {
                Diagnostics.Debug.Log("ERROR (Meshing.cs)[Dispose()]: " + e.Message, Diagnostics.Debug.LogType.Error);
            }
        }
    }
}