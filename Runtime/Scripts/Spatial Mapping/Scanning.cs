﻿using System.Collections;
using System.Collections.Generic;

using UnityEngine;
using UnityEngine.XR.ARFoundation;
using UnityEngine.XR.ARSubsystems;

using System;
using System.Threading;
using System.Threading.Tasks;
using System.Runtime.CompilerServices;

using static PhantomTech.PhantomSettings;

namespace PhantomTech.Spatial {

    /// <summary>
    /// Script that automatically scans device depth data into the OctreeManager, if available.
    /// </summary>
    public sealed class Scanning : MonoBehaviour {

        /* SINGLETON */

        public static Scanning Instance;

        /* PRIVATE */

        private SpatialMappingCSWrapped m_DepthMapComputeShader;
        private ComputeBuffer m_PointCloudCB;

        private Vector3D[] m_PointCloud;

#if UNITY_EDITOR

        /* SIMULATION */
        private Material      m_CameraDepthMaterial;
        private RenderTexture m_CameraDepthTexture;

#endif

        // Timer for the next subtraction.
        private float 
            m_ScanStartTime   = 0.0f,
            m_NextSubtraction = 0.0f;

        private bool 
            m_CurrentlyScanning = false,  // Has the scanning process finished?
            m_EventRaised       = true;   // Have we raised the FrameScanned event for this frame yet?

        private ScanFrameTransform m_ScanFrame;

        public struct ScanFrameTransform {

            public Vector3D m_Position;
            public Vector4D m_Rotation;
            public Vector3D m_Forward;

            public ScanFrameTransform(Vector3D _position, Vector4D _rotation, Vector3D _forward) {
                m_Position = _position;
                m_Rotation = _rotation;
                m_Forward  = _forward;
            }
        }

        /* EVENTS */
        public static        FrameScannedHandler FrameScanned;
        public delegate void FrameScannedHandler(FrameScannedArgs _args);
        
        public class FrameScannedArgs : EventArgs {

            public FrameScannedArgs() { 
            
            }

            public FrameScannedArgs(SpatialMapping.RenderTextureRW _depthMapRaw, SpatialMapping.RenderTextureRW _depthMapProcessed) {
                depthMapRaw       = _depthMapRaw;
                depthMapProcessed = _depthMapProcessed;
            }

            public SpatialMapping.RenderTextureRW depthMapRaw       { get; private set; }
            public SpatialMapping.RenderTextureRW depthMapProcessed { get; private set; }
        }

        private void Awake() {

            // Subscribe to the frameRecieved event to process frames as they come in, instead of all the time.
            PScene.Instance.m_AROcclusionManager.frameReceived += OnDepthFrameRecieved;

            Instance = this;
        }

        private void Start() {

            m_NextSubtraction = SpatialMappingSettings.m_Scanning.m_SubtractiveInterval;

            /* INITIALISE COMPUTE SHADER */
            m_DepthMapComputeShader = new SpatialMappingCSWrapped(new SpatialMappingCSWrapped.SpatialMappingParameters());
        }

        private void OnDisable() {

            StopAllCoroutines();

            m_CurrentlyScanning = false;
            m_EventRaised       = true;

            m_PointCloud = null;
        }

        private void OnDestroy() {

            StopAllCoroutines();

            m_CurrentlyScanning = false;
            m_EventRaised       = true;

            if (m_DepthMapComputeShader != null) {
                m_DepthMapComputeShader.Dispose();
                m_DepthMapComputeShader = null;
            }
            
            if (m_PointCloudCB != null) {
                m_PointCloudCB.Release();
                m_PointCloudCB.Dispose();
                m_PointCloudCB = null;
            }
        }

        private void OnDepthFrameRecieved(AROcclusionFrameEventArgs _args) {

            if (enabled) {

                if (PScene.Instance.m_AROcclusionManager != null) {

                    if (Time.timeSinceLevelLoad >= 2.0f) {
                        UpdateDepthMap();

                        if (Time.timeSinceLevelLoad - m_ScanStartTime >= 5.0f) {
                            m_CurrentlyScanning = false;

                            PhantomTech.Diagnostics.Debug.Log("Scanning operation timeout. Resetting...");

                            StopAllCoroutines();
                        }

                        if (m_CurrentlyScanning == false) {
                            m_ScanStartTime = Time.timeSinceLevelLoad;

                            UpdateScanFrame();

                            StartCoroutine(PerformScan());
                        }
                    }
                }
                else { 
                    enabled = false;
                    Diagnostics.Debug.Log("No AROcclusionManager found on PScene.", Diagnostics.Debug.LogType.Error);
                }
            }
        }

#if UNITY_EDITOR

        private void OnRenderImage(RenderTexture source, RenderTexture destination) {
            
            if (PScene.s_SimulateInEditor == true && m_CurrentlyScanning == false) {

                int width  = source.height,
                    height = source.width;

                if (m_CameraDepthTexture == null ||
                   (m_CameraDepthTexture.width  != width ||
                    m_CameraDepthTexture.height != height)) {

                    if (m_CameraDepthTexture != null) { 
                        m_CameraDepthTexture.DiscardContents();
                        m_CameraDepthTexture.Release();
                        DestroyImmediate(m_CameraDepthTexture);
                    }

                    m_CameraDepthTexture = new RenderTexture(
                        width,
                        height,
                        0,
                        RenderTextureFormat.RFloat
                    );

                    m_CameraDepthTexture.enableRandomWrite = true;
                    m_CameraDepthTexture.useMipMap = false;
                    m_CameraDepthTexture.Create();
                }
                
                if (m_CameraDepthMaterial == null) {
                    m_CameraDepthMaterial = new Material(PhantomSettings.Instance.m_ScanningSettings.m_CameraDepthShader);
                }

                Graphics.Blit(source, m_CameraDepthTexture, m_CameraDepthMaterial);

                SpatialMapping.s_BackgroundTexture.Assign(source);

                PScene.CameraFrameScanned?.Invoke(
                    new PScene.CameraFrameScannedArgs(SpatialMapping.s_BackgroundTexture)
                );

                OnDepthFrameRecieved(new AROcclusionFrameEventArgs());
            }
            
            Graphics.Blit(source, destination);
        }

#endif

        private void LateUpdate() {

            if (m_EventRaised == false) {
                m_EventRaised = true;

                if (m_PointCloud != null) {
                    PhantomTech.Diagnostics.Debug.Log(m_PointCloud.Length + " points scanned this frame.");

                    FrameScanned?.Invoke(new FrameScannedArgs(SpatialMapping.s_DepthMapRaw, SpatialMapping.s_DepthMapProcessedScaled));

                    m_PointCloud = null;
                }
            }
        }

        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        private static IEnumerator SubtractAsync(UnityEngine.Texture2D _depthMap) {

            var sw = new System.Diagnostics.Stopwatch();
            sw.Start();

            Diagnostics.Debug.Log("Processing Subtraction...");

            var rotation = PhantomTech.Math.Inverse(Scanning.Instance.m_ScanFrame.m_Rotation);
            var position = Scanning.Instance.m_ScanFrame.m_Position;
            var  forward = Scanning.Instance.m_ScanFrame.m_Forward;

            var pixels = _depthMap.GetRawTextureData<float>();

            int width  = _depthMap.width,
                height = _depthMap.height;

            Task t = Task.Factory.StartNew(() => {

                // Change thread's priority.
                //Thread.CurrentThread.Priority = System.Threading.ThreadPriority.AboveNormal;

                // OFFSET: Offset the distance of the depth map by this many units. Positive values move the depth "further away".
                // Changing this can help reduce artifacts caused by subtraction but also causes more or fewer nodes to be subtracted than is neccesary.
                const float OFFSET = -0.3f;

                // Threshold: Reduces the max depth allowed for subtraction. This is to mitigate artifacts caused by temporaly-unstable depth maps.
                float THRESHOLD = PhantomSettings.Instance.m_ScanningSettings.m_Scanning.m_SubtractionTruncation;

                float sqrMaxDistance       = PhantomTech.Math.Sqr(PScene.Instance.m_DeviceSensorData.m_MaxDepth - THRESHOLD),
                      closeOctreeThreshold = PhantomTech.Math.Sqr(OctreeManager.Instance.m_OctreeSize);

                Vector2 halfFOVRad = PScene.Instance.m_DeviceSensorData.m_HalfFOVRadians;

                // MAIN LOOP:
                var octrees = new List<Octree>();

                // Lock other threads that are using the OctreeManager.
                lock (OctreeManager.Instance.m_Octrees) {

                    // Determine whether each octree is within the range of the point cloud and add the ones that are to a list.
                    foreach (var kvp in OctreeManager.Instance.m_Octrees) {

                        // Lock the octree while we work on it.
                        lock (kvp.Value) {

                            var sqrDistanceToOctree = PhantomTech.Math.SqrDistance(kvp.Key, position);

                            if (sqrDistanceToOctree <= sqrMaxDistance) {

                                // Directional vector to octree, relative to device position and rotation.
                                var dir = PhantomTech.Math.Rotate(kvp.Key - position, rotation);

                                // Get the angle to the octree.
                                PhantomTech.Math.AngleXYRadians(dir, PhantomTech.Math.Forward3, out float x, out float y);

                                // Check the octree is within the sensor's FOV and range.
                                // (Octrees that are close are added anyway).
                                if (sqrDistanceToOctree <= closeOctreeThreshold || (x <= halfFOVRad.x && y <= halfFOVRad.y)) {
                                    octrees.Add(kvp.Value);
                                }
                            }
                        }
                    }
                }

                /* Iterate upon each node of each octree in the list 
                 * and subtract those in front of the depth map. */
                foreach (var octree in octrees) {
                
                    // Lock the octree to prevent modification by another thread while we subtract.
                    lock (octree) {

                        bool shouldTrim = false;

                        var voxels = octree.GetAll(Octree.CopyMode.Reference);

                        foreach (var node in voxels) {

                            if (node.IsLeaf() && node.enabled) { 

                                var sqrDistanceToLeaf = PhantomTech.Math.SqrDistance(node.m_Position, position);
                                if (sqrDistanceToLeaf <= sqrMaxDistance) {

                                    // Directional vector to target node, relative to device position and rotation.
                                    var dir = PhantomTech.Math.Rotate(node.m_Position - position, rotation);

                                    // Get the angle to the node.
                                    PhantomTech.Math.AngleXYRadians(dir, PhantomTech.Math.Forward3, out float radiansX, out float radiansY);

                                    // Check the node is within the sensor's FOV and range.
                                    if (PhantomTech.Math.Abs(radiansX) <= halfFOVRad.x && PhantomTech.Math.Abs(radiansY) <= halfFOVRad.y) {
                                
                                        int x = PhantomTech.Math.Floor(Math.Remap(radiansX, -halfFOVRad.x, halfFOVRad.x, 0.0f, 1.0f) * width);
                                        int y = PhantomTech.Math.Floor(Math.Remap(radiansY, -halfFOVRad.y, halfFOVRad.y, 0.0f, 1.0f) * height);

                                        // Get the depth at the x,y coordinates of the node.
                                        var pixel = pixels[(y * width) + x];

                                        // Get the square distance to the depth value.
                                        float sqrDepth = PhantomTech.Math.Sqr(pixel + OFFSET);

                                        // If the node is "in front" of the depth map, remove it from the octree.
                                        if (sqrDistanceToLeaf < sqrDepth) {
                                            shouldTrim = true;
                                    
                                            // Update the octree's difference depending on how many leaves it already has.
                                            // If it has less than the threshold, set it to the threshold to guarantee a remesh.
                                            octree.m_Difference = PhantomSettings.Instance.m_ScanningSettings.m_MeshGeneration.m_CellDeltaTolerance + 1;

                                            node.Trim(false);
                                        }
                                    }
                                }
                            }
                        }

                        // Optimise the structure of subtracted trees.
                        if (shouldTrim) { octree.RecursiveTrim(); }
                    }
                }
            });

            // Wait until the task above has finished executing and the device isn't paused.
            yield return new UnityEngine.WaitUntil(() => t.IsCompleted && UnityEngine.Time.timeScale != 0.0f);

            sw.Stop();
            Diagnostics.Debug.Log("Subtraction Complete! (" + sw.ElapsedMilliseconds + "ms)");
        }

        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        private void UpdateScanFrame() {
            m_ScanFrame.m_Position = PScene.Instance.m_Device.transform.position;
            m_ScanFrame.m_Rotation = PScene.Instance.m_Device.transform.rotation;
            m_ScanFrame.m_Forward  = PScene.Instance.m_Device.transform.forward;
        }

        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        private void UpdateDepthMap() {
            
            if (enabled) {

#if UNITY_EDITOR
                var image = m_CameraDepthTexture;

                if (image != null) { {
#else
                if (PScene.Instance.m_AROcclusionManager.TryAcquireEnvironmentDepthCpuImage(out var image)) {

                    using (image) {
#endif
                        if (SpatialMapping.s_DepthMapRaw == null) {
                            SpatialMapping.s_DepthMapRaw = new SpatialMapping.RenderTextureRW(
                                image.width,
                                image.height,
                                      TextureFormat.RFloat,
                                RenderTextureFormat.RFloat
                            );
                        }

#if UNITY_EDITOR
                        SpatialMapping.s_DepthMapRaw.Assign(image);
#else
                        var temp = new Texture2D(image.width, image.height, TextureFormat.RFloat, false);
                        image.Copy(temp, XRCpuImage.Transformation.MirrorY);

                        SpatialMapping.s_DepthMapRaw.Assign(temp);

                        Destroy(temp);
#endif
                    }

                    int width  = SpatialMapping.s_DepthMapRaw.width,
                        height = SpatialMapping.s_DepthMapRaw.height;

                    if (SpatialMapping.s_DepthMapProcessedUnscaled == null) {
                        SpatialMapping.s_DepthMapProcessedUnscaled = new SpatialMapping.RenderTextureRW(height, width, TextureFormat.RFloat, RenderTextureFormat.RFloat);

                        float aspectA = (float)SpatialMapping.s_DepthMapProcessedUnscaled.width /
                                        (float)SpatialMapping.s_DepthMapProcessedUnscaled.height,

                              aspectB = (float)Screen.width /
                                        (float)Screen.height,

                              aspectDelta = aspectB / aspectA;

                        // Initialise processing kernel.
                        m_DepthMapComputeShader.InitK0(SpatialMapping.s_DepthMapRaw.RenderTexture, SpatialMapping.s_DepthMapProcessedUnscaled.RenderTexture, new Vector2(1.0f * aspectDelta, 1.0f));
                    }

                    // Process depth map.
                    m_DepthMapComputeShader.ExecuteK0();
                    
                    #region Determine Rescaling Factor

                    // Calculate an optimum texture size for lossless downscaling.
                    float rescalingFactor = SpatialMappingSettings.m_Scanning.m_OptimiseTextureResolution ? 
                        SpatialMapping.DetermineRescalingFactor(width, PScene.Instance.m_DeviceSensorData.m_MaxDepth) : 
                        1.0f;

                    // Apply a lossy downscale.
                    rescalingFactor *= SpatialMappingSettings.m_Scanning.m_FinalTextureScale;

                    #endregion Determine Rescaling Factor

                    #region Perform Rescaling on GPU

                    // Initialise the ProcessedScaled texture.
                    if (SpatialMapping.s_DepthMapProcessedScaled == null) {
                        SpatialMapping.s_DepthMapProcessedScaled = new SpatialMapping.RenderTextureRW(1, 1, TextureFormat.RFloat, RenderTextureFormat.RFloat);
                    }

                    // Resize the depth map and assign output:
                    if (rescalingFactor == 1.0f) {
                        SpatialMapping.s_DepthMapProcessedScaled.Assign(SpatialMapping.s_DepthMapProcessedUnscaled.RenderTexture);
                    }
                    else {
                        var scaledProcessedDepth = RenderTexture.GetTemporary(
                            PhantomTech.Math.ClampPositive((int)(width * rescalingFactor)),
                            PhantomTech.Math.ClampPositive((int)(height * rescalingFactor)),
                            0,
                            RenderTextureFormat.RFloat,
                            RenderTextureReadWrite.Linear
                        );
                        scaledProcessedDepth.filterMode = FilterMode.Point;
                        scaledProcessedDepth.Assign(SpatialMapping.s_DepthMapProcessedUnscaled.RenderTexture);

                        // Assign output:
                        SpatialMapping.s_DepthMapProcessedScaled.Assign(scaledProcessedDepth);

                        RenderTexture.ReleaseTemporary(scaledProcessedDepth);
                    }

                    if (m_PointCloudCB == null) {
                        PhantomTech.Diagnostics.Debug.Log(string.Format("Creating new point cloud ComputeBuffer ({0}, {1})", SpatialMapping.s_DepthMapProcessedScaled.width, SpatialMapping.s_DepthMapProcessedScaled.height));
                        m_PointCloudCB = UnityHelpers.CreateCB(typeof(Vector3D), SpatialMapping.s_DepthMapProcessedScaled.width * SpatialMapping.s_DepthMapProcessedScaled.height);
                    }
                    else if (m_PointCloudCB.count !=
                        SpatialMapping.s_DepthMapProcessedScaled.width *
                        SpatialMapping.s_DepthMapProcessedScaled.height) {

                        m_PointCloudCB.Release();
                        m_PointCloudCB.Dispose();

                        PhantomTech.Diagnostics.Debug.Log(string.Format("Creating new point cloud ComputeBuffer ({0}, {1})", SpatialMapping.s_DepthMapProcessedScaled.width, SpatialMapping.s_DepthMapProcessedScaled.height));
                        m_PointCloudCB = UnityHelpers.CreateCB(typeof(Vector3D), SpatialMapping.s_DepthMapProcessedScaled.width * SpatialMapping.s_DepthMapProcessedScaled.height);
                    }

                    #endregion Perform Rescaling on GPU

                }
                else {
                    PhantomTech.Diagnostics.Debug.Log("ERROR (TestScan.cs): Couldn't retrieve depth map from AROcclusionManager!", PhantomTech.Diagnostics.Debug.LogType.Error);
                }
            }
        }

        [MethodImpl(MethodImplOptions.AggressiveInlining)]
#if UNITY_ANDROID && !UNITY_EDITOR
        private void UpdatePointCloud() {
#else
        private IEnumerator UpdatePointCloud() {
#endif

            /* KERNEL 1: */
            var preprocessedPoints = SpatialMapping.PreprocessDepthPoints(
                SpatialMapping.s_DepthMapProcessedScaled,
                PScene.Instance.m_DeviceSensorData.m_MaxDepth,
                SpatialMappingSettings.m_Scanning.m_ScanningTruncation
            );

            var pointsForProjecting = new Vector3D[m_PointCloudCB.count];

            pointsForProjecting.SetRange(preprocessedPoints, 0, 0,  preprocessedPoints.Length);
                 m_PointCloudCB.SetData(pointsForProjecting, 0, 0, pointsForProjecting.Length);

            Diagnostics.Debug.Log("Projecting points...");
            m_DepthMapComputeShader.m_CSParameters.HalfFOV = PScene.Instance.m_DeviceSensorData.m_HalfFOVRadians;

            m_DepthMapComputeShader.m_CSParameters.CamPosIn = m_ScanFrame.m_Position;
            m_DepthMapComputeShader.m_CSParameters.CamRotIn = m_ScanFrame.m_Rotation;
            m_DepthMapComputeShader.InitK1(m_PointCloudCB, preprocessedPoints.Length);

#if UNITY_ANDROID && !UNITY_EDITOR
            // Run on GPU without using Async (May degrade performance but more platform support).
            m_DepthMapComputeShader.ExecuteK1(m_PointCloud = new Vector3D[preprocessedPoints.Length]);
#else
            // Run on GPU and then pull the data asyncronously.
            var request = m_DepthMapComputeShader.ExecuteK1();

            yield return new WaitUntil(() => request.done);

            if (request.hasError == false) {
                m_PointCloud = new Vector3D[preprocessedPoints.Length];
                    
                m_PointCloud.SetRange(request.GetData<Vector3D>().ToArray(), 0, 0, m_PointCloud.Length);
            }
            else {
                PhantomTech.Diagnostics.Debug.Log("ERROR (TestInsertObject.cs): Unknown error retrieving GPU data asyncronously!", Diagnostics.Debug.LogType.Error);
            }
#endif

            Diagnostics.Debug.Log("Finished projecting points.");
        }

        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        private IEnumerator PerformScan() {

            m_CurrentlyScanning = true;

            // Subtract empty space from the octree manager before inserting the points:
            if (SpatialMappingSettings.m_Scanning.m_AdditiveOnly == false && Time.timeSinceLevelLoad >= m_NextSubtraction) {
                m_NextSubtraction = Time.timeSinceLevelLoad + SpatialMappingSettings.m_Scanning.m_SubtractiveInterval;

                yield return SubtractAsync(SpatialMapping.s_DepthMapProcessedUnscaled);
            }

#if !UNITY_ANDROID || UNITY_EDITOR
            yield return UpdatePointCloud();
#else
            UpdatePointCloud();
#endif

            // Insert the points:
            OctreeManager.Instance.AddPoints(m_PointCloud);

            yield return new WaitForSecondsRealtime(1.0f / SpatialMappingSettings.m_Scanning.m_ScanningFPS);

            m_EventRaised       = false;
            m_CurrentlyScanning = false;
        }
    }
}
