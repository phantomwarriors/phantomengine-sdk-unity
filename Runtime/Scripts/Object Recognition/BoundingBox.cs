using System.Collections;
using System.Collections.Generic;
using UnityEngine;

using System;
using System.Runtime.InteropServices;

namespace PhantomTech.ML {
    [StructLayout(LayoutKind.Sequential)]
    public class BoundingBox {
        [MarshalAs(UnmanagedType.LPStr)]
        public string m_Name;
        public float
                      m_Confidence,
                      m_X,
                      m_Y,
                      m_Width,
                      m_Height;

        public bool m_IsFull;

        public BoundingBox() { }

        public BoundingBox(BoundingBox _other) {
            this.m_Name = _other.m_Name;
            this.m_Confidence = _other.m_Confidence;
            this.m_X = _other.m_X;
            this.m_Y = _other.m_Y;
            this.m_Width = _other.m_Width;
            this.m_Height = _other.m_Height;
            this.m_IsFull = _other.m_IsFull;
        }
        /// <summary>
        /// Converts Normalized BoundingBoxes received from OpenCV
        /// to Bounding Box class objects and corrects box coordinates
        /// to match Device Aspect Ratio
        /// </summary>
        public void Normalise(int _screenWidth, int _screenHeight) {

            this.m_X *= _screenWidth;
            this.m_Y *= _screenHeight;
            this.m_Width *= _screenWidth;
            this.m_Height *= _screenHeight;

        }
        /// <summary>
        /// Prints out a detected object's parameters.
        /// </summary>
        public void PrintDetectedObject() {
            Debug.Log("Detected Object: " + m_Name +
                      " With Confidence: " + m_Confidence +
                      " At Coordinates: " + "CenterX: " + m_X +
                                             " " +
                                             "CenterY: " + m_Y +
                                             " " +
                                             "With Box Dimensions: " +
                                             "Width: " + m_Width +
                                             "  " +
                                             "Height: " + m_Height);
        }

        /// <summary>
        /// Get the name of a detected object.
        /// </summary>
        /// <param name="_index"></param>
        /// <returns></returns>
        public string GetDetectedObjectName() {
            return m_Name;
        }

        /// Get the confidence of a detected object.
        /// </summary>
        /// <param name="_index"></param>
        /// <returns></returns>
        public float GetObjectConfidence() {
            return m_Confidence;
        }
        /// Get the Rect of a detected object.
        /// </summary>
        /// <param name="_index"></param>
        /// <returns></returns>
        public Rect GetObjectRect() {
            return new Rect(m_X, m_Y, m_Width, m_Height);
        }

        public override bool Equals(object _obj) {

            bool result;

            BoundingBox item = (BoundingBox)_obj;

            if (item == null) {
                result = _obj == null;
            }
            else {
                result = item != null
                      && item.m_Name == this.m_Name
                      && item.m_Confidence == this.m_Confidence
                      && item.m_X == this.m_X
                      && item.m_Y == this.m_Y
                      && item.m_Width == this.m_Width
                      && item.m_Height == this.m_Height
                      && item.m_IsFull == this.m_IsFull;
            }

            return result;
        }
        public override int GetHashCode() {

            /*
             * Courtesy of https://stackoverflow.com/questions/5059994/custom-type-gethashcode (Resharper)
             */

            unchecked {
                int result = 23;

                result = (result * 513) + m_Name.GetHashCode();
                result = (result * 513) + m_Confidence.GetHashCode();
                result = (result * 513) + m_X.GetHashCode();
                result = (result * 513) + m_Y.GetHashCode();
                result = (result * 513) + m_Width.GetHashCode();
                result = (result * 513) + m_Height.GetHashCode();
                result = (result * 513) + m_IsFull.GetHashCode();

                return result;
            }
        }
    }
}