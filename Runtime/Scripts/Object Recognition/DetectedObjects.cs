using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace PhantomTech.ML {

    /// <summary>
    /// Wrapper Class providing methods to modify and access
    /// an array of BoundingBox detections.
    /// </summary>
    public class DetectedObjects {

        BoundingBox[] m_DetectedObjects;
        public DetectedObjects(BoundingBox[] boundingBoxes) {
            m_DetectedObjects = boundingBoxes;

        }

        /// <summary>
        /// Get a copy of all detected bounding boxes.
        /// </summary>
        /// <returns></returns>
        public BoundingBox[] GetDetectedObjects() {
            return m_DetectedObjects;
        }


        /// <summary>
        /// Get an array listing all detected objects names.
        /// </summary>
        /// <returns></returns>
        public string[] GetDetectedObjectNames() {
            string[] result = null;
            result = new string[m_DetectedObjects.Length];
            for (int i = 0; i < result.Length; i++) {
                result[i] = new string(m_DetectedObjects[i].m_Name.ToCharArray());
            }
            return result;
        }

        /// <summary>
        /// Get an array listing all detected objects confidences.
        /// </summary>
        /// <returns></returns>
        public float[] GetDetectedObjectsConfidences() {
            float[] result;
            result = new float[m_DetectedObjects.Length];
            for (int i = 0; i < result.Length; i++) {
                result[i] = m_DetectedObjects[i].m_Confidence;
            }
            return result;
        }

        /// <summary>
        /// Get an array listing all detected objects Rects.
        /// </summary>
        /// <returns></returns>
        public Rect[] GetDetectedObjectRects() {
            Rect[] result;
            result = new Rect[m_DetectedObjects.Length];
            for (int i = 0; i < result.Length; i++) {
                result[i] = new Rect(m_DetectedObjects[i].m_X, m_DetectedObjects[i].m_Y, m_DetectedObjects[i].m_Width, m_DetectedObjects[i].m_Height);
            }
            return result;
        }

        /// <summary>
        /// Get the total amount of detected objects.
        /// </summary>
        /// <returns></returns>
        public int GetDetectedObjectsCount() {
            return m_DetectedObjects.Length;
        }

        /// <summary>
        /// Get the BoundingBox of a detected object at index.
        /// </summary>
        /// <param name="_index"></param>
        /// <returns></returns>
        public BoundingBox GetDetectedObject(int _index) {
            BoundingBox result;
            result = new BoundingBox(m_DetectedObjects[_index]);
            return result;
        }

        /// <summary>
        /// Returns true if specified object is detected.
        /// </summary>
        /// <param name="_name"></param>
        /// <returns></returns>
        public bool OnSpecificObjectDetection(string _name) {
            bool result = false;
            for (int i = 0; i < m_DetectedObjects.Length; i++) {
                if (m_DetectedObjects[i].m_Name == _name) {
                    result = true;
                    break;
                }
            }
            return result;
        }
        /// <summary>
        /// Returns Bounding Box with a specified name
        /// </summary>
        /// <param name="_name"></param>
        /// <param name="_specificBoundingBox"></param>
        /// <returns></returns>
        public bool TryGetSpecificObjectDetection(string _name, out BoundingBox _specificBoundingBox) {
            if (OnSpecificObjectDetection(_name)) {
                for (int i = 0; i < m_DetectedObjects.Length; i++) {
                    if (m_DetectedObjects[i].m_Name == _name) {
                        _specificBoundingBox = m_DetectedObjects[i];
                        return true;
                    }

                }

            }
            else {
                _specificBoundingBox = null;
                return false;
            }
            _specificBoundingBox = null;
            return false;
        }

        /// <summary>
        /// Prints out all detection in the console.
        /// </summary>
        public void PrintDetectedObjects() {
            for (int i = 0; i < m_DetectedObjects.Length; i++) {
                Debug.Log("Detected Object: " + m_DetectedObjects[i].m_Name +
                          " With Confidence: " + m_DetectedObjects[i].m_Confidence +
                          " At Coordinates: " + "X: " + m_DetectedObjects[i].m_X +
                                                  " " +
                                                 "Y: " + m_DetectedObjects[i].m_Y +
                                                  " " +
                                                 "With Box Dimensions: " +
                                                 "Width: " + m_DetectedObjects[i].m_Width +
                                                  "  " +
                                                 "Height: " + m_DetectedObjects[i].m_Height);
            }
        }

        /// <summary>
        /// Returns Last Detection
        /// </summary>
        /// <param name="bb"></param>
        /// <returns></returns>
        public bool TryGetLastDetection(out BoundingBox bb) {
            bb = m_DetectedObjects[m_DetectedObjects.Length - 1];
            return true;

        }
    }
}
