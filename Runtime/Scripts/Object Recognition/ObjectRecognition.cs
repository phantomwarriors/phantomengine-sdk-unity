#if PHANTOMTECH_EXPERIMENTAL_OBJECTRECOGNITION

using System;
using System.IO;
using System.Linq;
using System.Collections.Concurrent;

using System.Threading;
using System.Runtime.InteropServices;

using UnityEngine;

using static PhantomTech.PhantomSettings;

namespace PhantomTech.ML {

    /// <summary>
    /// ObjectRecognition is an experimental module that implements the OpenCV library for running Machine Learning Algorithms.
    /// It is highly performance demanding.
    /// This feature is still in active development and hasn't been optimised.
    /// You may encounter performance drops whilst using this module.
    /// We do not claim any responsibility for any damages potentially caused by using this experimental feature.
    /// Please report any bugs to the developer.
    /// </summary>
    [AddComponentMenu("PhantomEngine/Experimental/Object Recognition/Object Recognition")]
    public sealed class ObjectRecognition : MonoBehaviour {


        /// <summary>
        /// Singleton of ObjectRecognition use it to access all public methods.
        /// </summary>
        public static ObjectRecognition s_Instance;

        /// <summary>
        /// Struct used to obtrain Texture Data from OpenCV using
        /// GetRawTextureData, which should be the fastest metod for
        /// obtaining texture data.
        /// </summary>
        struct RGB {
            byte R, G, B;
        }
        /// <summary>
        /// Pointer to access unmanaged OpenCV code.
        /// </summary>
        private IntPtr p_YOLOv4Pointer;
       
        /// <summary>
        /// List of Bounding Boxes to be used in managed code.
        /// Coordinates use the screen width and height.
        /// </summary>
        private ConcurrentQueue<BoundingBox> m_BoundingBoxes = new ConcurrentQueue<BoundingBox>();

        /// <summary>
        /// List of Bounding Boxes that are received from unmanaged code.
        /// Coordinates are normalized.
        /// </summary>
        private ConcurrentStack<BoundingBox> m_BoundingBoxesStack = new ConcurrentStack<BoundingBox>();
               
        /// <summary>
        /// Collection of Frame Data to Process Using OpenCV.
        /// </summary>
        private ConcurrentQueue<RGB[]> m_PreProcessFrames = new ConcurrentQueue<RGB[]>();

        /// <summary>
        /// Collection of BoundingBoxes specifically for drawing them when debugging.
        /// </summary>
        private ConcurrentQueue<BoundingBox> m_BoundingBoxDraw = new ConcurrentQueue<BoundingBox>();
       
        /// <summary>
        /// GUIStyle for Bounding Boxes
        /// </summary>
        private GUIStyle m_GUIStyleBB = new GUIStyle();
       
        /// <summary>
        /// GUIStyle for Bounding Box Label
        /// </summary>
        private GUIStyle m_GUIStyleLabel = new GUIStyle();
    
        /// <summary>
        /// Worker Thread to run the YOLO algorithm on
        /// to not bottleneck the Main Thread
        /// </summary>
        private static Thread s_YOLOWorkerThread;

        /// <summary>
        /// Access to Main Camea.
        /// </summary>
        private Camera m_Camera;

        /// <summary>
        /// Detected Objects get assigned to this wrapper.
        /// class
        /// </summary>
        private DetectedObjects m_DetectedObjects;
       
        /// <summary>
        /// Total amount of detected boxes.
        /// </summary>
        private int m_BoundingBoxCount;

        /// <summary>
        /// Cache of the ScreenWidth and Height
        /// </summary>
        private int m_ScreenWidth, m_ScreenHeight;

        ///TODO, use this variable in some manner to pause the thread
        ///after some time of it not being used
        //public float m_ThreadTimer = 10;

        /// <summary>
        /// Storing DeltaTime so that it can be used in a thread.
        /// </summary>
        private float m_DeltaTime;

        /// <summary>
        /// Toggle for MultiThreading.
        /// </summary>
        static private volatile bool m_ToggleMultiThreading;

        /// <summary>
        /// Thread Specific Object for locking Threads.
        /// </summary>
        static readonly object locker = new object();

        /// <summary>
        /// Cache of the width and height of the background texture
        /// provided from Spatial Mapping.
        /// </summary>
        private int m_TexWidth, m_TexHeight;

        /// <summary>
        /// Boolean Value to enable Drawing of Debugging Bounding Boxes.
        /// </summary>
        private bool m_Draw;

        /// <summary>
        /// Timer to prevent debugging bounding boxes from disappering too quickly.
        /// </summary>
        private float m_DrawTimer;


        /// <summary>
        /// Label offset from top left corner of Bounding Box.
        /// </summary>
        private const float m_BoundingBoxLabelOffset = 9;

        /* EVENTS */
        public static ObjectRecognitionHandler OnDetections;
        public delegate void ObjectRecognitionHandler(ObjectRecognitionArgs _args);

        public class ObjectRecognitionArgs : EventArgs {

            public ObjectRecognitionArgs(DetectedObjects _detectedObjects) {

                DetectedObjects = _detectedObjects;
            }

            public DetectedObjects DetectedObjects { get; private set; }



        }
        /// <summary>
        /// Sets GUIStyle Variables
        /// </summary>
        private void SetGUIStyles() {


            Font myFont;
            Texture2D BoundingBox;
            Texture2D BoundingBoxLabel;

            //Grabs Assets from Editor Parameters  
            PhantomTech.Interface.EditorParameters.Instance.TryGetElement(Interface.EditorUtilities.Instance.m_Font_Header, out myFont);
            PhantomTech.Interface.EditorParameters.Instance.TryGetElement(Interface.EditorUtilities.Instance.m_Texture_BoundingBox, out BoundingBox);
            PhantomTech.Interface.EditorParameters.Instance.TryGetElement(Interface.EditorUtilities.Instance.m_Texture_BoundingBoxLabel, out BoundingBoxLabel);

            //Text Changes
            m_GUIStyleBB.font = myFont;
            m_GUIStyleBB.normal.textColor = Color.white;
            m_GUIStyleBB.padding.left = 10;
            m_GUIStyleBB.padding.top = 10;
            //BoundingBox Texture
            m_GUIStyleBB.normal.background = BoundingBox;
            m_GUIStyleBB.border.left = 5;
            m_GUIStyleBB.border.right = 5;
            m_GUIStyleBB.border.top = 5;
            m_GUIStyleBB.border.bottom = 5;
            //BoundingBox Label
            m_GUIStyleLabel.normal.background = BoundingBoxLabel;
            m_GUIStyleLabel.imagePosition = ImagePosition.ImageOnly;

        }

        private void Awake() {

            s_Instance = this;
            PScene.CameraFrameScanned += GrabCameraFootage;
            m_Camera = GameObject.FindGameObjectWithTag("MainCamera").GetComponent<Camera>();

#if UNITY_STANDALONE || UNITY_EDITOR




#elif UNITY_IOS
          
#elif UNITY_ANDROID
 
#endif

        }

        private void Start() {
            SetGUIStyles();
            m_ScreenWidth = Screen.width;
            m_ScreenHeight = Screen.height;
#if UNITY_STANDALONE || UNITY_EDITOR



            p_YOLOv4Pointer = OpenCVWrapper.CreateYOLO();

            OpenCVWrapper.SetPaths(p_YOLOv4Pointer,
                Application.dataPath + "/StreamingAssets/PhantomTech/" + ObjectRecognitionSettings.m_Names,
                Application.dataPath + "/StreamingAssets/PhantomTech/" + ObjectRecognitionSettings.m_Config,
                Application.dataPath + "/StreamingAssets/PhantomTech/" + ObjectRecognitionSettings.m_Weights);




            OpenCVWrapper.InitYOLO(p_YOLOv4Pointer);
            CheckDetectableObjectsList("Assets/StreamingAssets/PhantomTech/yolov4-tiny.names");
            m_DeltaTime = Time.deltaTime;
#elif UNITY_IOS

        Debug.Log("Hello IOS");

        p_YOLOv4Pointer = OpenCVWrapper.CreateYOLO();

        string names   = Application.dataPath + "/Raw" + "/PhantomTech/" + ObjectRecognitionSettings.m_Names;
        string cfg     = Application.dataPath + "/Raw" + "/PhantomTech/" + ObjectRecognitionSettings.m_Config;
        string weights = Application.dataPath + "/Raw" + "/PhantomTech/" + ObjectRecognitionSettings.m_Weights;

        OpenCVWrapper.SetPaths(p_YOLOv4Pointer, names, cfg, weights);
        OpenCVWrapper.InitYOLO(p_YOLOv4Pointer);
        CheckDetectableObjectsList(names);
        m_DeltaTime = Time.deltaTime;
#elif UNITY_ANDROID

        Debug.Log("Hello Android");

        CopyFileAsyncOnAndroid();
        p_YOLOv4Pointer = OpenCVWrapper.CreateYOLO();

        string names   = Application.persistentDataPath + "/PhantomTech/" + ObjectRecognitionSettings.m_Names;
        string cfg     = Application.persistentDataPath + "/PhantomTech/" + ObjectRecognitionSettings.m_Config;
        string weights = Application.persistentDataPath + "/PhantomTech/" + ObjectRecognitionSettings.m_Weights;

        OpenCVWrapper.SetPaths(p_YOLOv4Pointer, names, cfg, weights);
        OpenCVWrapper.InitYOLO(p_YOLOv4Pointer);
           
        CheckDetectableObjectsList(names);

#else
        throw new System.NotImplementedException("ERROR (YOLOImage.cs [Start()]): Unimplemented platform.");
#endif

        }

        private void OnEnable() {
#if UNITY_STANDALONE || UNITY_EDITOR
            ToggleThread();

#elif UNITY_IOS
  ToggleThread();
#elif UNITY_ANDROID
    ToggleThread();
#endif
        }

        private void OnDisable() {
#if UNITY_STANDALONE || UNITY_EDITOR
            ToggleThread();
#elif UNITY_IOS
    ToggleThread();
#elif UNITY_ANDROID
    ToggleThread();
#endif
        }

        /// <summary>
        /// Draw Debug Bounding Boxes
        /// </summary>
        private void OnGUI() {

            if (m_Draw && ObjectRecognitionSettings.m_DebuggingBoxes) {
                if (!m_BoundingBoxDraw.IsEmpty) {
                    foreach (var m_BoundingBoxDraw in m_BoundingBoxDraw) {


                        AdjustFontText(m_BoundingBoxDraw, m_GUIStyleBB);
                        string text = m_BoundingBoxDraw.m_Name + " " + Math.Round(m_BoundingBoxDraw.m_Confidence, 1);
                        GUIContent content = new GUIContent(text);

                        m_GUIStyleBB.CalcSize(content);
                        Vector2 temp = m_GUIStyleBB.CalcSize(content);


                        GUI.Box(new Rect(m_BoundingBoxDraw.m_X + m_BoundingBoxLabelOffset, m_BoundingBoxDraw.m_Y + m_BoundingBoxLabelOffset, temp.x, temp.y), GUIContent.none, m_GUIStyleLabel);
                        GUI.Box(new Rect(m_BoundingBoxDraw.m_X, m_BoundingBoxDraw.m_Y, m_BoundingBoxDraw.m_Width, m_BoundingBoxDraw.m_Height),
                                        text, m_GUIStyleBB);

                    }
                }
            }
        }

        /// <summary>
        /// Adjust debug boxes text font based on
        /// Bounding Box Width and text length
        /// </summary>
        /// <param name="_BB"></param>
        /// <param name="_style"></param>
        private void AdjustFontText(BoundingBox _BB, GUIStyle _style) {
            float fontChange = 0;
            float widthChange = 0;
            string text = _BB.m_Name + " " + Math.Round(_BB.m_Confidence, 1);
            //Decreases font based on how many characters need be printed in the box
            fontChange -= text.Length / 2;
            //Decreases/Increases font size based on if bounding box width is over 150
            widthChange = (17 - Mathf.Sqrt(_BB.m_Width)) * 1.5f;
            fontChange -= widthChange;
            _style.fontSize = 30 + (int)fontChange;
        }

        private void LateUpdate() {
            if (m_Draw == true) {
                m_DrawTimer -= Time.deltaTime;

                if (m_DrawTimer <= 0) {
                    m_Draw = false;
                }
            }

        }

        /// <summary>
        /// Checks if custom detection list is valid. 
        /// </summary>
        /// <param name="path"></param>
        private void CheckDetectableObjectsList(string path) {
            var mainList = File.ReadAllLines(path);

            foreach (var item in ObjectRecognitionSettings.m_CustomDetectableObjectsList) {
                if (mainList.Contains(item)) {
                    PhantomTech.Diagnostics.Debug.Log(item + " Is on the list of detectable objects");
                }
                else {
                    PhantomTech.Diagnostics.Debug.Log(item + " Is not on list of detectable objects");
                    PhantomTech.Diagnostics.Debug.Log("The list can be found at: Assets/StreamingAssets/PhantomTech/yolov4-tiny.names");
                }
            }
        }

        /// <summary>
        /// Grabs Webcam Footage if on Windows.
        /// Otherwise grabs camera footage from Scanning.cs.
        /// </summary>
        /// <param name="_args"></param>


        private void GrabCameraFootage(PScene.CameraFrameScannedArgs _args) {

            /* Event can still be triggered while script is disabled, 
             * so check if enabled first */
            if (enabled) {


#if UNITY_STANDALONE || UNITY_EDITOR
                {
                    m_TexWidth = Screen.width;
                    m_TexHeight = Screen.height;

                    Texture2D texture2D = new Texture2D(m_TexWidth, m_TexHeight, TextureFormat.RGB24, false);
                    PhantomTech.UnityExtensions.Copy(m_Camera.activeTexture, texture2D);
                    //Texture2D texture2D = ToTexture2D(m_Camera.activeTexture);

                    if (s_YOLOWorkerThread.IsAlive == false) {
                        var PixelData = texture2D.GetRawTextureData<RGB>().ToArray();

                        InferenceOnImage(PixelData);

                    }
                    else {
                        var PixelData = texture2D.GetRawTextureData<RGB>().ToArray();
                        m_PreProcessFrames.Enqueue(PixelData);
                    }


                    if (OnDetection()) {
                        m_DetectedObjects = new DetectedObjects(GetDetectedObjects());
                        OnDetections?.Invoke(new ObjectRecognitionArgs(m_DetectedObjects));

                        m_BoundingBoxDraw = new ConcurrentQueue<BoundingBox>();
                        foreach (var item in m_BoundingBoxes) {

                            m_BoundingBoxDraw.Enqueue(item);
                        }
                        m_Draw = true;
                        m_DrawTimer = ObjectRecognitionSettings.m_DisplayTime;
                        ClearLastBoxes();
                    }

                    ///Prevents Container from overfilling
                    ///Alternative create a new concurrent queue as its
                    ///less performance intensive
                    if (m_PreProcessFrames.Count > 10) {

                        for (int i = 1; i < m_PreProcessFrames.Count; i++) {
                            m_PreProcessFrames.TryDequeue(out var frame);
                        }

                    }
                    Destroy(texture2D);

                }
#elif UNITY_IOS
                if (s_YOLOWorkerThread.IsAlive == false)
                {
                    Texture2D data = _args.cameraFrame.Texture2D;

                    if (data != null)
                    {
                        InferenceOnImage(data.GetRawTextureData<RGB>().ToArray());
                    }
                }
                else
                {

                    Texture2D data = _args.cameraFrame.Texture2D;
                    m_TexWidth = data.width;
                    m_TexHeight = data.height;
                    m_PreProcessFrames.Enqueue(data.GetRawTextureData<RGB>().ToArray());
                }
            if (!m_BoundingBoxes.IsEmpty)
            {

                m_BoundingBoxDraw = new ConcurrentQueue<BoundingBox>();
                foreach (var item in m_BoundingBoxes)
                {

                    m_BoundingBoxDraw.Enqueue(item);
                }
                m_Draw = true;
                m_DrawTimer = ObjectRecognitionSettings.m_DisplayTime;
                ClearLastBoxes();
            }
           

                    ///Prevents Container from overfilling
                    ///Alternative create a new concurrent queue as its
                    ///less performance intensive
                    if (m_PreProcessFrames.Count > 10)
                    {

                        for (int i = 1; i < m_PreProcessFrames.Count; i++)
                        {
                            m_PreProcessFrames.TryDequeue(out var frame);
                        }

                    }
                
#elif UNITY_ANDROID
    if (s_YOLOWorkerThread.IsAlive == false)
                {
                    Texture2D data = _args.cameraFrame.Texture2D;

                    if (data != null)
                    {
                        InferenceOnImage(data.GetRawTextureData<RGB>().ToArray());
                    }
                }
                else
                {

                    Texture2D data = _args.cameraFrame.Texture2D;
                    m_TexWidth = data.width;
                    m_TexHeight = data.height;
                    m_PreProcessFrames.Enqueue(data.GetRawTextureData<RGB>().ToArray());
                }
            if (!m_BoundingBoxes.IsEmpty)
            {

                m_BoundingBoxDraw = new ConcurrentQueue<BoundingBox>();
                foreach (var item in m_BoundingBoxes)
                {

                    m_BoundingBoxDraw.Enqueue(item);
                }
                m_Draw = true;
                m_DrawTimer = ObjectRecognitionSettings.m_DisplayTime;
                ClearLastBoxes();
            }
           

                    ///Prevents Container from overfilling
                    ///Alternative create a new concurrent queue as its
                    ///less performance intensive
                    if (m_PreProcessFrames.Count > 10)
                    {

                        for (int i = 1; i < m_PreProcessFrames.Count; i++)
                        {
                            m_PreProcessFrames.TryDequeue(out var frame);
                        }

                    }
#else
                throw new System.NotImplementedException("ERROR (ObjectRecognition.cs [Start()]): Unimplemented platform.");
#endif
            }
        }

        /// <summary>
        /// Runs YOLO algorithm on image and passes Bounding Boxes
        /// from unmanaged code to Unity
        /// </summary>
        /// <param name="_texture"></param>
        private void InferenceOnImage(RGB[] data) {

            var pixelHandle = GCHandle.Alloc(data, GCHandleType.Pinned);


            OpenCVWrapper.SetConfidence(p_YOLOv4Pointer, ObjectRecognitionSettings.m_ConfThreshold);
            OpenCVWrapper.SetNMSConfidence(p_YOLOv4Pointer, ObjectRecognitionSettings.m_NmsThreshold);
            OpenCVWrapper.RunInference(p_YOLOv4Pointer, pixelHandle.AddrOfPinnedObject(), m_TexWidth, m_TexHeight, 0);

            m_BoundingBoxCount = OpenCVWrapper.GetBoundingBoxSizes(p_YOLOv4Pointer);

            pixelHandle.Free();

            //Assign Boxes if detections occur
            for (int i = 0; i < m_BoundingBoxCount; i++) {
                m_BoundingBoxesStack.Push((BoundingBox)Marshal.PtrToStructure(OpenCVWrapper.GetBoundingBox(p_YOLOv4Pointer, i), typeof(BoundingBox)));
                if (ObjectRecognitionSettings.m_CustomDetectableObjectsList.Length == 0) {

                    m_BoundingBoxesStack.TryPop(out var _yoloBox);
                    _yoloBox.Normalise(m_ScreenWidth, m_ScreenHeight);
                    m_BoundingBoxes.Enqueue(_yoloBox);
                }
                else {
                    foreach (var name in ObjectRecognitionSettings.m_CustomDetectableObjectsList) {

                        m_BoundingBoxesStack.TryPeek(out var _peekYoloBox);
                        string YoloName = _peekYoloBox.m_Name;
                        if (YoloName == name) {

                            m_BoundingBoxesStack.TryPop(out var _yoloBox);
                            _yoloBox.Normalise(m_ScreenWidth, m_ScreenHeight);

                            m_BoundingBoxes.Enqueue(_yoloBox);
                            break;
                        }
                        else {

                            m_BoundingBoxesStack.TryPop(out var _yoloBox);
                            m_BoundingBoxesStack.Push(_yoloBox);
                        }
                    }
                }

            }
            m_BoundingBoxCount = m_BoundingBoxes.Count;

            OpenCVWrapper.ClearBoundingBoxes(p_YOLOv4Pointer);

        }

        /// <summary>
        /// Funcion to clear bounding box container class 
        /// from old detections.
        /// </summary>
        private void ClearLastBoxes() {
            lock (locker) {

                if (!m_BoundingBoxes.IsEmpty) {
                    lock (m_BoundingBoxes) {
                        m_BoundingBoxes = new ConcurrentQueue<BoundingBox>();
                    }
                }
                if (!m_BoundingBoxesStack.IsEmpty) {
                    m_BoundingBoxesStack.Clear();
                }
            }
        }


        /// <summary>
        /// Copies neural network files on to Android device.
        /// </summary>
        private void CopyFileAsyncOnAndroid() {

            //In Android = "jar:file://" + Application.dataPath + "!/assets/" 

            var fromPath = Application.streamingAssetsPath + "/";
            var toPath = Application.persistentDataPath + "/";

            if (Directory.Exists(toPath + "PhantomTech")) {
                Debug.Log("PhantomTech Directory already exists.");
            }
            else {
                Directory.CreateDirectory(toPath + "PhantomTech");
            }

            var filesNamesToCopy = new string[] {
            "PhantomTech/yolov4-tiny.names",
            "PhantomTech/yolov4-tiny.cfg",
            "PhantomTech/yolov4-tiny.weights"
        };

            foreach (var fileName in filesNamesToCopy) {

                Debug.Log("Copying from " + fromPath + fileName + " to " + toPath);

                var www = new WWW(fromPath + fileName);
                while (!www.isDone) { }

                File.WriteAllBytes(toPath + fileName, www.bytes);
            }

        }

        public void ToggleThread() {
            lock (locker) {
                m_ToggleMultiThreading = !m_ToggleMultiThreading;
            }
            if (m_ToggleMultiThreading == true) {

                s_YOLOWorkerThread = new Thread(() => PreProcess(m_PreProcessFrames));
                s_YOLOWorkerThread.IsBackground = true;
                s_YOLOWorkerThread.Start();
                PhantomTech.Diagnostics.Debug.Log("Is YOLOThread Alive?: " + s_YOLOWorkerThread.IsAlive);
            }
            else {
                ///Doesn't actually abort, just throws a flag that joining is about to happen
                s_YOLOWorkerThread.Abort();
                s_YOLOWorkerThread.Join();
                PhantomTech.Diagnostics.Debug.Log("Is YOLOThread Alive?: " + s_YOLOWorkerThread.IsAlive);
            }

        }
        private void OnDestroy() {


            lock (locker) {
                if (s_YOLOWorkerThread != null) {
                    m_ToggleMultiThreading = false;
                    ///Doesn't actually abort, just throws a flag that joining is about to happen
                    s_YOLOWorkerThread.Abort();
                    s_YOLOWorkerThread.Join();
                    PhantomTech.Diagnostics.Debug.Log("Is YOLOThread Alive?: " + s_YOLOWorkerThread.IsAlive);
                    s_YOLOWorkerThread = null;
                }
                if (m_BoundingBoxes.Count > 0) {
                    lock (m_BoundingBoxes) {
                        m_BoundingBoxes = new ConcurrentQueue<BoundingBox>();
                        m_BoundingBoxes = null;
                    }
                }
                if (m_BoundingBoxDraw.Count > 0) {

                    m_BoundingBoxDraw = new ConcurrentQueue<BoundingBox>();
                    m_BoundingBoxDraw = null;

                }

            }
            if (m_BoundingBoxesStack.Count > 0) {
                m_BoundingBoxesStack.Clear();
            }

        }


        private void PreProcess(ConcurrentQueue<RGB[]> _PreProcessFrames) {
            //      float timer = m_ThreadTimer;
            while (m_ToggleMultiThreading) {


                if (_PreProcessFrames.Count() > 0) {
                    RGB[] frame;

                    _PreProcessFrames.TryDequeue(out frame);
                    InferenceOnImage(frame);
                }
                else {

                    // Debug.Log("No Frames To Process");
                }

                ///Time out the thread if it's running prepetually
                ///Ensure thread can restart if needed.
            }

        }

        /// <summary>
        /// Get an Array copy of all detected bounding boxes.
        /// </summary>
        /// <returns></returns>
        private BoundingBox[] GetDetectedObjects() {
            lock (m_BoundingBoxes) {

                BoundingBox[] result = m_BoundingBoxes.ToArray();
                return result;
            }
        }

        /*DEVELOPER METHODS*/
#region DeveloperMethods

        public void setActive() {
            enabled = !enabled;
        }

        /// <summary>
        /// Returns true if any detections have occured.
        /// </summary>
        /// <returns></returns>
        public bool OnDetection() {
            return m_BoundingBoxes.Count > 0;
        }

        /// <summary>
        /// Enable/Disable Object Recognition.
        /// </summary>
        /// <param name="_isOn"></param>
        public void EnableObjectRecognition(bool _isOn) {
            this.enabled = _isOn;
        }



#endregion

    }
    internal class OpenCVWrapper {

#if UNITY_STANDALONE_WIN || UNITY_EDITOR_WIN
        const string m_DLLName = "OpenCVWrapperDLL";
#elif UNITY_STANDALONE_OSX || UNITY_EDITOR_OSX
            const string m_DLLName = "OpenCVWrapper";
#elif UNITY_IOS
        const string m_DLLName = "__Internal";
#elif UNITY_ANDROID
        const string m_DLLName = "OpenCVAndroidVS455";
#endif

        [DllImport(m_DLLName, EntryPoint = "CreateYOLO")]
        public static extern IntPtr CreateYOLO();

        [DllImport(m_DLLName, EntryPoint = "DeleteYOLO")]
        public static extern void DeleteYOLO(IntPtr _YOLOv4Pointer);

        [DllImport(m_DLLName, EntryPoint = "initYOLO")]
        public static extern void InitYOLO(IntPtr _YOLOv4Pointer);

        [DllImport(m_DLLName, EntryPoint = "setPaths")]
        public static extern void SetPaths(IntPtr _YOLOv4Pointer, string _filePathNames, string _filePathsCfg, string _filePathsWeights);

        [DllImport(m_DLLName, EntryPoint = "runYOLO")]
        public static extern IntPtr RunYOLO(IntPtr _YOLOv4Pointer, IntPtr _rawImage, int _width, int _height);

        [DllImport(m_DLLName, EntryPoint = "ProcessImage")]
        public static extern IntPtr ProcessImage(IntPtr _YOLOv4Pointer, IntPtr _rawImage, int _width, int _height);

        [DllImport(m_DLLName, EntryPoint = "initBuffer")]
        public static extern void InitBuffer(IntPtr _YOLOv4Pointer, int _width, int _height);

        [DllImport(m_DLLName, EntryPoint = "getBoundingBox")]
        public static extern IntPtr GetBoundingBox(IntPtr _YOLOv4Pointer, int _index);


        [DllImport(m_DLLName, EntryPoint = "getBoundingBoxSizes")]
        public static extern int GetBoundingBoxSizes(IntPtr _YOLOv4Pointer);

        [DllImport(m_DLLName, EntryPoint = "runInference")]
        public static extern void RunInference(IntPtr _YOLOv4Pointer, IntPtr _rawImage, int _width, int _height, int mirror);

        [DllImport(m_DLLName, EntryPoint = "clearBoundingBoxes")]
        public static extern void ClearBoundingBoxes(IntPtr _YOLOv4Pointer);

        [DllImport(m_DLLName, EntryPoint = "setConfidence")]
        public static extern void SetConfidence(IntPtr _YOLOv4Pointer, double _confidence);

        [DllImport(m_DLLName, EntryPoint = "setNMSConfidence")]
        public static extern void SetNMSConfidence(IntPtr _YOLOv4Pointer, double _NMSconfidence);

    }
}

#else

using UnityEngine;
using UnityEditor;

namespace PhantomTech.ML{

#if UNITY_EDITOR

 /// <summary>
    /// This object is Editor-Only. Scripting for it may result in unexpected behaviour.
    /// </summary>
    [CustomEditor(typeof(ObjectRecognition))]
    public sealed class ObjectRecognitionEditor : Editor {

        public override void OnInspectorGUI() {

            DrawDefaultInspector();

            EditorGUILayout.HelpBox("ObjectRecognition is not enabled. If you wish to enable ObjectRecognition, please define PHANTOMTECH_EXPERIMENTAL_OBJECTRECOGNITION as a symbol in your project's scripting define symbols.", MessageType.Info);

            EditorGUILayout.TextField("PHANTOMTECH_EXPERIMENTAL_OBJECTRECOGNITION");
        }

    }

#endif

    /// <summary>
    /// If you are seeing this message, ObjectRecognition is not enabled. If you wish to enable ObjectRecognition, please define PHANTOMTECH_EXPERIMENTAL_OBJECTRECOGNITION as a symbol in your project's scripting define symbols.
    /// </summary>
    [AddComponentMenu("PhantomEngine/Experimental/Object Recognition/Object Recognition")]
    public sealed class ObjectRecognition : MonoBehaviour {}
}

#endif