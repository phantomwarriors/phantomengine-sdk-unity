using System.Collections;
using System.Collections.Generic;

using UnityEngine;

#if UNITY_EDITOR
using UnityEditor;
using UnityEditor.Callbacks;

using System.IO;

///Credit: https://support.unity.com/hc/en-us/articles/207942813-How-can-I-disable-Bitcode-support-


#if UNITY_IOS

using UnityEditor.iOS.Xcode;

namespace PhantomTech.ML { 

    public class DisableBitcodeiOS : MonoBehaviour {

        [PostProcessBuild(1000)]
        public static void PostProcessBuildAttribute(BuildTarget target, string pathToBuildProject) {

            if (target == BuildTarget.iOS) {

                string projectPath = PBXProject.GetPBXProjectPath(pathToBuildProject);

                PBXProject pbxProject = new PBXProject();
                pbxProject.ReadFromFile(projectPath);
#if UNITY_2019_3_OR_NEWER
                var targetGuid = pbxProject.GetUnityMainTargetGuid();
#else
                    var targetName = PBXProject.GetUnityTargetName();
                    var targetGuid = pbxProject.TargetGuidByName(targetName);
#endif
                pbxProject.SetBuildProperty(targetGuid, "ENABLE_BITCODE", "NO");
                pbxProject.WriteToFile(projectPath);

                var projectInString = File.ReadAllText(projectPath);

                projectInString = projectInString.Replace("ENABLE_BITCODE = YES;",
                    $"ENABLE_BITCODE = NO;");
                File.WriteAllText(projectPath, projectInString);
            }
        }
    }
}
#endif
#endif