﻿using System.Collections;
using System.Collections.Generic;

using UnityEditor;
using UnityEngine;
using UnityEngine.Android;

using System;
using System.Runtime.CompilerServices;

using static PhantomTech.Math;
using static PhantomTech.Location.Coordinates;

namespace PhantomTech.Location {

    /// <summary>
    /// Script providing access to the device's location, if available.
    /// </summary>
    [ExecuteInEditMode]
    public sealed class GlobalPositioningService : MonoBehaviour {

        /* PUBLIC */

        /// <summary>
        /// Duration of time before a GPS location query times out.
        /// </summary>
        public const float s_QueryTimeout = 5;

        /// <summary>
        /// Amount of time to spend calculating the initial coordinates for coordinate conversions.
        /// </summary>
        public const float s_CalibrationDuration = 5;

        /* PRIVATE */

        private double s_NextCalibration = 0;

        /// <summary>
        /// The last timestam that the initial coordinates were calculated.
        /// </summary>
        public double s_LastCalibrationTimestamp { get; private set; } = 0;

        /* EVENTS */
        public static        PositionUpdatedHandler PositionUpdated;
        public delegate void PositionUpdatedHandler(PositionUpdatedArgs _args);

        public class PositionUpdatedArgs : EventArgs {

            public PositionUpdatedArgs(LocationInfo _locationInfo) {
                position = new Position(_locationInfo);
            }

            public PositionUpdatedArgs(Position _position) {
                position = _position;
            }

            public Position position { get; private set; }
        }

        // True if the device's GPS location is currently being queried.
        private bool m_isQuerying = false;

        #region EDITOR_GIZMOS

#if UNITY_EDITOR

        [Header("Editor Debug")]
        [SerializeField] private bool m_DrawGizmos = true;

        [Space]
        [SerializeField] private Vector3D m_DeviceCoords = new Vector3D(51.477928f, -0.001545f, 6.0f); // Greenwich, England (Approx.)
        [SerializeField] private float    m_DeviceHeading;

        [Space]
        [SerializeField] private Vector3D m_CustomMarkerCoords;

        [Space]
        [SerializeField] [Range(0.000001f, 1.0f)] private float m_EarthScale  = 1.0f;
        [SerializeField] private float m_MarkerSize = 10.0f;

        private static bool s_PositionInvoked = false;

        private void OnDrawGizmos() {

            if (m_DrawGizmos) {

                // Initialise the initial position for debugging purposes.
                if (Application.isEditor == true && Application.isPlaying == false) {

                    // Initialise the current heading for debug use.
                    s_CurrentHeading = Wrap(m_DeviceHeading, -180.0f, 180.0f);

                    // Initialise the initial position for debug use.
                    s_InitialPosition = new Position(
                        WrapGPS(m_DeviceCoords),
                        GetTimestamp()
                    );
                }

                if (s_InitialPosition != null) {
                    
                    float sphereRadius = s_EarthRadius * m_EarthScale;

                    Vector3D coords = s_InitialPosition.m_LatLongAlt;
                    coords.x  = 180.0f - coords.x;
                    coords.z += Sqr(s_EarthRadius);

                    var sphereSpace  = GeoToSphere(coords);
                    var scaledCoords = sphereSpace * m_EarthScale;

                    var center = -scaledCoords;

                    // Rotate the gizmo by initial GPS coords.
                    Gizmos.matrix = PhantomTech.Matrix4x4.Multiply(
                        PhantomTech.Matrix4x4.Rotation(0, s_CurrentHeading,      0),
                        PhantomTech.Matrix4x4.Rotation(90 - coords.x, -coords.y, 0)
                    ).Parse<UnityEngine.Matrix4x4>();

                    #region Draw Globe

                    // Globe
                    Gizmos.color = Color.gray;
                    Gizmos.DrawWireSphere(center, sphereRadius);

                    #endregion Draw Globe

                    #region Draw Poles

                    float poleLength = sphereRadius * 1.5f;

                    // North Pole
                    Gizmos.color = Color.red;
                    Gizmos.DrawRay(center, new Vector3(0,  poleLength, 0));

                    // South Pole
                    Gizmos.color = Color.blue;
                    Gizmos.DrawRay(center, new Vector3(0, -poleLength, 0));

                    #endregion Draw Poles

                    #region Draw Origin

                    // Player
                    Handles.color = Handles.zAxisColor;
                    Handles.ConeHandleCap(
                        0,
                        center + scaledCoords,
                        UnityEngine.Quaternion.Euler(0, 0, 0),
                        m_MarkerSize,
                        EventType.Repaint
                    );

                    #endregion Draw Origin

                    // Set the gizmo to use engine-space.
                    Gizmos.matrix = UnityEngine.Matrix4x4.TRS(new Vector3(), new UnityEngine.Quaternion(0, 0, 0, 1), new Vector3(1, 1, 1));

                    #region Draw Custom Marker

                    Gizmos.DrawSphere(
                        GeoToWorld(m_CustomMarkerCoords),
                        m_MarkerSize
                    );

                    #endregion Draw Custom Marker
                }

            }
        }

        private void SpoofGPS() {

            if (s_PositionInvoked == false) {
                s_PositionInvoked = true;

                var pos = new Position(m_DeviceCoords, GetTimestamp());

                Coordinates.s_InitialPosition = pos;

                Coordinates.s_InitialHeading = m_DeviceHeading;
                Coordinates.s_CurrentHeading = m_DeviceHeading;

                PositionUpdated?.Invoke(new PositionUpdatedArgs(pos));
            }
        }

#endif

        #endregion EDITOR_GIZMOS

        private void Start() {
            RequestLocationAccess();
        }

        private void OnDisable() {
            Input.location.Stop();
        }

        private void Update() {

#if UNITY_EDITOR

            if (PScene.s_SimulateInEditor) {
                SpoofGPS(); // Editor debug code.
            }
            else {
                return;
            }
#endif
            if (!m_isQuerying) {
                StartCoroutine(QueryLocation());
            }
        }

        private IEnumerator QueryLocation() {

            m_isQuerying = true;

            if (Input.location.isEnabledByUser) {
                Input.location.Start();
            }
            else {
                m_isQuerying = false;

                yield break;
            }

            float timeout = s_QueryTimeout;

            while (Input.location.status == LocationServiceStatus.Initializing) {

                if (timeout > 0) {

                    yield return null;

                    timeout -= Time.unscaledDeltaTime;
                }
                else {
                    PhantomTech.Diagnostics.Debug.Log("Location service timeout.");

                    m_isQuerying = false;
                    yield break;
                }
            }

            if (Input.location.status != LocationServiceStatus.Running) {
                PhantomTech.Diagnostics.Debug.Log("Unable to determine device location.");

                m_isQuerying = false;
                yield break;
            }

            UpdateLocation(Input.location.lastData);

            m_isQuerying = false;
        }

        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        private void UpdateLocation(LocationInfo _locationInfo) {

            Input.compass.enabled = true;

            if (s_NextCalibration <= 0) {
                s_NextCalibration = GetTimestamp();
            }

            Coordinates.s_CurrentHeading = Input.compass.trueHeading;

            if (GetTimestamp() < s_NextCalibration + s_CalibrationDuration) {
                CalibrateInitialCoordinates(_locationInfo);
            }

            PositionUpdated?.Invoke(new PositionUpdatedArgs(_locationInfo));
        }

        /// <summary>
        /// Calculates the initial coordinates of the device for global coordinate conversions.
        /// </summary>
        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        private void CalibrateInitialCoordinates(LocationInfo _locationInfo) {

            Coordinates.s_InitialPosition = new Coordinates.Position(_locationInfo);
            Coordinates.s_InitialHeading  = s_CurrentHeading + PScene.Instance.m_Device.transform.eulerAngles.y;

            s_LastCalibrationTimestamp = GetTimestamp();
        }

        /// <summary>
        /// Request permission to use the device's location services.
        /// </summary>
        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        public void RequestLocationAccess() {

            bool success;

            Permission.RequestUserPermission(Permission.FineLocation);
            success = Permission.HasUserAuthorizedPermission(Permission.FineLocation);

            if (!success) {

                Permission.RequestUserPermission(Permission.CoarseLocation);
                success = Permission.HasUserAuthorizedPermission(Permission.CoarseLocation);
            }

            if (!success) {
                PhantomTech.Diagnostics.Debug.Log("Permission to use location not granted by user.", PhantomTech.Diagnostics.Debug.LogType.Error);
            }
        }

        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        private double GetTimestamp() { 
            return DateTime.Now.Subtract(new DateTime(1970, 1, 1, 0, 0, 0)).TotalSeconds;
        }
    }
}
