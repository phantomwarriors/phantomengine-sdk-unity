﻿using UnityEngine;

/// <summary>
/// Controls the animation state of the flying robot character.
/// </summary>
public sealed class FlyingRobotStateController : MonoBehaviour {

    /* PUBLIC */

    /// <summary>
    /// Animator of the character.
    /// </summary>
    public Animator Animator { get; private set; }

    public bool Idle   { get; set; }
    public bool Moving { get; set; }

    private void Awake() {
        Animator = GetComponent<Animator>();
    }

    void LateUpdate() {

        enabled = Animator != null;

        if (enabled) {
            Animator.SetBool("isIdle",   Idle);
            Animator.SetBool("isMoving", Moving);
        }
    }
}
