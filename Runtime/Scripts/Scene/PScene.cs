﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;

using UnityEngine.XR.ARFoundation;
using UnityEngine.XR.ARSubsystems;

using PhantomTech.Spatial;
using PhantomTech.Navigation;

using System;
using System.Linq;
using System.Runtime.CompilerServices;

namespace PhantomTech {

    /// <summary>
    /// Implements PhantomScene in a Unity Engine context.
    /// </summary>
    public sealed class PScene : MonoBehaviour {

        /* SINGLETON */

        public static PScene Instance;

        /* PUBLIC */

        public PhantomScene m_Scene { get; private set; } = new PhantomScene();

        [Header("References")]
        public AROcclusionManager m_AROcclusionManager;
        public ARCameraBackground m_ARCameraBackground;
        public ARCameraManager    m_ARCameraManager;

        public Camera m_Device;

        public Octree.RoundingMode m_OctreeSizing;

        [HideInInspector] public SensorData m_DeviceSensorData;

#if UNITY_EDITOR

        [HideInInspector] [SerializeField] 
        public static bool 
            s_SimulateInEditor = true, 
            s_SyncEditorCamera = true;

#endif

        /* PRIVATE */

        [Header("Octree Manager")]
        [SerializeField] private int m_OctreeNodesPerUnit = 512;    // Target node capacity of each octree within the scene's OctreeManager.
        [SerializeField] private float m_OctreeUnitSize   = 1.0f;   // Size of individual octrees within the scene's OctreeManager.

        private Material m_RotateMaterial;
        private Texture2D m_BackgroundTextureRaw;
        private RenderTexture m_BackgroundTextureRT;
        
        /// <summary>
        /// Collection of all objects within the PScene.
        /// </summary>
        private List<PObject> m_Objects = new List<PObject>();

        /// <summary>
        /// Class containing information about a device's depth sensor.
        /// </summary>
        public class SensorData {

            [Tooltip("The name of the device according to Unity's \"SystemInfo.deviceName\".")]
            private string m_DeviceName = string.Empty;
            public string DeviceName {

                get {

                    if (string.IsNullOrEmpty(m_DeviceName)) { 
                    
#if UNITY_EDITOR
                        m_DeviceName = "Unity Editor";
#else
                        m_DeviceName = SystemInfo.deviceModel;
#endif

                        m_DeviceName = m_DeviceName.Trim();
                    }

                    return m_DeviceName;
                }
            }

            [Tooltip("The horizontal and vertical field of view of the device when held in portrait mode.")]
            public Vector2 m_FOV;

            [Tooltip("FOV in radians.")]
            public Vector2 m_FOVRadians;

            [Tooltip("Half of FOV.")]
            public Vector2 m_HalfFOV;

            [Tooltip("Half of FOV in radians.")]
            public Vector2 m_HalfFOVRadians;

            [Tooltip("The maximum distance from the camera the device's depth can output. These values are usually 5 for iOS and 8 for Android devices.")]
            public float m_MaxDepth;

        }

        /* EVENTS */
        public static        CameraFrameScannedHandler CameraFrameScanned;
        public delegate void CameraFrameScannedHandler(CameraFrameScannedArgs _args);
        
        public class CameraFrameScannedArgs : EventArgs {

            public CameraFrameScannedArgs(SpatialMapping.RenderTextureRW _cameraFrame) {
                cameraFrame = _cameraFrame;
            }

            public SpatialMapping.RenderTextureRW cameraFrame { get; private set; }
        }

#if UNITY_EDITOR

        /* EDITOR SIMULATION */
        private Camera m_SimulationCamera;
        const string s_SimulationCameraName = "Simulation Camera";

#pragma warning disable 0649

        [Header("Debug")]
        [SerializeField] private bool m_DrawOctrees = true;
        [SerializeField] private Gradient m_OctreeColor;
        [SerializeField] private Transform[] m_DebugPoints;

#pragma warning restore 0649

#endif

        /// <summary>
        /// Handles initialisation of Phantom Scene.
        /// </summary>
        private void Awake() {

#if UNITY_EDITOR
            InitSimulationMode();
#endif
            InitSensorData();

            m_RotateMaterial = new Material(PhantomSettings.Instance.m_ScanningSettings.m_RotateShader);

            m_ARCameraManager.frameReceived += OnCameraFrameRecieved;

#if UNITY_EDITOR

            m_DrawOctrees = false;

            foreach (var debugPoint in m_DebugPoints) {
                Destroy(debugPoint.gameObject);
            }

#endif

            m_Scene.Init(m_OctreeNodesPerUnit, m_OctreeUnitSize, m_OctreeSizing);

            gameObject.layer = LayerMask.NameToLayer("PhantomTech_Hidden");
            gameObject.tag = "PhantomScene";

            Instance = this;
        }

        private void Update() {
            UpdateSensorFOV();
        }

        private void LateUpdate() {

#if UNITY_EDITOR

            if (s_SimulateInEditor && s_SyncEditorCamera) { 
            
                var sv = SceneView.lastActiveSceneView;

                m_Device.fieldOfView = sv.camera.fieldOfView;

                if (sv) {
                    var view = sv.camera.transform;

                    PScene.Instance.m_Device.transform.SetPositionAndRotation(view.position, view.rotation);
                }
            }

            if (m_SimulationCamera) {
                m_SimulationCamera.nearClipPlane = PScene.Instance.m_Device.nearClipPlane;
                m_SimulationCamera.farClipPlane  = PScene.Instance.m_Device.farClipPlane;
            }
#endif

        }

#if UNITY_EDITOR

        private void InitSimulationMode() { 
        
            // Initialise the parameters needed for simulation mode.
            m_Device = Camera.main;

            m_Device.depthTextureMode |= DepthTextureMode.Depth;

            SpatialMapping.s_BackgroundTexture = new SpatialMapping.RenderTextureRW(Screen.width, Screen.height, false);

            m_SimulationCamera = new GameObject(s_SimulationCameraName, typeof(Camera)).GetComponent<Camera>();
            m_SimulationCamera.transform.parent = m_Device.transform;
            m_SimulationCamera.transform.SetPositionAndRotation(
                m_Device.transform.position, 
                m_Device.transform.rotation
            );

            var layer = LayerMask.NameToLayer(PhantomSettings.Instance.m_ScanningSettings.m_MeshGeneration.m_Layer);

            int mask = 0;
            mask.SetBit(layer, 1);

            m_SimulationCamera.cullingMask = mask;
            m_SimulationCamera.clearFlags  = CameraClearFlags.Nothing;

            int renderMask = m_Device.cullingMask;
            renderMask.SetBit(layer, 0);

            m_Device.cullingMask = renderMask;
        }

#endif

        private void InitSensorData() {

            m_DeviceSensorData = new SensorData() {

#if UNITY_EDITOR || UNITY_ANDROID
                m_MaxDepth = 8.0f,
#elif UNITY_IOS
                m_MaxDepth = 5.0f,
#else
                "Not Implemented"
#endif
            };

            UpdateSensorFOV();
        }

        private void UpdateSensorFOV() {

            float newY = m_Device.fieldOfView;
            float newX = newY * m_Device.aspect;

            // Only ever increase the device's FOV.
            if (newY > m_DeviceSensorData.m_FOV.y) {

                m_DeviceSensorData.m_FOV.x = newX;
                m_DeviceSensorData.m_FOV.y = newY;

                m_DeviceSensorData.m_FOVRadians.x = PhantomTech.Math.DegreesToRadiansUnwrapped(m_DeviceSensorData.m_FOV.x);
                m_DeviceSensorData.m_FOVRadians.y = PhantomTech.Math.DegreesToRadiansUnwrapped(m_DeviceSensorData.m_FOV.y);

                m_DeviceSensorData.m_HalfFOV.x = newX * 0.5f;
                m_DeviceSensorData.m_HalfFOV.y = newY * 0.5f;

                m_DeviceSensorData.m_HalfFOVRadians.x = PhantomTech.Math.DegreesToRadiansUnwrapped(m_DeviceSensorData.m_HalfFOV.x);
                m_DeviceSensorData.m_HalfFOVRadians.y = PhantomTech.Math.DegreesToRadiansUnwrapped(m_DeviceSensorData.m_HalfFOV.y);
            }
        }

        private void OnCameraFrameRecieved(ARCameraFrameEventArgs _eventArgs) {
            UpdateBackgroundTexture();
        }

        /// <summary>
        /// Adds a new PObject to the scene using provided parameters.
        /// </summary>
        /// <param name="_prefab">The prefab of the object.</param>
        /// <param name="_purposes">The object's purposes.</param>
        /// <param name="_name">Optional custom name to assign the object.</param>
        /// <returns>The created PObject.</returns>
        public PObject CreateObject(GameObject _prefab, uint[] _purposes, string _name = null) {

            var pObject = GameObject.Instantiate(_prefab, transform).AddComponent<PObject>();
            pObject.m_Object.Init(m_Scene, _purposes, _name);

            if (string.IsNullOrEmpty(_name)) {
                _name = pObject.name.Remove(pObject.name.Length - 7, 7);
            }

            pObject.transform.name = string.Concat(pObject.m_Object.UID, ": ", _name);

            m_Scene.AddObject(pObject.m_Object);

            m_Objects.Add(pObject);

            return pObject;

        }

        /// <summary>
        /// Destroys all PObjects in the Unity scene that are not referenced in the underlying PhantomScene.
        /// </summary>
        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        public void Consolidate() {

            for (int i = 0; i != m_Objects.Count; i++) {

                var pObject = m_Objects[i];

                if (m_Scene.m_PhantomObjects.ContainsValue(pObject.m_Object) == false) {
                    Destroy(pObject.gameObject);

                    m_Objects.RemoveAt(i);
                }
            }
        }

        /// <summary>
        /// Update the SpatialMapping camera background texture.
        /// </summary>
        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        private void UpdateBackgroundTexture() {

            if (m_ARCameraManager != null) {

                if (m_ARCameraManager.TryAcquireLatestCpuImage(out XRCpuImage image)) {

                    using (image) {

                        float aspectA = (float)image.height / (float)image.width;
                        float aspectB = (float)Screen.width / (float)Screen.height;

                        float aspectDelta = aspectB / aspectA;

                        int newWidth  = (int)((float)image.height * aspectDelta), 
                            newHeight = image.width;
                        
                        var       textureFormat =        TextureFormat.RGB24;
                        var renderTextureFormat = RenderTextureFormat.ARGB32;

                        // Initialise background texture if it is null.
                        if (SpatialMapping.s_BackgroundTexture == null) {

                            m_BackgroundTextureRaw = new Texture2D(image.width, image.height, textureFormat, false);
                            
                            SpatialMapping.s_BackgroundTexture = new SpatialMapping.RenderTextureRW(newWidth, newHeight, textureFormat, renderTextureFormat);

                            m_BackgroundTextureRT = UnityHelpers.CreateComputeRT(newWidth, newHeight, false);
                        }

                        image.Convert(
                            new XRCpuImage.ConversionParams(image, textureFormat, XRCpuImage.Transformation.MirrorY),
                            m_BackgroundTextureRaw.GetRawTextureData<byte>()
                        );

                        m_BackgroundTextureRaw.Apply(false);

                        m_RotateMaterial.SetVector("_ScaleFactor", new Vector2(1.0f * aspectDelta, 1.0f));
                        Graphics.Blit(m_BackgroundTextureRaw, m_BackgroundTextureRT, m_RotateMaterial);

                        SpatialMapping.s_BackgroundTexture.Assign(m_BackgroundTextureRT);

                        CameraFrameScanned?.Invoke(new CameraFrameScannedArgs(SpatialMapping.s_BackgroundTexture));
                    }
                }
                else {
                    PhantomTech.Diagnostics.Debug.Log("ERROR (TestScan.cs): Failed to acquire latest CPU Image.", Diagnostics.Debug.LogType.Error);
                }
            }
            else {
                PhantomTech.Diagnostics.Debug.Log("ERROR (TestScan.cs): No ARCameraManager detected for getting background material!", Diagnostics.Debug.LogType.Error);
            }
        }

        #region EDITOR_GIZMOS

#if UNITY_EDITOR

        private void OnDrawGizmos() {

            if (m_DrawOctrees) {

                if (Application.isPlaying == false) {

                    OctreeManager.Instantiate(PhantomTech.Math.ClampMin(m_OctreeNodesPerUnit, 1), m_OctreeUnitSize, m_OctreeSizing);

                    OctreeManager.Instance.AddPoints(
                        m_DebugPoints.Select(transform => (Vector3D)transform.position).ToArray()
                    );
                }

                foreach (var octree in OctreeManager.Instance.m_Octrees.Values) {
                    DrawOctreeGizmo(octree, m_OctreeColor);
                }
            }
        }

        private static void DrawOctreeGizmo(Octree _octree, UnityEngine.Gradient _color) {

            // Roughly checks if the _octree is on the screen and prevents it being rendered if not. Useful when drawing multiple octrees in the scene view, or octrees with a very high resolution.
            bool outOfView = false;

            Camera mainSceneViewCamera = SceneView.GetAllSceneCameras()[0];

            if ((mainSceneViewCamera.transform.position - _octree.m_Position.m_Vector3).sqrMagnitude > (_octree.m_Size * 2) * (_octree.m_Size * 2)) {
                outOfView = Vector3.Angle(_octree.m_Position.m_Vector3 - mainSceneViewCamera.transform.position, mainSceneViewCamera.transform.forward) > Mathf.Max(90, mainSceneViewCamera.fieldOfView);
            }

            // Draw the _octree.
            if (outOfView == false) {

                var nodes = _octree.GetAll(Octree.CopyMode.Reference);

                foreach (var leaf in nodes) {

                    // Draw a solid cube where enabled nodes are.
                    if (leaf.enabled == true) {

                        Gizmos.color = _color.Evaluate(1.0f);

                        Gizmos.DrawCube(leaf.m_Position, leaf.m_Size * new Vector3(1, 1, 1));
                    }

                    // Draw empty cubes where disabled nodes are.
                    else {
                        Gizmos.color = _color.Evaluate((float)((_octree.m_Resolution - 1) - leaf.m_Depth) / (float)_octree.m_Resolution);

                        if (Gizmos.color.a != 0) {
                            Gizmos.DrawWireCube(leaf.m_Position, leaf.m_Size * new Vector3(1, 1, 1));
                        }
                    }
                }
            }
        }

#endif

#endregion EDITOR_GIZMOS

        /// <summary>
        /// Handles automatic destruction of a PhantomScene when the object is destroyed or goes out of scope in Unity.
        /// </summary>
        private void OnDestroy() {
            m_Scene.DestroySelf();
        }
    }
}