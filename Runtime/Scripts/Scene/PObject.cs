﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace PhantomTech {

    /// <summary>
    /// Implements PhantomObject in a Unity Engine context.
    /// </summary>
    public sealed class PObject : MonoBehaviour {

        public PhantomObject m_Object { get; private set; } = new PhantomObject();

        private void Awake() {
            gameObject.layer = LayerMask.NameToLayer("PhantomTech_Visible");
            gameObject.tag   = "PhantomObject";
        }

        private void Start() {
            SyncPosition();
        }

        private void LateUpdate() {
            SyncPosition();
        }

        /// <summary>
        /// Resynchronise the position and rotation of the PhantomObject at the end of each frame.
        /// </summary>
        private void SyncPosition() {
            m_Object.m_Position.Assign(transform.position);
            m_Object.m_Rotation.Assign(transform.rotation);
        }

#if UNITY_EDITOR

        private void OnDrawGizmosSelected() {
            Gizmos.color = new Color32(128, 128, 255, 255);
            
            foreach (Transform t in transform.GetComponentsInChildren<Transform>()) {
                foreach (MeshFilter meshFilter in t.GetComponents<MeshFilter>()) {
                    Gizmos.DrawWireMesh(meshFilter.mesh, -1, t.position, t.rotation, t.lossyScale);
                }
            }
        }

#endif

        /// <summary>
        /// Handles automatic removal of PhantomObjects from the PhantomScene when the object is destroyed or goes out of scope in Unity.
        /// </summary>
        private void OnDestroy() {
            m_Object.DestroySelf();
        }
    }
}