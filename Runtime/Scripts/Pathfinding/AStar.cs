﻿using System;
using System.Collections.Generic;

namespace PhantomTech.Navigation {

    public sealed class AStar {

        private PathfindingGrid m_Map;
        private PathfindingNode[,] m_PathfindingNodes;

        private int m_MapSizeX, m_MapSizeZ;

        public AStar(PathfindingGrid _map) {
            m_Map = _map;
        }
        ~AStar() { }

        public List<Vector3D> Solve(Vector3D _start, Vector3D _end) {

            m_MapSizeX = m_Map.GridSizeX;
            m_MapSizeZ = m_Map.GridSizeZ;
            m_PathfindingNodes = new PathfindingNode[m_MapSizeX, m_MapSizeZ];

            List<Vector3D> path = new List<Vector3D>();
            Heap<PathfindingNode> openSet = new Heap<PathfindingNode>(m_Map.GridSizeX * m_Map.GridSizeZ);

            for (int x = 0; x < m_Map.GridSizeX; x++) {
                for (int z = 0; z < m_Map.GridSizeZ; z++) {
                    m_PathfindingNodes[x, z] = new PathfindingNode(new Vector3DInt(x, 0, z));
                    m_PathfindingNodes[x, z].m_obstructed = m_Map.Grid[x, z].m_obstructed;
                }
            }
            
            Vector3DInt start = m_Map.GetNode(_start).m_position;
            Vector3DInt end = m_Map.GetNode(_end).m_position;

            m_PathfindingNodes[start.x, start.z].m_gScore = 0;
            m_PathfindingNodes[end.x, end.z].m_hScore = 0;

            PathfindingNode startNode = m_PathfindingNodes[start.x, start.z];
            PathfindingNode endNode = m_PathfindingNodes[end.x, end.z];

            openSet.Add(startNode);

            bool solved = false;
            while (solved != true && openSet.Count != 0) {
                
                Vector3DInt currentPos = openSet.RemoveFirst().m_position;
                PathfindingNode currentNode = m_PathfindingNodes[currentPos.x, currentPos.z];
                currentNode.m_complete = true;
               
                if (currentPos == end) {
                    solved = true;
                    path = TracePath(startNode, endNode);
                }
                else {
                    List<PathfindingNode> jumpPoints = JumpPointSearch(currentPos, currentNode.m_direction, end);

                    int count = jumpPoints.Count;
                    for (int i = 0; i < count; i++) {
                        if (jumpPoints[i] != null) {

                            m_Map.Grid[jumpPoints[i].m_position.x, jumpPoints[i].m_position.z].m_jumpedsearched = true; //DEBUGGING

                            if (!jumpPoints[i].m_complete && !jumpPoints[i].m_obstructed) {

                                float gScore;
                                float dist = Math.Abs(jumpPoints[i].m_position.x - currentPos.x) + Math.Abs(jumpPoints[i].m_position.z - currentPos.z);

                                if (jumpPoints[i].m_direction.x != 0 && jumpPoints[i].m_direction.z != 0) {
                                    gScore = currentNode.m_gScore + (dist * 0.707107f);
                                }
                                else {
                                    gScore = currentNode.m_gScore + dist;
                                }

                                if (gScore < jumpPoints[i].m_gScore) {

                                    jumpPoints[i].m_gScore = gScore;

                                    if (jumpPoints[i].m_hScore == Math.FloatInfinity) {
                                        jumpPoints[i].m_hScore = Math.Abs(endNode.m_position.x - jumpPoints[i].m_position.x) + Math.Abs(endNode.m_position.z - jumpPoints[i].m_position.z);
                                    }

                                    jumpPoints[i].m_fScore = jumpPoints[i].m_gScore + jumpPoints[i].m_hScore;
                                    jumpPoints[i].m_parent = currentPos;

                                    openSet.Add(jumpPoints[i]);
                                }
                            }
                        }
                    }
                }
            }

            return path;
        }

        private List<PathfindingNode> JumpPointSearch(Vector3DInt _pos, Vector3DInt _dir, Vector3DInt _end) {

            List<PathfindingNode> jumpPoints = new List<PathfindingNode>();
            List<PathfindingNode> neighbours = new List<PathfindingNode>();

            neighbours = PruneNeighbours(_pos, _dir);

            int count = neighbours.Count;
            for (int i = 0; i < count; i++) {
                if (neighbours[i] != null) {
                    jumpPoints.Add(FindJumpPoint(neighbours[i].m_position, neighbours[i].m_direction, _end));
                }
            }

            return jumpPoints;
        }

        private PathfindingNode FindJumpPoint(Vector3DInt _pos, Vector3DInt _dir, Vector3DInt _end) {

            PathfindingNode jumpPoint = null;

            bool stop = false;
            bool isDiagonal = _dir.x * _dir.z != 0;

            if (QueryObstacle(_pos.x, _pos.z)) {
                stop = true;
            }
            else if (QueryEnd(_pos.x, _pos.z, _end)) {
                jumpPoint = m_PathfindingNodes[_pos.x, _pos.z];
                jumpPoint.m_direction = _dir;

                stop = true;
            }
            else if (isDiagonal) {
                if (QueryObstacle(_pos.x - _dir.x, _pos.z)) {
                    if (!QueryObstacle(_pos.x - _dir.x, _pos.z + _dir.z)) {
                        jumpPoint = m_PathfindingNodes[_pos.x, _pos.z];
                        jumpPoint.m_direction = _dir;

                        stop = true;
                    }
                }
                else if (QueryObstacle(_pos.x, _pos.z - _dir.z)) {
                    if (!QueryObstacle(_pos.x + _dir.x, _pos.z - _dir.z)) {
                        jumpPoint = m_PathfindingNodes[_pos.x, _pos.z];
                        jumpPoint.m_direction = _dir;

                        stop = true;
                    }
                }

                if (jumpPoint == null) {
                    Vector3DInt horizontal = new Vector3DInt(_dir.x, 0, 0);
                    Vector3DInt vertical = new Vector3DInt(0, 0, _dir.z);

                    if (FindJumpPoint(_pos + horizontal, horizontal, _end) != null || FindJumpPoint(_pos + vertical, vertical, _end) != null) {
                        jumpPoint = m_PathfindingNodes[_pos.x, _pos.z];
                        jumpPoint.m_direction = _dir;

                        stop = true;
                    }
                }
            }
            else if (_dir.x != 0 || _dir.z != 0) {

                if (QueryObstacle(_pos.x - _dir.z, _pos.z - _dir.x)) {
                    if (!QueryObstacle(_pos.x - _dir.z + _dir.x, _pos.z - _dir.x + _dir.z)) {
                        jumpPoint = m_PathfindingNodes[_pos.x, _pos.z];
                        jumpPoint.m_direction = _dir;

                        stop = true;
                    }
                }
                if (QueryObstacle(_pos.x + _dir.z, _pos.z + _dir.x)) {
                    if(!QueryObstacle(_pos.x + _dir.z + _dir.x, _pos.z + _dir.x + _dir.z)) {
                        jumpPoint = m_PathfindingNodes[_pos.x, _pos.z];
                        jumpPoint.m_direction = _dir;

                        stop = true;
                    }
                }
            }

            if (!stop) {
                jumpPoint = FindJumpPoint(_pos + _dir, _dir, _end);
            }

            return jumpPoint;
        }

        private List<PathfindingNode> PruneNeighbours(Vector3DInt _pos, Vector3DInt _dir) {
            
            List<PathfindingNode> neighbours = new List<PathfindingNode>();

            if (_dir.x != 0 && _dir.z != 0) {

                neighbours.Add(ValidateNeighbour(_pos.x + _dir.x, _pos.z + _dir.z, _pos));
                neighbours.Add(ValidateNeighbour(_pos.x + _dir.x, _pos.z         , _pos));
                neighbours.Add(ValidateNeighbour(_pos.x         , _pos.z + _dir.z, _pos));
                
                if (QueryObstacle(_pos.x - _dir.x, _pos.z)) {
                    neighbours.Add(ValidateNeighbour(_pos.x - _dir.x, _pos.z + _dir.z, _pos));
                }
                if (QueryObstacle(_pos.x, _pos.z - _dir.z)) {
                    neighbours.Add(ValidateNeighbour(_pos.x + _dir.x, _pos.z - _dir.z, _pos));
                }
            }
            else if (_dir.x != 0 || _dir.z != 0) {
                neighbours.Add(ValidateNeighbour(_pos.x + _dir.x, _pos.z + _dir.z, _pos));
                
                if (QueryObstacle(_pos.x - _dir.z, _pos.z - _dir.x)) {
                    neighbours.Add(ValidateNeighbour(_pos.x - _dir.z + _dir.x, _pos.z - _dir.x + _dir.z, _pos));
                }
                if (QueryObstacle(_pos.x + _dir.z, _pos.z + _dir.x)) {
                    neighbours.Add(ValidateNeighbour(_pos.x + _dir.z + _dir.x, _pos.z + _dir.x + _dir.z, _pos));
                }
            }
            else {
                neighbours.Add(ValidateNeighbour(_pos.x - 1, _pos.z - 1, _pos));
                neighbours.Add(ValidateNeighbour(_pos.x - 1, _pos.z    , _pos));
                neighbours.Add(ValidateNeighbour(_pos.x - 1, _pos.z + 1, _pos));
                neighbours.Add(ValidateNeighbour(_pos.x    , _pos.z - 1, _pos));
                neighbours.Add(ValidateNeighbour(_pos.x    , _pos.z + 1, _pos));
                neighbours.Add(ValidateNeighbour(_pos.x + 1, _pos.z - 1, _pos));
                neighbours.Add(ValidateNeighbour(_pos.x + 1, _pos.z    , _pos));
                neighbours.Add(ValidateNeighbour(_pos.x + 1, _pos.z + 1, _pos));
            }

            return neighbours;
        }

        private PathfindingNode ValidateNeighbour(int _posX, int _posZ, Vector3DInt _pos) {

            PathfindingNode node = null;

            if (_posX >= 0 && _posX < m_MapSizeX &&
                _posZ >= 0 && _posZ < m_MapSizeZ) {

                if (!m_PathfindingNodes[_posX, _posZ].m_obstructed) {
                    node = m_PathfindingNodes[_posX, _posZ];
                    node.m_direction = node.m_position - _pos;
                }
            }

            return node;
        }

        private bool QueryObstacle(int _posX, int _posZ) {

            bool result = true;

            if (_posX >= 0 && _posX < m_MapSizeX && 
                _posZ >= 0 && _posZ < m_MapSizeZ) {

                result = m_PathfindingNodes[_posX, _posZ].m_obstructed;
            }

            return result;
        }

        private bool QueryEnd(int _posX, int _posZ, Vector3DInt _end) {

            bool result = false;

            if (_posX >= 0 && _posX < m_MapSizeX &&
                _posZ >= 0 && _posZ < m_MapSizeZ) {

                result = m_PathfindingNodes[_posX, _posZ].m_position == _end;
            }

            return result;
        }

        private List<Vector3D> TracePath(PathfindingNode _start, PathfindingNode _end) {

            List<Vector3D> path = new List<Vector3D>();
            Vector3DInt currentNode = _end.m_position;

            while (currentNode != _start.m_position) {
                path.Add(m_Map.GridToWorldPosition(currentNode.x, currentNode.z));
                currentNode = m_PathfindingNodes[currentNode.x, currentNode.z].m_parent.Value;
            }

            path.Reverse();

            return path;
        }
    }

    public sealed class PathfindingNode : IHeapItem<PathfindingNode> {

        int m_heapIndex = -1;

        public Vector3DInt m_position;
        public Vector3DInt m_direction;
        public Nullable<Vector3DInt> m_parent;

        public bool m_obstructed;

        public float m_gScore;
        public float m_hScore;
        public float m_fScore;

        public bool m_complete;

        public PathfindingNode(Vector3DInt _Position) {

            m_position = _Position;
            m_direction = new Vector3DInt(0, 0, 0);
            m_parent = null;

            m_obstructed = false;
            m_complete = false;

            m_gScore = Math.FloatInfinity;
            m_hScore = Math.FloatInfinity;
            m_fScore = Math.FloatInfinity;
        }

        public int m_HeapIndex {
            get {
                return m_heapIndex;
            }
            set {
                m_heapIndex = value;
            }
        }

        public int CompareTo(PathfindingNode _node) {
            int compare = m_fScore.CompareTo(_node.m_fScore);
            if (compare == 0) {
               compare = m_hScore.CompareTo(_node.m_hScore);
            }

            return -compare;
        }
    }
}
