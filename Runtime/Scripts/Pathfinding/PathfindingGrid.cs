using System.Collections.Generic;
using UnityEngine;

namespace PhantomTech.Navigation {

    public sealed class PathfindingGrid : MonoBehaviour {

        public Vector3 m_GridWorldSize = new Vector3(25.0f, 0.0f, 25.0f);
        public LayerMask m_Unwalkable;

        public int GridSizeX { get; private set; }
        public int GridSizeZ { get; private set; }
        public GridNode[,] Grid { get; private set; }

        public float m_NodeWidth = 0.2f;
        public float m_NodeThickness = 0.05f;

        public float m_CollisionYOffset = 0.7f;
        
        [Header("Debug")]
        [SerializeField] private bool m_DrawGrid = false;

        private Vector3D m_Centre;
        private List<Vector3D> m_Path;
        
        private Vector3D m_YOffsetVEC;
        private float m_NodeHalfWidth;

        private void Start() {
            m_NodeHalfWidth = m_NodeWidth / 2;
            m_YOffsetVEC = new Vector3D(0.0f, m_CollisionYOffset, 0.0f);
            
            GridSizeX = (int)(m_GridWorldSize.x / m_NodeWidth);
            GridSizeZ = (int)(m_GridWorldSize.z / m_NodeWidth);

            Grid = new GridNode[GridSizeX, GridSizeZ];
            
            for (int x = 0; x < GridSizeX; x++) {
                for (int z = 0; z < GridSizeZ; z++) {
                    Grid[x, z] = new GridNode(new Vector3DInt(x, 0, z));
                }
            }
        }

        private void Update() {
            if (PScene.Instance) {
                var newPosition = PScene.Instance.m_Device.transform.position;
                newPosition.y = transform.position.y;

                transform.position = newPosition;
                m_Centre = SnapToGrid(transform.position);
                
                m_YOffsetVEC = new Vector3D(0.0f, m_CollisionYOffset, 0.0f);

                for (int x = 0; x < GridSizeX; x++) {
                    for (int z = 0; z < GridSizeZ; z++) {
                        Grid[x, z].m_obstructed = Physics.CheckBox(GridToWorldPosition(x, z).m_Vector3 + m_YOffsetVEC.m_Vector3,
                            new Vector3(m_NodeHalfWidth, m_NodeThickness, m_NodeHalfWidth), new Quaternion(0, 0, 0, 1),
                            m_Unwalkable);
                    }
                }
            }
        }

#if UNITY_EDITOR

        private void OnDrawGizmos() {
            
            if (m_DrawGrid && Grid != null) {
                Gizmos.DrawWireCube(m_Centre.m_Vector3, new Vector3(GridSizeX * m_NodeWidth, 1, GridSizeZ * m_NodeWidth));
                
                for (int x = 0; x < GridSizeX; x++) {
                    for (int z = 0; z < GridSizeZ; z++) {
                        Gizmos.color = (!Grid[x, z].m_obstructed) ? new Color(0.0f, 1.0f, 0.0f, 0.1f) : new Color(1.0f, 0.0f, 0.0f, 0.2f);
                        if (Grid[x, z].m_jumpedsearched) { Gizmos.color = new Color(0.2f, 0.4f, 0.8f, 0.5f); }

                        Gizmos.DrawCube(GridToWorldPosition(x, z).m_Vector3 + m_YOffsetVEC.m_Vector3, new Vector3(m_NodeWidth, m_NodeThickness, m_NodeWidth));
                        Grid[x, z].m_jumpedsearched = false;
                    }
                }

                if (m_Path != null) {
                    int count = m_Path.Count;
                    for (int i = 0; i < count; i++) {
                        Gizmos.color = new Color(0.0f, 0.0f, 1.0f, 1.0f);
                    	Gizmos.DrawCube(m_Path[i].m_Vector3 + m_YOffsetVEC.m_Vector3, new Vector3(m_NodeWidth, m_NodeThickness, m_NodeWidth));
                	}
                }
            }
        }

#endif

        public GridNode GetNode(int _x, int _z) {
            int x = Math.Clamp(_x, 0, GridSizeX - 1);
            int z = Math.Clamp(_z, 0, GridSizeZ - 1);

            return Grid[x, z];
        }

        public GridNode GetNode(Vector3D _position) {

            _position = SnapToGrid(_position);

            int x = (int)Math.Clamp(Math.Round(((_position.x - m_Centre.x) / m_NodeWidth) + (GridSizeX / 2), 0), 0, GridSizeX - 1);
            int z = (int)Math.Clamp(Math.Round(((_position.z - m_Centre.z) / m_NodeWidth) + (GridSizeZ / 2), 0), 0, GridSizeZ - 1);

            return Grid[x, z];
        }

        public Vector3D GridToWorldPosition(int _x, int _z) {
            return m_Centre + (new Vector3D(_x * m_NodeWidth + m_NodeHalfWidth, 0, _z * m_NodeWidth + m_NodeHalfWidth) - (new Vector3D(GridSizeX / 2, 0, GridSizeZ / 2) * m_NodeWidth));
        }

        public Vector3D GridToWorldPosition(GridNode _node) {
            return m_Centre + (new Vector3D(_node.m_position.x * m_NodeWidth + m_NodeHalfWidth, 0, _node.m_position.z * m_NodeWidth + m_NodeHalfWidth) - (new Vector3D(GridSizeX / 2, 0, GridSizeZ / 2) * m_NodeWidth));
        }

        public Vector3D SnapToGrid(Vector3D _position) {
            
            Vector3D gridPoint = new Vector3D(
                (Math.Round(_position.x / m_NodeWidth, 0) * m_NodeWidth) - m_NodeHalfWidth,
                _position.y,
                (Math.Round(_position.z / m_NodeWidth, 0) * m_NodeWidth) - m_NodeHalfWidth
            );

            return gridPoint;
        }

        public void SetPath(List<Vector3D> _path) {
            m_Path = _path;
        }

        public bool CheckObstructed(Vector3D _position) {
            return GetNode(_position).m_obstructed;
        }
    }
    
    public struct GridNode {

        public Vector3DInt m_position;

        public bool m_obstructed;
        public bool m_jumpedsearched;

        public GridNode(Vector3DInt _position) {
            m_position = _position;
            m_obstructed = false;
            m_jumpedsearched = false;
        }
    }
}
