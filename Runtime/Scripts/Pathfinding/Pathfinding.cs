﻿using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Serialization;

namespace PhantomTech.Navigation {

    public sealed class Pathfinding : MonoBehaviour {

        private AStar m_AStar;

        [SerializeField] private PathfindingGrid m_Map;
        [SerializeField] private GameObject m_Target;
        
        /// <summary>
        /// Set the movement speed of the attached GameObject.
        /// </summary>
        public float m_Speed;

        /// <summary>
        /// Set the turn speed of the attached GameObject.
        /// </summary>
        public float m_RotationSpeed;

        /// <summary>
        /// Bool value describing if the pathfinding object is at it's target location.
        /// </summary>
        public bool AtTarget { get; private set; }

        /// <summary>
        /// Bool value describing if the pathfinding object is moving.
        /// </summary>
        public bool IsMoving { get; private set; }

        private bool m_IsPaused;
        private Vector3D m_Position;
        private List<Vector3D> m_Path;

        private Vector3D m_TargetPosition;

        private void Start() {
            if (m_Map == null) {
                m_Map = FindObjectOfType<PathfindingGrid>();
            }

            transform.position = new Vector3(transform.position.x, m_Map.transform.position.y, transform.position.z);
            m_AStar = new AStar(m_Map);
        }

        private void Update() {
            if (m_Target != null) {
                m_TargetPosition = new Vector3D(m_Target.transform.position.x, m_Map.transform.position.y, m_Target.transform.position.z);
            }

            if (m_TargetPosition != null) {
                if (!m_Map.CheckObstructed(transform.position) && !m_Map.CheckObstructed(m_TargetPosition)) {
                    m_Path = m_AStar.Solve(new Vector3D(transform.position), new Vector3D(m_TargetPosition));
                }
                else if (m_Map.CheckObstructed(transform.position)) {
                    if (m_Path == null) { m_Path = new List<Vector3D>(); }

                    if (m_Path.Count == 0) {
                        m_Path.Add(new Vector3D(m_TargetPosition.x, m_Map.transform.position.y, m_TargetPosition.z));
                    }
                }

                if (m_Path != null) {
                    Move();
                }
            }
        }

        private void Move() {

            if (!m_IsPaused) {

                if (m_Path.Count > 0) {

                    if (m_Speed > 0) { IsMoving = true; }
                    else { IsMoving = false; }
                    AtTarget = false;
                    
                    if (transform.position == m_Path[0].m_Vector3) {
                        m_Path.RemoveAt(0);
                    }

                    if (m_Path.Count > 0) {
                        Vector3 forwardVector = Vector3.Slerp(transform.forward, m_Path[0].m_Vector3 - transform.position, Time.deltaTime * m_RotationSpeed);
                        transform.forward = new Vector3(forwardVector.x, 0, forwardVector.z);
                        transform.position = PhantomTech.Math.MoveTowards(transform.position, m_Path[0], Time.deltaTime * m_Speed);
                    }
                }
                else {
                    AtTarget = true;
                    IsMoving = false;
                }
            }
            else {
                IsMoving = false;
            }
        }

        /// <summary>
        /// Set the target location to a fixed point for pathfinding with a position value.
        /// </summary>
        /// <param name="_position">Target position in world space.</param>
        public void SetTarget(Vector3 _position) {
            m_TargetPosition = new Vector3D(_position.x, m_Map.transform.position.y, _position.z);
            m_Target = null;
        }

        /// <summary>
        /// Set a target object for pathfinding to follow.
        /// </summary>
        /// <param name="_target">Target game object</param>
        public void SetTarget(GameObject _target) {
            m_Target = _target;
        }

        /// <summary>
        /// Pathfinder will stop moving towards its target.
        /// Path will continue to update while paused.
        /// </summary>
        public void Pause() {
            m_IsPaused = true;
        }

        /// <summary>
        /// Pathfinder will continue to follow its path.
        /// Path will continue to update.
        /// </summary>
        public void Resume() {
            m_IsPaused = false;
        }

        /// <summary>
        /// Toggles between pathfinding paused and unpaused states.
        /// Path will continue to update while paused.
        /// </summary>
        public void Toggle() {
            m_IsPaused = !m_IsPaused;
        }

        /// <summary>
        /// Returns the current path.
        /// Path updates frequently, so it is recommended to GetPath on Update() or LateUpdate().
        /// </summary>
        /// <returns></returns>
        public Vector3[] GetPath() {

            Vector3[] path = null;
            if (m_Path != null) {
                path = new Vector3[m_Path.Count + 1];
                path[0] = transform.position;

                for (int i = 1; i < path.Length; i++) {
                    path[i] = m_Path[i - 1].m_Vector3;
                }
            }

            return path;
        }

        public float GetDistanceToEnd() {
            return (m_TargetPosition.m_Vector3 - transform.position).magnitude;
        }
    }
}