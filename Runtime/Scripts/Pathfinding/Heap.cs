﻿using System;

namespace PhantomTech.Navigation {

    public class Heap<T> where T : IHeapItem<T>{

        private T[] m_Items;

        public int Count { get; private set; }

        public Heap(int _maxHeapSize) {
            m_Items = new T[_maxHeapSize];
        }

        public bool Contains(T _item) {
            return Equals(m_Items[_item.m_HeapIndex], _item);
        }

        public void Add(T _item) {

            if (_item.m_HeapIndex != -1) {
                m_Items[_item.m_HeapIndex] = _item;
                UpdateItem(_item);
            }
            else {
                _item.m_HeapIndex = Count;
                m_Items[Count] = _item;
                Count++;
                SortUp(_item);
            }
        }

        public T RemoveFirst() {

            T firstItem = m_Items[0];
            firstItem.m_HeapIndex = -1;
            Count--;

            if (Count > 0) {
                m_Items[0] = m_Items[Count];
                m_Items[0].m_HeapIndex = 0;
                m_Items[Count] = default(T);

                SortDown(m_Items[0]);
            } else {
                m_Items[0] = default(T);
            }

            return firstItem;
        }

        public void UpdateItem(T _item) {
            SortUp(_item);
            SortDown(_item);
        }

        public void Clear() {
            Array.Clear(m_Items, 0, m_Items.Length);
            Count = 0;
        }
        
        private void SortDown(T _item) {

            int childIndexLeft;
            int childIndexRight;
            int swapIndex = 0;

            while (true) { 

                childIndexLeft = _item.m_HeapIndex * 2 + 1;
                childIndexRight = _item.m_HeapIndex * 2 + 2;

                if (childIndexLeft < Count) {

                    swapIndex = childIndexLeft;

                    if (childIndexRight < Count) {
                        if (m_Items[childIndexLeft].CompareTo(m_Items[childIndexRight]) < 0) {
                            swapIndex = childIndexRight;
                        }
                    }

                    if (_item.CompareTo(m_Items[swapIndex]) < 0) {
                        Swap(_item, m_Items[swapIndex]);
                    }
                    else {
                        break;
                    }
                }
                else {
                    break;
                }
            }

            return;
        }

        private void SortUp(T _item) {

            int parentIndex = (_item.m_HeapIndex - 1) / 2;

            while (_item.CompareTo(m_Items[parentIndex]) > 0) {
                Swap(_item, m_Items[parentIndex]);
                parentIndex = (_item.m_HeapIndex-1)/2;
            }
        }

        private void Swap (T _itemA, T _itemB) {

            m_Items[_itemA.m_HeapIndex] = _itemB;
            m_Items[_itemB.m_HeapIndex] = _itemA;

            int itemAIndex = _itemA.m_HeapIndex;
            _itemA.m_HeapIndex = _itemB.m_HeapIndex;
            _itemB.m_HeapIndex = itemAIndex;
        }

        public T GetItem(int _index) {
            return m_Items[_index];
        }
    }
}

public interface IHeapItem<T> : IComparable<T>{
    int m_HeapIndex { get; set; }
}
