﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace PhantomTech {

    /// <summary>
    /// Adjusts an object's rendering queue to enable mesh-based occlusion.
    /// </summary>
    public sealed class RenderQueueCorrection : MonoBehaviour {
    
        private void Awake() {

            /* Changing the render queue so that everything behind
               the generated mesh (which is set to render queue 2001) gets occluded */
            const int newQueue = 2002;
            
            var renderer = GetComponent<Renderer>();

            if (renderer) {
            
                var mats = renderer.materials;

                for (int i = 0; i < mats.Length; i++) {
                    mats[i].renderQueue = newQueue;
                }
            }
        }
    }
}