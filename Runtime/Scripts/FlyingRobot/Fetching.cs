using PhantomTech.Navigation;
using UnityEngine;

namespace PhantomTech {
    
    public sealed class Fetching : MonoBehaviour {
        
        [SerializeField] private GameObject m_Hand;
        [SerializeField] private float m_FetchWaitTime;
        
        private Pathfinding m_Pathfinding;
        private GameObject m_FetchObject;
        private GameObject m_HeldObject;
        private GameObject m_Player;
        
        private float m_FetchCLK;
        
        private bool m_Holding;
        private bool m_Fetching;
        
        private void Start() {
            m_Pathfinding = GetComponent<Pathfinding>();
            m_Player = GameObject.FindWithTag("MainCamera");
        }

        private void Update() {
            if (m_Fetching) {
                if (m_FetchCLK > 0.0f) {
                    m_FetchCLK -= Time.deltaTime;
                }
                else {
                    if ((m_FetchObject.transform.position - transform.position).magnitude < 1f) {
                        PickUpObject(m_FetchObject);
                        m_Fetching = false;

                        m_Pathfinding.SetTarget(m_Player);
                    }
                }
            }
            if (m_Holding) {
                if ((new Vector2(transform.position.x, transform.position.z) - new Vector2(m_Player.transform.position.x, m_Player.transform.position.z)).magnitude < 0.5f) {
                    m_Pathfinding.SetTarget(transform.position);
                    DropObject();
                }
            }
        }

        public void PlayFetch(GameObject _fetchObject) {
            DropObject();
            m_FetchObject = _fetchObject;
            
            m_Pathfinding.SetTarget(m_FetchObject);
            m_Fetching = true;

            m_FetchCLK = m_FetchWaitTime;
        }

        private void PickUpObject(GameObject _object) {
            _object.GetComponent<Rigidbody>().isKinematic = true;
            _object.transform.position = m_Hand.transform.position;
            _object.transform.parent = transform;
            m_Holding = true;
            m_HeldObject = _object;
        }

        private void DropObject() {
            if (m_HeldObject) {
                m_HeldObject.GetComponent<Rigidbody>().isKinematic = false;
                m_HeldObject.transform.parent = null;
                m_HeldObject = null;
                m_Holding = false;
            }
        }
    }
}