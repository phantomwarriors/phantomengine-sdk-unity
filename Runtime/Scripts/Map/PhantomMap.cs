#if PHANTOMTECH_EXPERIMENTAL_MAP

using System.Collections;
using System.Collections.Generic;
using UnityEngine;

using Triangulator;

using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using System.Collections.Concurrent;
using System.Runtime.CompilerServices;

using static PhantomTech.Serialisation.OSMDeserialiser;

using Nodes  = System.Collections.Generic.Dictionary<ulong, PhantomTech.Serialisation.OSMDeserialiser.OSMJSON.Node>;
using Chunks = System.Collections.Generic.Dictionary<PhantomTech.Vector3D, PhantomTech.Location.PhantomMap.Chunk>;

namespace PhantomTech.Location {

    /// <summary>
    /// PhantomMap is an experimental feature that loads, deserialises, and spawns a 3D mesh using data from OpenStreetMap. 
    /// It requires an internet connection to work properly. 
    /// This feature is still in active development and hasn't been optimised.
    /// You may encounter bugs or other issues using this feature.
    /// We do not recommend that you use this feature in any contexts other than for bug testing or prototyping in a safe development environment.
    /// We do not claim any responsibilities for any damages (percieved, physical, accidental, or otherwise) using this experimental feature.
    /// Please report any bugs to the developer.
    /// </summary>
    [AddComponentMenu("PhantomEngine/Experimental/Phantom Map/Phantom Map")]
    public sealed class PhantomMap : MonoBehaviour {

        /* PUBLIC */
        public Transform m_Player;

        /* PRIVATE */
        [Header("Chunking")]
        [SerializeField] private int m_GridsAroundPlayer = 3;
        [SerializeField] private float m_GridSizeKm = 1f;

        [Header("Meshing")]
        [SerializeField] private float m_MapScale  = 0.05f;
        [SerializeField] private float m_RoadScale = 2.0f;

        [Header("Rendering")]
        [SerializeField] private string m_MapLayerName = "PhantomTech_Map";

#region Prefabs

        [Space]
        [SerializeField] private GameObject m_PointOfInterestPrefab;
        [SerializeField] private GameObject m_StreetNamePrefab;
        [SerializeField] private GameObject m_MapBackgroundPrefab;

#endregion Prefabs

#region Materials

        [Space]
        [SerializeField] private Material m_BackgroundMaterial;
        [SerializeField] private Material m_RoadMaterial;
        [SerializeField] private Material m_PathMaterial;
        [SerializeField] private Material m_RailMaterial;
        [SerializeField] private Material m_BuildingMaterial;
        [SerializeField] private Material m_BuildingSideMaterial;
        [SerializeField] private Material m_VegetationMaterial;
        [SerializeField] private Material m_ForestMaterial;
        [SerializeField] private Material m_SandMaterial;
        [SerializeField] private Material m_MudMaterial;
        [SerializeField] private Material m_AsphaltMaterial;
        [SerializeField] private Material m_RockMaterial;
        [SerializeField] private Material m_WaterMaterial;
        [SerializeField] private Material m_IceMaterial;
        [SerializeField] private Material m_MetalMaterial;
        [SerializeField] private Material m_WoodMaterial;

#endregion Materials

        private Chunks m_Chunks = new Chunks();

        public class Chunk {

            private readonly string[] s_POITags = new string[] {
                "amenity:community_centre",
                "amenity:fountain",
                "amenity:planetarium",
                "amenity:social_centre",
                "amenity:ranger_station",
                "amenity:townhall",
                "amenity:shelter",
                "amenity:marketplace",
                "building:civic",
                "building:conservatory",
                "historic:aircraft",
                "historic:aqueduct",
                "historic:bomb_crater",
                "historic:building",
                "historic:castle",
                "historic:castle_wall",
                "historic:church",
                "historic:citywalls",
                "historic:locomotive",
                "historic:manor",
                "historic:memorial",
                "historic:milestone",
                "historic:monastery",
                "historic:monument",
                "historic:pillory",
                "historic:railway_car",
                "historic:ruins",
                "historic:rune_stone",
                "historic:ship",
                "historic:tank",
                "historic:tower",
                "landuse:allotments",
                "landuse:forest",
                "landuse:grass",
                "landuse:recreation_ground",
                "landuse:village_green",
                "leisure:amusement_arcade",
                "leisure:beach_resort",
                "leisure:bandstand",
                "leisure:bird_hide",
                "leisure:common",
                "leisure:dog_park",
                "leisure:fishing",
                "leisure:fitness_station",
                "leisure:garden",
                "leisure:marina",
                "leisure:miniature_golf",
                "leisure:nature_reserve",
                "leisure:park",
                "leisure:water_park",
                "man_made:lighthouse",
                "man_made:obelisk",
                "man_made:pier",
                "man_made:survey_point",
                "man_made:tower",
                "man_made:watermill",
                "man_made:water_well",
                "man_made:windmill",
                "natural:wood",
                "natural:beach",
                "natural:rock",
                "natural:stone",
                "tourism:alpine_hut",
                "tourism:aquarium",
                "tourism:artwork",
                "tourism:attraction",
                "tourism:camp_pitch",
                "tourism:camp_site",
                "tourism:caravan_site",
                "tourism:chalet",
                "tourism:gallery",
                "tourism:information",
                "tourism:museum",
                "tourism:theme_park",
                "tourism:viewpoint",
                "tourism:wilderness_hut",
                "tourism:zoo"
            };

            public Nodes m_Nodes;

            public Transform m_Transform;

            public Vector3D m_Position;

            public GameObject m_Background;

            public Category m_Roads;
            public Category m_Buildings;
            public Category m_Landuse;
            public Category m_Natural;
            public Category m_Waterways;
            public Category m_Surfaces;
            public Category m_Ocean;
            public Category m_POIs;
            public Category m_Uncategorised;

            public Chunk(Transform _map, Vector3D _position, string _name, Material _background, float _sizeM, int _layer, bool _showBackground = true) {

                m_Transform = SetLayer(_map.GetOrAddChild(_name), _layer).transform;

                m_Background = GameObject.CreatePrimitive(PrimitiveType.Quad);
                m_Background.name = "Background";
                m_Background.layer = _layer;

                var renderer = m_Background.GetComponent<Renderer>();
                renderer.material = _background;

                PhantomTech.Meshing.Meshing.DisableComplexRendering(renderer, UnityEngine.Rendering.ShadowCastingMode.Off, true);

                m_Background.transform.parent = m_Transform;
                m_Background.transform.position = _position.Parse<Vector3>();

                m_Background.transform.forward = new Vector3(0, -1f, 0);
                m_Background.transform.localScale = new Vector3(_sizeM, _sizeM, 1);

                m_Background.SetActive(_showBackground);

                m_Roads = new Category(m_Transform, "Roads", _layer);
                m_Buildings = new Category(m_Transform, "Buildings", _layer);
                m_Landuse = new Category(m_Transform, "Landuse", _layer);
                m_Natural = new Category(m_Transform, "Natural", _layer);
                m_Waterways = new Category(m_Transform, "Waterways", _layer);
                m_Surfaces = new Category(m_Transform, "Surfaces", _layer);
                m_Ocean = new Category(m_Transform, "Ocean", _layer);
                m_POIs = new Category(m_Transform, "POIs", _layer);
                m_Uncategorised = new Category(m_Transform, "Uncategorised", _layer);
            }

            public class Category {

                public Transform m_Transform;

                public Dictionary<GameObject, OSMJSON.Node> m_Elements;

                public Category(Transform _map, string _name, int _layer) {
                    m_Transform = SetLayer(_map.GetOrAddChild(_name), _layer).transform;
                }

                public static implicit operator Transform(Category _c) => _c.m_Transform;
                public static implicit operator Dictionary<GameObject, OSMJSON.Node>(Category _c) => _c.m_Elements;
            }

            [MethodImpl(MethodImplOptions.AggressiveInlining)]
            public Nodes GetPOIs() {

                Nodes result = new Nodes();

                if (m_Nodes != null) {

                    foreach (var item in m_Nodes.Values) {

                        if (item.tags != null) {

                            foreach (var poi in s_POITags) {

                                string[] kvp = poi.Split(':');

                                if (item.tags.ContainsKey(kvp[0]) && item.tags[kvp[0]] == kvp[1]) {

                                    if (result.ContainsKey(item.id) == false) {
                                        result.Add(item.id, item);
                                    }
                                }
                            }
                        }
                    }
                }

                return result;
            }
        }

        /* CACHED */
        private int m_MapLayer;
        private bool m_InterpolatingPlayer = false;

        private GlobalPositioningService.PositionUpdatedArgs m_LastPosition;

        /* THREADING */
        private static ConcurrentDictionary<int, Task> s_PopulationTasks = new ConcurrentDictionary<int, Task>();

        private struct MapElement {

            public ulong m_ID { get; private set; }

            public float m_Width { get; private set; }

            public Transform m_Parent  { get; private set; }
            public PhantomMesh m_Mesh  { get; private set; }

            public Material m_Material { get; private set; }

            public ElementType m_Type  { get; private set; }

            public MapElement(ulong _id, Transform _parent, PhantomMesh _mesh, Material _material, ElementType _type, float _width = 0) {

                m_ID = _id;

                m_Width = _width;

                m_Parent   = _parent;
                m_Mesh     = _mesh;
                m_Material = _material;
                m_Type     = _type;
            }
        }

        private enum ElementType { 
            Way,
            Area,
            Walls,
            POI
        }

        private void Start() {

            // Get the integer representation of the map layer.
            m_MapLayer = LayerMask.NameToLayer(m_MapLayerName);

            PhantomTech.Location.GlobalPositioningService.PositionUpdated += OnPositionUpdated;
        }

        /// <summary>
        /// Called when the "PositionUpdated" event is raised.
        /// </summary>
        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        private void OnPositionUpdated(GlobalPositioningService.PositionUpdatedArgs _args) {

            if (isActiveAndEnabled) { 
            
                Vector3D chunkCenter = _args.position.m_LatLongAlt;

                UpdatePlayer(chunkCenter, Coordinates.s_CurrentHeading, 1f);

                if (m_LastPosition != null) {

                    const double  timeThreshold = 5.0;
                    const double spaceThreshold = 0.001;

                    var deltaTime  = _args.position.m_Timestamp  - m_LastPosition.position.m_Timestamp;
                    var deltaSpace = chunkCenter - m_LastPosition.position.m_LatLongAlt;
                    deltaSpace.z = 0;

                    var deltaDistance = PhantomTech.Math.SqrMagnitude(deltaSpace);

                    if (deltaTime     <  timeThreshold ||
                        deltaDistance < spaceThreshold)
                    {
                        return;
                    }
                }

#if UNITY_EDITOR
                if (Application.isPlaying == true) {
#endif
                    var halfSide = 500f * m_GridSizeKm;

                    float latitudeRadians  = PhantomTech.Math.DegreesToRadiansUnwrapped(chunkCenter.x);
                    float longitudeRadians = PhantomTech.Math.DegreesToRadiansUnwrapped(chunkCenter.y);

                    var radius  = OSM.WGS84.WGS84EarthRadius(_args.position.m_LatLongAlt.x); // Coordinates.s_EarthRadius;
                    var pradius = radius * PhantomTech.Math.Cos(latitudeRadians);

                    float factorLat = halfSide /  radius;
                    float factorLon = halfSide / pradius;

                    int max = m_GridsAroundPlayer / 2, min = -max;

                    float scalar = 1.2f; // 1.267f;

                    for (int x = min; x <= max; ++x) {
                        for (int y = min; y <= max; ++y) {

                            chunkCenter = new Vector3D(
                                PhantomTech.Math.RadiansToDegreesUnwrapped(latitudeRadians + (factorLat * y * scalar)),
                                PhantomTech.Math.RadiansToDegreesUnwrapped(longitudeRadians + (factorLon * x * scalar)),
                                0
                            );

                            LoadChunk(chunkCenter, m_GridSizeKm);
                        }
                    }
#if UNITY_EDITOR
                }
#endif
                m_LastPosition = _args;
            }
        }

        /// <summary>
        /// Updates the position of the player on the map.
        /// </summary>
        /// <param name="_useSmoothing">Determines the amount of time to interpolate the player's motion over. Higher values have smoother motion, but are less responsive.</param>
        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        private void UpdatePlayer(Vector3D _latLongAlt, float _heading, float _smoothingTime = 0f) {

            var newPosition = (GPSToMap(_latLongAlt) + PhantomTech.Math.Up3).Parse<Vector3>();
            var newRotation = Quaternion.Euler(0, _heading, 0);

            if (_smoothingTime > 0f) {

                if (m_InterpolatingPlayer == false) {

                    StartCoroutine(
                        InterpolatePlayer(
                            newPosition,
                            newRotation,
                            _smoothingTime
                        )
                    );
                }
            }
            else {
                m_Player.position = newPosition;
                m_Player.transform.rotation = newRotation;
            }
        }

        /// <summary>
        /// Coroutine that smoothly moves the player from its current position and rotation to new ones.
        /// </summary>
        /// <param name="_time">The time over which to smooth the movement.</param>
        private IEnumerator InterpolatePlayer(Vector3 _targetPos, Quaternion _targetRot, float _time) {

            m_InterpolatingPlayer = true;

            float startTime = Time.time;
            float endTime   = startTime + _time;

            while (Time.time < endTime) {

                yield return null;

                float timeElapsed = Time.time - startTime;
                float progress    = (timeElapsed / _time);

                m_Player.position = Vector3.SlerpUnclamped(m_Player.position, _targetPos, progress);
                m_Player.rotation = Quaternion.SlerpUnclamped(m_Player.rotation, _targetRot, progress);
            }

            m_InterpolatingPlayer = false;
        }
        
        /// <summary>
        /// Populates a chunk with data using OpenStreetMap nodes.
        /// </summary>
        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        private IEnumerator PopulateChunkAsync(Chunk _chunk, Nodes _nodes) {

            if (_nodes != null && _nodes.Count != 0) {

                var elements = new ConcurrentBag<MapElement>();

                var task = Task.Factory.StartNew(() => {

                    Thread.CurrentThread.Priority = System.Threading.ThreadPriority.AboveNormal;

                    foreach (var item in _nodes.Values) {

                        if (item.nodes != null && item.tags != null) {

                            float height = 0.0f;

                            Transform category = null;
                            Material mat = null;

#region Try Get Height

                            // Attempt to get the height directly.
                            if (item.tags.ContainsKey("height")) {
                                float.TryParse(item.tags["height"].TrimEnd('m').Trim(), out height);
                            }

#endregion

#region Try Get Name

                            string name;

                            if (item.tags.ContainsKey("name")) {
                                name = item.tags["name"];
                            }
                            else {
                                name = string.Empty;
                            }

#endregion

                            // Project node positions.
                            List<Vector3D> nodes = NodesToWorld(_nodes, item.nodes).ToList();

                            if (IsClockwise(nodes.ToArray()) == false) {
                                nodes.Reverse();
                            }

                            if (nodes.Count >= 2 && item.tags.ContainsKey("natural") && item.tags["natural"] == "coastline") {
                                // Something to do with ocean rendering.
                            }
                            else {

                                if (IsArea(nodes) && nodes.Count >= 4) {

                                    // Remove the last element from the area.
                                    nodes.RemoveAt(nodes.Count() - 1);

                                    // CREATE BUILDING MESH
                                    if (item.tags.ContainsKey("building")) {

#region Determine Building Height

                                        if (height == 0f) {

                                            const float averageStoreyHeight = 3.048f;
                                            const float defaultHeight = averageStoreyHeight * 3;

                                            if (item.tags.ContainsKey("building:levels")) {

                                                if (int.TryParse(item.tags["building:levels"], out int levels)) {
                                                    height = levels * averageStoreyHeight;
                                                }
                                                else {
                                                    height = defaultHeight;
                                                }
                                            }
                                            else {
                                                height = defaultHeight;
                                            }
                                        }

                                        height *= m_MapScale;

#endregion Determine Building Height

                                        category = _chunk.m_Buildings;
                                        mat = m_BuildingMaterial;

                                        // Create a parent for the building's roof and wall meshes.
                                        var roof = CreateArea(
                                            nodes.ToArray(),
                                            height,
                                            mat,
                                            category,
                                            "Roof",
                                            item.id
                                        );

                                        if (roof.m_Mesh.m_Vertices != null && roof.m_Mesh.m_Vertices.Length != 0) {

                                            var roofMesh = roof.m_Mesh;

                                            elements.Add(roof);

                                            /* CREATE WALLS */
                                            /* SURFACE */
                                            elements.Add(CreateWalls(roofMesh.m_Vertices, -height, m_BuildingSideMaterial, category, "Walls", item.id));
                                            /* SHADOW  */
                                            //elements.Add(CreateWalls(roofMesh.m_Vertices, -height, m_BuildingSideMaterial, category, "Shadow", item.id));
                                        }
                                    }
                                    else {

                                        // CREATE LANDUSE REGIONS
                                        if (item.tags.ContainsKey("landuse")) {

                                            height = 0.2f;

#region Determine Landuse Type

                                            string value = item.tags["landuse"];

                                            if      (value.StartsWith("allotments"))        { mat = m_VegetationMaterial; }
                                            else if (value.StartsWith("farmland"))          { mat = m_VegetationMaterial; }
                                            else if (value.StartsWith("flowerbed"))         { mat = m_VegetationMaterial; }
                                            else if (value.StartsWith("forest"))            { mat = m_VegetationMaterial; }
                                            else if (value.StartsWith("meadow"))            { mat = m_VegetationMaterial; }
                                            else if (value.StartsWith("orchard"))           { mat = m_VegetationMaterial; }
                                            else if (value.StartsWith("vineyard"))          { mat = m_VegetationMaterial; }
                                            else if (value.StartsWith("basin"))             { mat = m_WaterMaterial;      }
                                            else if (value.StartsWith("cemetary"))          { mat = m_VegetationMaterial; }
                                            else if (value.StartsWith("grass"))             { mat = m_VegetationMaterial; }
                                            else if (value.StartsWith("plant_nursery"))     { mat = m_VegetationMaterial; }
                                            else if (value.StartsWith("recreation_ground")) { mat = m_VegetationMaterial; }
                                            else if (value.StartsWith("reservoir"))         { mat = m_WaterMaterial;      }
                                            else if (value.StartsWith("village_green"))     { mat = m_VegetationMaterial; }

#endregion Determine Landuse Type

                                            category = _chunk.m_Landuse;
                                        }

                                        // CREATE NATURAL REGIONS
                                        else if (item.tags.ContainsKey("natural")) {

                                            height = 0.075f;

#region Determine Nature Type

                                            string value = item.tags["natural"];

                                                 if (value.StartsWith("wood"))       { mat = m_ForestMaterial;     }
                                            else if (value.StartsWith("tree_row"))   { mat = m_ForestMaterial;     }
                                            else if (value.StartsWith("tree"))       { mat = m_ForestMaterial;     }
                                            else if (value.StartsWith("scrub"))      { mat = m_VegetationMaterial; }
                                            else if (value.StartsWith("heath"))      { mat = m_VegetationMaterial; }
                                            else if (value.StartsWith("grassland"))  { mat = m_VegetationMaterial; }
                                            else if (value.StartsWith("fell"))       { mat = m_VegetationMaterial; }
                                            else if (value.StartsWith("bare_rock"))  { mat = m_RockMaterial;       }
                                            else if (value.StartsWith("scree"))      { mat = m_RockMaterial;       }
                                            else if (value.StartsWith("shingle"))    { mat = m_RockMaterial;       }
                                            else if (value.StartsWith("sand"))       { mat = m_SandMaterial;       }
                                            else if (value.StartsWith("mud"))        { mat = m_MudMaterial;        }
                                            else if (value.StartsWith("water"))      { mat = m_WaterMaterial;      }
                                            else if (value.StartsWith("wetland"))    { mat = m_WaterMaterial;      }
                                            else if (value.StartsWith("glacier"))    { mat = m_IceMaterial;        }
                                            else if (value.StartsWith("bay"))        { mat = m_WaterMaterial;      }
                                            else if (value.StartsWith("strait"))     { mat = m_WaterMaterial;      }
                                            else if (value.StartsWith("cape"))       { mat = m_RockMaterial;       }
                                            else if (value.StartsWith("beach"))      { mat = m_SandMaterial;       }
                                            else if (value.StartsWith("coastline"))  { mat = m_WaterMaterial;      }
                                            else if (value.StartsWith("spring"))     { mat = m_WaterMaterial;      }
                                            else if (value.StartsWith("hot_spring")) { mat = m_WaterMaterial;      }
                                            else if (value.StartsWith("geyser"))     { mat = m_WaterMaterial;      }
                                            else if (value.StartsWith("blowhole"))   { mat = m_WaterMaterial;      }

#endregion Determine Nature Type

                                            category = _chunk.m_Natural;
                                        }

                                        // DEFINE AREA FROM SURFACE
                                        if (mat == null) {

                                            if (item.tags.ContainsKey("surface")) {

                                                if (!TryGetSurface(item.tags["surface"], out mat)) {
                                                    mat = m_RockMaterial;
                                                }

                                                height = 0.1f;
                                                category = _chunk.m_Surfaces;
                                            }
                                            else {

                                                mat = m_RockMaterial;

                                                height = 0.11f;
                                                category = _chunk.m_Uncategorised;
                                            }
                                        }

                                        elements.Add(CreateArea(
                                            nodes.ToArray(),
                                            height,
                                            mat,
                                            category,
                                            item.id.ToString(),
                                            item.id
                                        ));
                                    }
                                }
                                else {

                                    float? width = null;

                                    // CREATE WATERWAY REGIONS
                                    if (item.tags.ContainsKey("waterway")) {

                                        mat = m_WaterMaterial;

#region Determine Waterway Width

                                        if (item.tags.ContainsKey("width")) {
                                            if (float.TryParse(item.tags["width"], out float val)) {
                                                width = val;
                                            }
                                        }

                                        // https://wiki.openstreetmap.org/wiki/Key:CEMT?uselang=en-GB#Waterways
                                        else if (item.tags.ContainsKey("CEMT")) {

                                            string value = item.tags["CEMT"];

                                            // Unofficial (NL only)
                                            if (value == "0") { width = 5.05f; }

                                            // Official
                                            else if (value == "I")   { width = 5.05f; }
                                            else if (value == "II")  { width = 6.6f; }
                                            else if (value == "III") { width = 8.2f; }
                                            else if (value == "IV")  { width = 9.5f; }
                                            else if (value == "Va")  { width = 11.4f; }
                                            else if (value == "Vb")  { width = 11.4f; }
                                            else if (value == "VIa") { width = 22.8f; }
                                            else if (value == "VIb") { width = 22.8f; }
                                            else if (value == "VIc") { width = 34.2f; }
                                            else if (value == "VII") { width = 34.2f; }

                                            else if (value == "RA") { width = 2.0f; }
                                            else if (value == "RB") { width = 3.0f; }
                                            else if (value == "RC") { width = 4.0f; }
                                            else if (value == "RD") { width = 4.0f; }
                                        }

                                        // https://wiki.openstreetmap.org/wiki/User:Jeisenbe/Waterway_Widths
                                        // Ditches, drains, and streams:
                                        else if (item.tags["waterway"] == "ditch")  { width = 2.0f; }
                                        else if (item.tags["waterway"] == "drain")  { width = 2.0f; }
                                        else if (item.tags["waterway"] == "stream") { width = 2.0f; }

                                        // Rivers:
                                        else if (item.tags["waterway"] == "river") { width = 3.0f; }

                                        // Canals:
                                        else if (item.tags["waterway"] == "canal") { width = 2.0f; }

#endregion Determine Waterway Width

                                        if (width.HasValue) {

                                            height = 2f;

                                            const float waterwayWidthUpscale = 5.0f;

                                            width *= m_MapScale * waterwayWidthUpscale;

                                            category = _chunk.m_Waterways;
                                        }
                                    }

                                    // CREATE ROAD MESH (Using LineRenderer)
                                    else if (item.tags.ContainsKey("highway") || item.tags.ContainsKey("footway") || item.tags.ContainsKey("railway")) {

#region Determine Road Width

                                        if (item.tags.ContainsKey("railway")) {

                                            mat = m_RailMaterial;
                                            width = 4f;

                                        }
                                        else if (item.tags.ContainsKey("footway")) {

                                            string value = item.tags["footway"];

                                                 if (value.StartsWith("sidewalk")) { mat = m_PathMaterial; width = 2f; }
                                            else if (value.StartsWith("crossing")) { mat = m_PathMaterial; width = 2f; }

                                        }
                                        else if (item.tags.ContainsKey("highway")) {

                                            string value = item.tags["highway"];

                                                 if (value.StartsWith("motorway"))      { mat = m_RoadMaterial; width = 12f; }
                                            else if (value.StartsWith("trunk"))         { mat = m_RoadMaterial; width = 10f; }
                                            else if (value.StartsWith("primary"))       { mat = m_RoadMaterial; width = 8f; }
                                            else if (value.StartsWith("secondary"))     { mat = m_RoadMaterial; width = 7f; }
                                            else if (value.StartsWith("tertiary"))      { mat = m_RoadMaterial; width = 6f; }
                                            else if (value.StartsWith("unclassified"))  { mat = m_RoadMaterial; width = 5f; }
                                            else if (value.StartsWith("residential"))   { mat = m_RoadMaterial; width = 5f; }
                                            else if (value.StartsWith("living_street")) { mat = m_RoadMaterial; width = 5f; }
                                            else if (value.StartsWith("service"))       { mat = m_RoadMaterial; width = 3f; }
                                            else if (value.StartsWith("pedestrian"))    { mat = m_PathMaterial; width = 5f; }
                                            else if (value.StartsWith("track"))         { mat = m_PathMaterial; width = 2f; }
                                            else if (value.StartsWith("bus_guideway"))  { mat = m_RoadMaterial; width = 2.5f; }
                                            else if (value.StartsWith("escape"))        { mat = m_RoadMaterial; width = 3.5f; }
                                            else if (value.StartsWith("raceway"))       { mat = m_RoadMaterial; width = 3.7f; }
                                            else if (value.StartsWith("road"))          { mat = m_RoadMaterial; width = 3.7f; }
                                            else if (value.StartsWith("busway"))        { mat = m_RoadMaterial; width = 5f; }
                                            else if (value.StartsWith("cycleway"))      { mat = m_PathMaterial; width = 5f; }

                                            if (width.HasValue && value.EndsWith("_link")) {
                                                width /= 2;
                                            }
                                        }

#endregion Determine Road Width

                                        if (width.HasValue) {

#region Determine Road Height

                                                 if (mat == m_RoadMaterial) {
                                                height += 7.5f;
                                            }
                                            else if (mat == m_RailMaterial) {
                                                height += 7.75f;
                                            }
                                            else {
                                                height += 8f;
                                            }

#endregion Determine Road Height

                                            width *= m_RoadScale * m_MapScale;

                                            category = _chunk.m_Roads;
                                        }
                                    }

                                    if (width.HasValue) {

                                        elements.Add(CreateWay(
                                            nodes.ToArray(),
                                            width.Value,
                                            height,
                                            mat,
                                            category,
                                            name,
                                            item.id
                                        ));
                                    }
                                }
                            }
                        }
                    }

                    var pois = _chunk.GetPOIs();

                    foreach (var node in pois) {
                        var position = NodeToWorld(pois, node.Key);

                        elements.Add(CreatePOI(position, _chunk.m_POIs, node.Key.ToString(), node.Key));
                    }
                });

                int UID;

                lock (s_PopulationTasks) {
                    UID = s_PopulationTasks.Count;
                }

                if (s_PopulationTasks.TryAdd(UID, task)) {

                    yield return new WaitUntil(() => task.IsCompleted);

                    if (s_PopulationTasks.TryRemove(UID, out var t) == false) {
                        PhantomTech.Debugging.Debug.Log(string.Format("ERROR (PhantomMap.cs): Removing population task \"{0}\" from pool of managed tasks failed!", UID), Debugging.Debug.LogType.Error);
                    }

                    // Spawn elements using time slicing. Every 'timeSliceFactor' iterations, the loop is sliced.
                    int count = 0;
                    int timeSliceFactor = 10;

                    foreach (var element in elements) {
                        
                        SpawnElement(element);

                        count++;
                        if (count >= timeSliceFactor) {
                            count = 0;

                            yield return null;
                        }
                    }
                }
            }
        }

        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        private void SpawnElement(MapElement _mapElement) {

            if (_mapElement.m_Mesh.m_Vertices != null && _mapElement.m_Mesh.m_Vertices.Length != 0) {

                // Create GameObject.
                var name   = _mapElement.m_Mesh.m_Name;
                var root   = SetLayer(_mapElement.m_Parent.transform.AddChild(name), m_MapLayer);
                var points = _mapElement.m_Mesh.m_Vertices.Select(point => point.Parse<Vector3>()).ToArray();

                switch (_mapElement.m_Type) {

                    case ElementType.Way: {

                        float width = _mapElement.m_Width;

#region Create Line

                        for (int i = 0; i < points.Length - 1; i++) {

                            var segment = SetLayer(root.transform.AddChild(i.ToString()), m_MapLayer);
                            var lineRenderer = segment.AddComponent<LineRenderer>();

                            segment.transform.forward = new Vector3(0, -1, 0);
                            lineRenderer.alignment = LineAlignment.TransformZ;

                            lineRenderer.textureMode = LineTextureMode.Tile;
                            lineRenderer.material    = _mapElement.m_Material;
                            lineRenderer.positionCount = 2;

                            lineRenderer.startWidth = width;
                            lineRenderer.endWidth   = width;

                            lineRenderer.numCapVertices    = 0;
                            lineRenderer.numCornerVertices = 0;

                            lineRenderer.SetPosition(0, points[  i  ]);
                            lineRenderer.SetPosition(1, points[i + 1]);

                            PhantomTech.Meshing.Meshing.DisableComplexRendering(lineRenderer, UnityEngine.Rendering.ShadowCastingMode.Off, true);
                        }

#endregion Create Line

#region Create Name

                        if (string.IsNullOrEmpty(name) == false) {

                            Vector3 center = points[0];
                            Vector3 dir = new Vector3(1, 0, 0);

                            float sqrLength = 0;

                            for (int i = 0; i < points.Length - 1; ++i) {
                                sqrLength += (points[i] - points[i + 1]).sqrMagnitude;
                            }

                            if (sqrLength != 0 && sqrLength * m_MapScale > 0.2f) {

                                // Find midpoint of way.
                                if (points.Length == 2) {

                                    dir = points[1] - points[0];

                                    center = points[0] + (dir * 0.5f);
                                }
                                else {

                                    // Not sure if this is correct, but it looks ok on the map.
                                    float totalSqrDist = 0f;
                                    float halfSqrLength = sqrLength * 0.5f;

                                    for (int i = 0; i < points.Length - 1; i++) {

                                        dir = points[i + 1] - points[i];

                                        float sqrMag = dir.sqrMagnitude;
                                        float newTotalSqrDist = totalSqrDist + sqrMag;

                                        if (totalSqrDist < halfSqrLength &&
                                            newTotalSqrDist > halfSqrLength) {

                                            float mag = PhantomTech.Math.Sqrt(sqrMag);
                                            float del = PhantomTech.Math.Sqrt(newTotalSqrDist - halfSqrLength) / mag / 2;

                                            center = Vector3.LerpUnclamped(points[i + 1], points[i], del);

                                            break;
                                        }
                                        else {
                                            totalSqrDist = newTotalSqrDist;
                                        }
                                    }

                                }

                                root = SetLayer(GameObject.Instantiate(m_StreetNamePrefab, center, new Quaternion(), root.transform), m_MapLayer);
                                root.transform.right = dir;

                                var text = root.GetComponent<TextMesh>();
                                text.text = name;
                                text.characterSize = width * 0.1f;

                                float flip = 0f;

                                if (dir.x < 0) {
                                    flip = 180f;
                                }

                                root.transform.Rotate(90, 0, flip);
                            }
                        }

#endregion Create Name

                        break;
                    }
                    case ElementType.Area: {

                        if (TrySpawnMesh(_mapElement.m_Mesh, root, _mapElement.m_Material, out MeshFilter filter, out MeshRenderer renderer)) {
                            PhantomTech.Meshing.Meshing.DisableComplexRendering(renderer, UnityEngine.Rendering.ShadowCastingMode.Off, true);
                        }

                        break;
                    }
                    case ElementType.Walls: {
                        
                        if (TrySpawnMesh(_mapElement.m_Mesh, root, _mapElement.m_Material, out MeshFilter filter, out MeshRenderer renderer)) {
                            PhantomTech.Meshing.Meshing.DisableComplexRendering(renderer, UnityEngine.Rendering.ShadowCastingMode.On, true);
                        }

                        break;
                    }
                    case ElementType.POI: {
                        GameObject.Instantiate(m_PointOfInterestPrefab, _mapElement.m_Mesh.m_Vertices[0].Parse<Vector3>(), new Quaternion(), root.transform);

                        break;
                    }

                    default: {
                        throw new System.NotImplementedException("ERROR(PhantomMap.cs): The selected ElementType has not been implemented in SpawnElement()");
                    }
                }
            }
        }

        /// <summary>
        /// Creates a 'way' using the provided information.
        /// </summary>
        /// <param name="_nodes">The nodes forming the way.</param>
        /// <param name="_width">The width of the way, in metres.</param>
        /// <param name="_height">The height of the way above the ground, in metres/</param>
        /// <param name="_mat">The material to give the way.</param>
        /// <param name="_parent">What object to anchor the way to (null if none).</param>
        /// <param name="_name">What name to give the way.</param>
        /// <param name="_id">The unique ID of the way.</param>
        /// <param name="_id">ID of the OSM element this MapElement references.</param>
        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        private MapElement CreateWay(Vector3D[] _nodes, float _width, float _height, Material _mat, Transform _parent, string _name, ulong _id) {

            PhantomMesh mesh = new PhantomMesh(_name);

            if (_nodes != null && _nodes.Length != 0) {

                var elevation = new Vector3D(0, _height * m_MapScale, 0);

                for (int i = 0; i < _nodes.Length; ++i) {
                    _nodes[i] += elevation;
                }

                mesh.SetVertices(_nodes);
            }

            return new MapElement(_id, _parent, mesh, _mat, ElementType.Way, _width);
        }

        /// <summary>
        /// Creates an 'area' using the provided information.
        /// </summary>
        /// <param name="_nodes">The nodes forming the area.</param>
        /// <param name="_height">The height of the area above the ground, in metres.</param>
        /// <param name="_mat">The material to give the area.</param>
        /// <param name="_parent">What object to anchor the area to (null if none).</param>
        /// <param name="_name">What name to give the area.</param>
        /// <param name="_id">ID of the OSM element this MapElement references.</param>
        /// <returns></returns>
        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        private MapElement CreateArea(Vector3D[] _nodes, float _height, Material _mat, Transform _parent, string _name, ulong _id) {

            // Create area mesh.
            PhantomMesh mesh = MeshFromPerimeter(_nodes, _height, _name);

            return new MapElement(_id, _parent, mesh, _mat, ElementType.Area);
         }

        /// <summary>
        /// Creates 'walls' using the provided information.
        /// </summary>
        /// <param name="_nodes">The nodes forming the walls.</param>
        /// <param name="_height">The height of the walls above the ground, in metres.</param>
        /// <param name="_mat">The material to give the walls.</param>
        /// <param name="_parent">What object to anchor the walls to (null if none).</param>
        /// <param name="_name">What name to give the walls.</param>
        /// <param name="_id">ID of the OSM element this MapElement references.</param>
        /// <returns></returns>
        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        private MapElement CreateWalls(Vector3D[] _nodes, float _height, Material _mat, Transform _parent, string _name, ulong _id, bool _doubleSided = false) {

            PhantomMesh mesh = Extrude(_nodes, new Vector3D(0, _height, 0), _name, _doubleSided);

            return new MapElement(_id, _parent, mesh, _mat, ElementType.Walls);
        }

        /// <summary>
        /// Creates a POI using the provided information.
        /// </summary>
        /// <param name="_position">Position of the POI in engine space.</param>
        /// <param name="_parent">What object to anchor the POI to (null if none).</param>
        /// <param name="_name">What name to give the POI.</param>
        /// <param name="_id">ID of the OSM element this MapElement references.</param>
        /// <returns></returns>
        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        private MapElement CreatePOI(Vector3D _position, Transform _parent, string _name, ulong _id) {

            PhantomMesh mesh = new PhantomMesh(_name);
            mesh.SetVertices(new Vector3D[] {_position});

            return new MapElement(_id, _parent, mesh, null, ElementType.POI);
        } 

        /// <summary>
        /// Queries OpenStreetMap for data in a box around a coordinate and loads the data into the map as a chunk.
        /// </summary>
        /// <param name="_latLong">The center coordinate of the bounding box.</param>
        /// <param name="_sizeKm">The width, and height of the bounding box (For best accuracy, choose a value below 10km).</param>
        /// <returns></returns>
        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        private Chunk LoadChunk(Vector3D _latLong, float _sizeKm) {

            var result = new Chunk(transform, GPSToMap(_latLong), _latLong.ToString(), m_BackgroundMaterial, _sizeKm * 1000 * m_MapScale, m_MapLayer);

            StartCoroutine(GetGridSquareAsync(_latLong, _sizeKm, (data) => {

                if (data != null) {

                    var nodes = new Nodes();

                    foreach (var item in data.m_Root.elements) {

                        if (!nodes.ContainsKey(item.id)) {
                            nodes.Add(item.id, item);
                        }
                    }

                    result.m_Nodes = nodes;
                    m_Chunks.Add(_latLong, result);

                    StartCoroutine(PopulateChunkAsync(result, nodes));
                }
            }));

            return result;
        }
        
        /// <summary>
        /// Converts a list of node IDs to their positions in world space (using Mercator projection) and return the result.
        /// </summary>
        /// <param name="_nodes">List of node IDs.</param>
        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        private IEnumerable<Vector3D> NodesToWorld(Nodes _nodes, ulong[] _IDs) {

            var result = new List<Vector3D>();

            foreach (var id in _IDs) {
                if (_nodes.ContainsKey(id)) {
                    result.Add(NodeToWorld(_nodes, id));
                }
            }

            return result;
        }

        /// <summary>
        /// Converts a node's ID to its position in world space (using Mercator projection).
        /// </summary>
        /// <param name="_nodes">ID of node.</param>
        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        private Vector3D NodeToWorld(Nodes _nodes, ulong _ID) {

            Vector3D result;

            var node = _nodes[_ID];

            result = GPSToMap(new Vector3D(node.lat, node.lon, 0));

            return result;
        }

        /// <summary>
        /// Converts a GPS Lat, Long coordinate to map space (Currently in Mercator).
        /// </summary>
        /// <param name="_latLong">The input coordinate in Latitude, Longitude format (Altitude not yet supported).</param>
        /// <returns></returns>
        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        private Vector3D GPSToMap(Vector3D _latLong) {

            Vector3D result;

            float stretch = OSM.WGS84.CalculateEquatorialStretchFactor(Coordinates.s_InitialPosition.m_LatLongAlt.x);

            if (stretch == 0) {
                stretch = PhantomTech.Math.Epsilon * PhantomTech.Math.Sign(Coordinates.s_InitialPosition.m_LatLongAlt.x);
            }

            // Use a projection to convert a node's position from Latitude, Longitude to X, Y.
            // Here we are using mercator.
            result = new Vector3D(
                OSM.WGS84.LongitudeToX(_latLong.y - Coordinates.s_InitialPosition.m_LatLongAlt.y) * m_MapScale,
                0.0f,
                OSM.WGS84.LatitudeToY(_latLong.x - Coordinates.s_InitialPosition.m_LatLongAlt.x) * m_MapScale * stretch
            );

            return result;
        }
        
        /// <summary>
        /// Attempts to get the material of an element tagged with "surface" from OSM. Returns false if surface is unknown.
        /// </summary>
        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        private bool TryGetSurface(string _surface, out Material _result) {

            _result = null;

            if (string.IsNullOrEmpty(_surface) == false) { 
            
                if      (_surface.StartsWith("paved"))              { _result = m_RockMaterial;       }
                else if (_surface.StartsWith("asphalt"))            { _result = m_AsphaltMaterial;    }
                else if (_surface.StartsWith("chipseal"))           { _result = m_RockMaterial;       }
                else if (_surface.StartsWith("concrete"))           { _result = m_RockMaterial;       }
                else if (_surface.StartsWith("sett"))               { _result = m_RockMaterial;       }
                else if (_surface.StartsWith("unhewn_cobblestone")) { _result = m_RockMaterial;       }
                else if (_surface.StartsWith("metal"))              { _result = m_MetalMaterial;      }
                else if (_surface.StartsWith("wood"))               { _result = m_WoodMaterial;       }
                else if (_surface.StartsWith("stepping_stones"))    { _result = m_RockMaterial;       }
                else if (_surface.StartsWith("compacted"))          { _result = m_RockMaterial;       }
                else if (_surface.StartsWith("fine_gravel"))        { _result = m_RockMaterial;       }
                else if (_surface.StartsWith("gravel"))             { _result = m_RockMaterial;       }
                else if (_surface.StartsWith("rock"))               { _result = m_RockMaterial;       }
                else if (_surface.StartsWith("pebblestone"))        { _result = m_RockMaterial;       }
                else if (_surface.StartsWith("ground"))             { _result = m_MudMaterial;        }
                else if (_surface.StartsWith("dirt"))               { _result = m_MudMaterial;        }
                else if (_surface.StartsWith("earth"))              { _result = m_MudMaterial;        }
                else if (_surface.StartsWith("grass"))              { _result = m_VegetationMaterial; }
                else if (_surface.StartsWith("mud"))                { _result = m_MudMaterial;        }
                else if (_surface.StartsWith("sand"))               { _result = m_SandMaterial;       }
                else if (_surface.StartsWith("woodchips"))          { _result = m_WoodMaterial;       }
                else if (_surface.StartsWith("snow"))               { _result = m_RockMaterial;       }
                else if (_surface.StartsWith("ice"))                { _result = m_IceMaterial;        }
                else if (_surface.StartsWith("salt"))               { _result = m_RockMaterial;       }
                else if (_surface.StartsWith("clay"))               { _result = m_RockMaterial;       }
                else if (_surface.StartsWith("tartan"))             { _result = m_RockMaterial;       }
                else if (_surface.StartsWith("artificial_turf"))    { _result = m_VegetationMaterial; }
                else if (_surface.StartsWith("acrylic"))            { _result = m_RockMaterial;       }
                else if (_surface.StartsWith("carpet"))             { _result = m_RockMaterial;       }
            }

            return _result != null;
        }

        /// <summary>
        /// Adds a mesh to a GameObject, returning references to the Unity Components used for rendering.
        /// </summary>
        /// <param name="_mesh">The Mesh to be spawned.</param>
        /// <param name="_gameObject">The GameObject to add the mesh to.</param>
        /// <param name="_material">Material to add to the spawned mesh.</param>
        /// <param name="_filter">The MeshFilter created during spawning.</param>
        /// <param name="_renderer">The MeshRenderer created during spawning.</param>
        /// <param name="_recalculateNormals">Should the normals of the mesh be recalculated during spawn?</param>
        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        private static bool TrySpawnMesh(PhantomMesh _mesh, GameObject _gameObject, in Material _material, out MeshFilter _filter, out MeshRenderer _renderer) {

            bool result;

            try {
                _filter = _gameObject.AddComponent<MeshFilter>();
                _filter.mesh = _mesh.Parse<UnityEngine.Mesh>();

                _renderer = _gameObject.AddComponent<MeshRenderer>();
                _renderer.material = _material;

                result = true;
            }
            catch (System.Exception e) {
                PhantomTech.Debugging.Debug.Log(e, Debugging.Debug.LogType.Error);

                _filter = null;
                _renderer = null;

                result = false;
            }

            return result;
        }
        
        /// <summary>
        /// Attempts to retrieve a bounding square 'gridSize' Km around provided coordinates of data from OpenStreetMap via the Overpass API.
        /// </summary>
        /// <param name="_latLongAlt">Center of the bounding square in latitude and longitude.</param>
        /// <param name="_gridSizeKm">Size of the bounding square in kilometers.</param>
        /// <param name="_callback">Executed upon completion.</param>
        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        public static IEnumerator GetGridSquareAsync(Vector3D _latLongAlt, float _gridSizeKm, System.Action<OSMJSON> _callback) {

            OSMJSON result = null;
            Task task = null;

            const uint timeoutDuration = 30;

            OSM.QueryOSMPositionAsync(_latLongAlt, _gridSizeKm, timeoutDuration, (string json) => {

                task = Task.Factory.StartNew(() => {

                    if (string.IsNullOrEmpty(json) == false) {

                        Thread.CurrentThread.Priority = System.Threading.ThreadPriority.AboveNormal;

                        result = Deserialise(json);
                    }
                });
            });

            yield return new WaitUntil(() => task != null);

            float timeout = Time.time + (float)timeoutDuration;

            // Wait for the task to complete execution, or timeout.
            while (task.IsCompleted == false) {

                yield return new WaitForSecondsRealtime(1.0f);

                if (Time.time >= timeout || Application.isPlaying == false) {
                    task.Dispose();
                    yield break;
                }
            }

            if (_callback != null && result != null && Application.isPlaying == true) {
                _callback.Invoke(result);
            }
        }

        /// <summary>
        /// Constructs a PhantomMesh from a list of boundary vertices (in 2D [X,Z]) using the ear clipping method and elevates them by a specified height.
        /// </summary>
        /// <param name="_meshName">Name of the constructed mesh.</param>
        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        private static PhantomMesh MeshFromPerimeter(IEnumerable<Vector3D> _perimeterVertices, float _height, string _meshName = "New Mesh") {
            
            var result = new PhantomMesh(_meshName);

            int count = _perimeterVertices.Count();

            if (count >= 3) {

                Vector3D offsetDirection = new Vector3D(0, _height, 0);

                // CREATE A POLYGON USING EAR CLIPPING METHOD
                if (count > 4) {

                    Triangulator.Triangulator.Triangulate(
                        _perimeterVertices.Select(vertex => new System.Numerics.Vector2(vertex.x, vertex.z)).ToArray(),
                        WindingOrder.Clockwise,
                        out System.Numerics.Vector2[] verts,
                        out int[] indices
                    );

                    // Assign polygon data to the mesh.
                    result.SetVertices(verts.Select(vert => new Vector3D(vert.X, 0.0f, vert.Y) + offsetDirection).ToArray());
                    result.SetTriangles(indices);
                }
                else {

                    var vertices = _perimeterVertices.Select(vertex => vertex + offsetDirection);

                    if (count == 4) {   // CREATE A QUAD.
                        result.SetVertices(vertices.ToArray());
                        result.SetTriangles(new int[] { 0, 1, 2, 0, 2, 3 });
                    }
                    else {              // CREATE A TRI.
                        result.SetVertices(vertices.ToArray());
                        result.SetTriangles(new int[] { 0, 1, 2 });
                    }

                    // If the triangles are winding the wrong way, reverse them.
                    if (PhantomTech.Math.GetNormal(
                        result.m_Vertices[0],
                        result.m_Vertices[1],
                        result.m_Vertices[2],
                        false
                    ).y < 0) {
                        result.SetTriangles(result.m_Triangles.Reverse().ToArray());
                    }
                }
            }

            // Automatically create normals.
            result.CalculateNormals(false);

            return result;
        }

        /// <summary>
        /// Constructs a mesh consisting of the input vertices extruded by direction.
        /// </summary>
        /// <param name="_meshName">Name of the constructed mesh.</param>
        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        private static PhantomMesh Extrude(IEnumerable<Vector3D> _vertices, Vector3D _direction, string _meshName = "New Mesh", bool _doubleSided = false) {

            PhantomMesh result;

            int pointCount = _vertices.Count();

            if (pointCount >= 2) {

                result = new PhantomMesh(_meshName);

                var points = _vertices.ToList();

                if (points[pointCount - 1] != points[0]) {
                    points.Add(points[0]);
                    pointCount++;
                }

                var extrudedPoints = points.Select(node => node + _direction);

                // Combine the floor and roof vertex arrays and add them to the mesh.
                var combinedPoints = new List<Vector3D>(points);
                combinedPoints.AddRange(extrudedPoints);

                // Only generate triangles if the number of points is 2 or more.
                if (pointCount >= 2) {

                    var triangles = new List<int>();

                    for (int i = 0; i < pointCount - 1; ++i) {

                        int index = i;

                        int[] quad1 = {
                            index + 1,
                            index + pointCount,
                            index,

                            index + pointCount + 1,
                            index + pointCount,
                            index + 1
                        };

                        triangles.AddRange(quad1);
                    }

                    if (_doubleSided) {

                        for (int i = 0; i < pointCount - 1; ++i) {

                            int index = i + pointCount;

                            int[] quad2 = {
                                index,
                                index + 1,
                                index + pointCount,

                                index + 1,
                                index + pointCount + 1,
                                index + pointCount
                            };

                            triangles.AddRange(quad2);
                        }

                        // Duplicate the vertices.
                        combinedPoints.AddRange(combinedPoints);
                    }

                    result.SetTriangles(triangles.ToArray());
                }

                result.SetVertices(combinedPoints.ToArray());

                // Automatically create normals.
                result.CalculateNormals(false);
            }
            else {
                result = null;
            }

            return result;
        }

        /// <summary>
        /// Sets the layer of a GameObject and returns a reference to it.
        /// </summary>
        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        private static GameObject SetLayer(GameObject _go, int _layer) {
            _go.layer = _layer;

            return _go;
        }

        /// <summary>
        /// Determines if a set of points is in clockwise winding order. 
        /// See also: https://stackoverflow.com/questions/1165647/how-to-determine-if-a-list-of-polygon-points-are-in-clockwise-order
        /// </summary>
        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        private static bool IsClockwise(Vector3D[] _points) {

            float sum = 0;

            for (int i = 0; i < _points.Length - 1; i++) {

                var A = _points[  i  ];
                var B = _points[i + 1];

                sum += (B.x - A.x) * (B.z - A.z);
            }

            return sum >= 0;
        }

        /// <summary>
        /// Determines if an OSM 'way' denotes an area (contiguous loop of nodes) or a path (line of nodes).
        /// </summary>
        /// <returns></returns>
        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        private static bool IsArea(IEnumerable<Vector3D> _way) {

            bool result;

            int count = _way.Count();

            if (count > 2) {
                result = _way.ElementAt(count - 1) == _way.ElementAt(0);
            }
            else {
                result = false;
            }

            return result;

        }

        /// <summary>
        /// Disposes all active tasks.
        /// </summary>
        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        private void DisposeTasks() {

            foreach (var task in s_PopulationTasks.Values) {
                task.Dispose();
            }
            s_PopulationTasks.Clear();

        }

        private void OnDestroy() {

            DisposeTasks();

            /* Dispose all active queries to prevent the possibility 
               of a thread/request not closing with the app. */
            OSM.DisposeAllRequests();
        }
    }
}

#else

using UnityEngine;
using UnityEditor;

namespace PhantomTech.Location {

#if UNITY_EDITOR

    /// <summary>
    /// This object is Editor-Only. Scripting for it may result in unexpected behaviour.
    /// </summary>
    [CustomEditor(typeof(PhantomMap))]
    public sealed class PhantomMapEditor : Editor {

        public override void OnInspectorGUI() {

            DrawDefaultInspector();

            EditorGUILayout.HelpBox("PhantomMap is not enabled. If you wish to enable PhantomMap, please define PHANTOMTECH_EXPERIMENTAL_MAP as a symbol in your project's scripting define symbols.", MessageType.Info);

            EditorGUILayout.TextField("PHANTOMTECH_EXPERIMENTAL_MAP");
        }

    }

#endif

    /// <summary>
    /// If you are seeing this message, PhantomMap is not enabled. If you wish to enable PhantomMap, please define PHANTOMTECH_EXPERIMENTAL_MAP as a symbol in your project's scripting define symbols.
    /// </summary>
    [AddComponentMenu("PhantomEngine/Experimental/Phantom Map/Phantom Map")]
    public sealed class PhantomMap : MonoBehaviour {}
}

#endif