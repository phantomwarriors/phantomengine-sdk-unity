using PhantomTech.Spatial;
using UnityEngine;

namespace PhantomTech {

    public sealed class MergeDepthMaps : MonoBehaviour {

        [SerializeField] Shader m_UnifyDepth;

        [Header("Camera")]
        [Range(0.0f, 10.0f)]

        private RenderTexture m_HybridDepth;

        private Material m_UnifyDepthMAT;
        private Material m_BlurMAT;

        void Start() {
            Camera camera = GetComponent<Camera>();
            camera.depthTextureMode = DepthTextureMode.Depth;

            m_HybridDepth = new RenderTexture(Screen.width, Screen.height, 0, RenderTextureFormat.ARGBFloat);

            m_UnifyDepthMAT = new Material(m_UnifyDepth);
        }

        private void OnRenderImage(RenderTexture _src, RenderTexture _dest) {

            if (SpatialMapping.s_DepthMapProcessedUnscaled != null) {
                m_UnifyDepthMAT.SetTexture("_WorldDepth", SpatialMapping.s_DepthMapProcessedUnscaled.RenderTexture);
            }
            
            Graphics.Blit(_src, m_HybridDepth, m_UnifyDepthMAT);
            
            m_HybridDepth.SetGlobalShaderProperty("_HybridDepth");

            Graphics.Blit(_src, _dest);
        }
    }
}