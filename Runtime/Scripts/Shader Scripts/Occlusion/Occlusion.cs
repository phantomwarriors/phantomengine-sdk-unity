using UnityEngine;

namespace PhantomTech {

    public sealed class Occlusion : MonoBehaviour {
    /*If you have post-processing enabled, disable the 'Direct to Camera Target' setting
    in the 'Post-process Layer' component of your Camera.*/

        [SerializeField] private Shader m_OcclusionShader;
        
        [Range(0.0f, 0.1f)]
        [SerializeField] private float m_BlendValue = 0.02f;

        private Material m_OcclusionMAT;

        private void Start() {
            m_OcclusionMAT = new Material(m_OcclusionShader);
        }

#if !UNITY_EDITOR
        private void OnRenderImage(RenderTexture _src, RenderTexture _dst) {

            m_OcclusionMAT.SetTexture("_CameraFeed", Spatial.SpatialMapping.s_BackgroundTexture);
            m_OcclusionMAT.SetFloat("_Blend", m_BlendValue);

            Graphics.Blit(_src, _dst, m_OcclusionMAT);
        }
#endif
    }
}