using System;
using UnityEngine;

namespace PhantomTech {
    
    public sealed class PulseReveal : MonoBehaviour {
        
        [SerializeField] private Material m_PulseRevealMAT;
        [SerializeField] private GameObject m_PulseSource;
        [SerializeField] private float m_PulseCooldown;
        
        [SerializeField] private Occlusion m_Occlusion;
        [SerializeField] float m_OcclusionDisableTime = 3.0f;

        private float m_TimeSincePulse = 100;
        private float m_OcclusionDisableCLK;

        private void Start() {
            if (!m_PulseSource) {
                m_PulseSource = gameObject;
            }
        }

        private void Update() {
            if (m_TimeSincePulse < 100) {
                m_TimeSincePulse += Time.deltaTime;
            }

            if (m_Occlusion) {
                if (m_OcclusionDisableCLK <= 0.0f) {
                    m_Occlusion.enabled = true;
                }
                else {
                    m_OcclusionDisableCLK -= Time.deltaTime;
                }
            }

            m_PulseRevealMAT.SetFloat("_TimeSincePulse", m_TimeSincePulse);
            m_PulseRevealMAT.SetVector("_EmissionPoint", m_PulseSource.transform.position);
        }

        public void Pulse() {
            if (m_Occlusion) {
                m_Occlusion.enabled = false;
                m_OcclusionDisableCLK = m_OcclusionDisableTime;
            }
            
            if (m_TimeSincePulse >= m_PulseCooldown) {
                m_TimeSincePulse = 0.0f;
            }
        }

        private void OnDestroy() {
#if UNITY_EDITOR
            m_PulseRevealMAT.SetFloat("_TimeSincePulse", 100);
            m_PulseRevealMAT.SetVector("_EmissionPoint", new Vector4(0.0f, 0.0f, 0.0f, 0.0f));
#endif
        }
    }
}