﻿using System.Collections;
using System.Collections.Generic;

using UnityEngine;

namespace PhantomTech {

    /// <summary>
    /// Script that automagically positions an object at the edge of a specified camera's viewing frustrum, to occlude anything that may be rendered behind.
    /// </summary>
    public sealed class HideBackground : MonoBehaviour {

        [SerializeField] private Camera m_Camera;

        private const float s_Trunctation = -0.01f;

        private void Start() {
            Sync();
        }

        private void LateUpdate() {
            Sync();
        }

        /// <summary>
        /// Resynchronise the position of this object with the camera's far clip plane.
        /// </summary>
        public void Sync() {
            transform.parent = m_Camera.transform;
            transform.localPosition = new Vector3(0, 0, m_Camera.farClipPlane + s_Trunctation);
        }

    }
}