﻿using System.Collections;
using System.Collections.Generic;

using UnityEngine;
using UnityEngine.UI;

using System.Runtime.CompilerServices;

namespace PhantomTech.Diagnostics {

    /// <summary>
    /// Script providing functionality for an FPS counter with adjustable interval and sampling type.
    /// </summary>
    public sealed class FPSCounter : MonoBehaviour {

        /* PRIVATE */

        [SerializeField] private Text m_OutputText;
        [SerializeField] private float m_Interval;
        [SerializeField] private SampleType m_SampleType;

        [Space]
        [SerializeField] private bool m_RoundToInt = true;
        [SerializeField] private bool m_ClampToApplicationTargetFPS = false;
    
        /// <summary>
        /// Method of filtering the frame data for the FPS counter.
        /// </summary>
        public enum SampleType { 
            /// <summary>
            /// Display the lowest fps.
            /// </summary>
            Min,
            /// <summary>
            /// Display the highest fps.
            /// </summary>
            Max,
            /// <summary>
            /// Display the average fps.
            /// </summary>
            Average,
            /// <summary>
            /// Display the most recent fps.
            /// </summary>
            Latest
        }

        private List<float> m_Samples = new List<float>();

        private float m_NextCheck = 0.0f;

        private void LateUpdate() {

            // Take a sample of the FPS each frame.
            float recording = 1.0f / Time.unscaledDeltaTime;

            recording = PhantomTech.Math.ClampMax(recording, Screen.currentResolution.refreshRate);

            m_Samples.Add(recording);

            // If the current interval has finished, display the output.
            if (m_NextCheck <= 0) {
                m_NextCheck += m_Interval;

                UpdateOutput();

                m_Samples.Clear();
            }
            else {
                m_NextCheck -= Time.unscaledDeltaTime;
            }
        }

        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        private void UpdateOutput() {

            float result;

            // Filter the output based on the chosen sample type.
            switch (m_SampleType) {

                case SampleType.Min: {
                    result = PhantomTech.Math.Min(m_Samples.ToArray());
                    break; 
                }
                case SampleType.Max: {
                    result = PhantomTech.Math.Max(m_Samples.ToArray());
                    break; 
                }
                case SampleType.Average: {
                    result = PhantomTech.Math.Average(m_Samples);
                    break; 
                }
                case SampleType.Latest: {
                    result = m_Samples[m_Samples.Count - 1];
                    break; 
                }

                default: { 
                    PhantomTech.Diagnostics.Debug.Log("FPSCounter.cs Unrecognised SampleType!", PhantomTech.Diagnostics.Debug.LogType.Error);

                    result = -1f;

                    break; 
                }
            }

            // Optionally clamp the output to the current target frame rate.
            if (m_ClampToApplicationTargetFPS && Application.targetFrameRate >= 0) {
                result = PhantomTech.Math.ClampMax(result, Application.targetFrameRate);
            }

            // Convert to a string...
            string fpsOutput;

            // Optionally display the FPS as (rounded up) whole numbers.
            if (m_RoundToInt == true) {
                fpsOutput = PhantomTech.Math.Ceiling(result).ToString();
            }
            else {
                fpsOutput = result.ToString("F2");
            }

            // ...Assign string to text output.
            m_OutputText.text = fpsOutput + "fps (" + ((int)((1.0f / result) * 1000)).ToString() + "ms)";
        }
    }
}