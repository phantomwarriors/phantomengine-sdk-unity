Shader "Hidden/ProcessDepthMap" {

    Properties {
        _MainTex("Texture", 2D) = "white" {}
        _ScaleFactor("Scaling Factor", Vector) = (1.0, 1.0, 0, 0)
        _MaxDepth("Max Depth", float) = 5.0
    }

    SubShader {

        Pass {

            CGPROGRAM
            #pragma vertex vert
            #pragma fragment frag

            struct appdata {
                half4 vertex : POSITION;
                half2 uv : TEXCOORD0;
            };

            struct v2f {
                half4 vertex : SV_POSITION;
                half2 uv : TEXCOORD0;
            };

            sampler2D_half _MainTex;
            half2 _ScaleFactor;
            half _MaxDepth;

            v2f vert(appdata v) {
                v2f o;
                o.vertex = UnityObjectToClipPos(v.vertex);
                o.uv = v.uv;

                return o;
            }

            half4 frag(v2f i) : SV_Target {
                half2 scaledIndex = clamp((((half2(i.uv.y, 1 - i.uv.x) - 0.5) * _ScaleFactor) + 0.5), 0, 1);
                return 1.0 - (tex2D(_MainTex, scaledIndex).r / _MaxDepth);
            }

            ENDCG
        }
    }
}