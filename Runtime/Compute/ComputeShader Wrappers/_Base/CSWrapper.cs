﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

using System;

namespace PhantomTech {

    /// <summary>
    /// Provides a framework for wrapping a Unity ComputeShader into an easily disposable managed object.
    /// </summary>
    public abstract class CSWrapper : IDisposable {

        protected ComputeShader       m_CS;
        public    CSWrapperParameters m_CSParameters { get; protected set; }

        public CSWrapper(CSWrapperParameters _CSParameters) {
            m_CSParameters = _CSParameters;
        }

        ~CSWrapper() {
            Dispose(false);
        }

        /// <summary>
        /// Disposes the compute shader wrapper.
        /// </summary>
        public virtual void Dispose() {
            Dispose(true);

            GC.SuppressFinalize(this);
        }

        private void Dispose(bool _disposing) {

            if (_disposing) {

                DisposeUnmanaged();

                m_CSParameters.Dispose();
                m_CSParameters = null;
            }

            DisposeManaged();
        }

        protected virtual void DisposeManaged()   { }
        protected virtual void DisposeUnmanaged() { }

    }

    /// <summary>
    /// Provides functionality for wrapping data associated with a Unity Compute Shader into an easily disposable managed object.
    /// </summary>
    public abstract class CSWrapperParameters : IDisposable {

        ~CSWrapperParameters() {
            Dispose(false);
        }

        /// <summary>
        /// Disposes the compute shader wrapper parameters.
        /// </summary>
        public virtual void Dispose() {
            Dispose(true);

            GC.SuppressFinalize(this);
        }

        private void Dispose(bool _disposing) {

            if (_disposing) {
                DisposeUnmanaged();
            }

            DisposeManaged();
        }

        protected abstract void DisposeManaged();
        protected abstract void DisposeUnmanaged();
    }
}