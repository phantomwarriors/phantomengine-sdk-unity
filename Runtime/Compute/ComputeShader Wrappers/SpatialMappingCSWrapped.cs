﻿using System.Collections;
using System.Collections.Generic;

using UnityEngine;
using UnityEngine.Rendering;

using static PhantomTech.PhantomSettings;
using static PhantomTech.UnityExtensions;

namespace PhantomTech.Spatial {

    /// <summary>
    /// Wrapper for the spatial mapping compute shader.
    /// </summary>
    public sealed class SpatialMappingCSWrapped : CSWrapper {

        /// <summary>
        /// Wrapper for the parameters of the spatial mapping compute shader.
        /// </summary>
        public class SpatialMappingParameters : CSWrapperParameters {

            // MANAGED:   (Handled by .NET)
            public int      PointCount;
            public Vector2  ScaleFactor;
            public Vector2  HalfFOV;
            public Vector3D CamPosIn;
            public Vector4D CamRotIn;

            // UNMANAGED: (Not handled by .NET)
            public RenderTexture TexIn, TexOut;
            public ComputeBuffer CamSpaceOut;

            protected override void DisposeManaged() {
            }

            protected override void DisposeUnmanaged() {

                if (TexIn != null) {
                    TexIn.DiscardContents();
                    TexIn.Release();
                    TexIn = null;
                }
                if (CamSpaceOut != null) {
                    CamSpaceOut.Dispose();
                    CamSpaceOut.Release();
                    CamSpaceOut = null;
                }
            }
        }

        // SHADER VARIABLE IDs
        private static readonly int s_PointCount_UID  = PropertyID("PointCount");
        private static readonly int s_ScaleFactor_UID = PropertyID("ScaleFactor");

        private static readonly int s_HalfFOV_UID     = PropertyID("HalfFOV");
        private static readonly int s_CamPosIn_UID    = PropertyID("CamPosIn");
        private static readonly int s_CamRotIn_UID    = PropertyID("CamRotIn");
        private static readonly int s_CamSpaceOut_UID = PropertyID("CamSpaceOut");

        private static readonly int s_TexIn_UID  = PropertyID("TexIn");
        private static readonly int s_TexOut_UID = PropertyID("TexOut");

        private static readonly Vector3DInt s_DispatchThreadsK0 = new Vector3DInt(1, 1, 1);
        private static readonly Vector3DInt s_DispatchThreadsK1 = new Vector3DInt(1, 1, 1);

        public new SpatialMappingParameters m_CSParameters {
            get {
                return base.m_CSParameters as SpatialMappingParameters;
            }
        }

        public SpatialMappingCSWrapped(SpatialMappingParameters _CSParameters) : base(_CSParameters) {

            // Load the native shader from scanning parameters.
            m_CS = SpatialMappingSettings.m_SpatialMappingComputeShader;
        }

        public void InitK0(RenderTexture _TexIn, RenderTexture _TexOut, Vector2 _ScaleFactor) {

            m_CSParameters.ScaleFactor = _ScaleFactor;

            m_CSParameters.TexIn  = _TexIn;
            m_CSParameters.TexOut = _TexOut;

            m_CS.Assign(s_ScaleFactor_UID, m_CSParameters.ScaleFactor);

            m_CS.Assign(0, s_TexIn_UID,  m_CSParameters.TexIn);
            m_CS.Assign(0, s_TexOut_UID, m_CSParameters.TexOut);
        }

        public void ExecuteK0() {
            m_CS.Run(0, s_DispatchThreadsK0.x, s_DispatchThreadsK0.y, s_DispatchThreadsK0.z);
        }

        /// <summary>
        /// Initialises kernel 1 of the compute shader.
        /// </summary>
        public void InitK1(ComputeBuffer _CamSpaceOut, int _PointCount) {

            m_CSParameters.PointCount  = _PointCount;
            m_CSParameters.CamSpaceOut = _CamSpaceOut;

            m_CS.Assign(s_PointCount_UID, m_CSParameters.PointCount);

            m_CS.Assign(s_HalfFOV_UID, m_CSParameters.HalfFOV);

            m_CS.Assign(s_CamPosIn_UID, m_CSParameters.CamPosIn);
            m_CS.Assign(s_CamRotIn_UID, m_CSParameters.CamRotIn);

            m_CS.Assign(1, s_CamSpaceOut_UID, m_CSParameters.CamSpaceOut);
        }

        /// <summary>
        /// Executes kernel 1 of the compute shader.
        /// </summary>
        public void ExecuteK1(Vector3D[] _result) {
            m_CS.Run(1, s_DispatchThreadsK1.x, s_DispatchThreadsK1.y, s_DispatchThreadsK1.z);

            m_CSParameters.CamSpaceOut.GetData(_result, 0, 0, m_CSParameters.PointCount);
        }

        /// <summary>
        /// Executes kernel 1 of the compute shader. 
        /// Note: Not available on all platforms. (See: <see href="https://docs.unity3d.com/2021.2/Documentation/ScriptReference/Device.SystemInfo-supportsAsyncGPUReadback.html"/>)
        /// </summary>
        /// <returns></returns>
        public AsyncGPUReadbackRequest ExecuteK1() {
            m_CS.Run(1, s_DispatchThreadsK1.x, s_DispatchThreadsK1.y, s_DispatchThreadsK1.z);

            return AsyncGPUReadback.Request(m_CSParameters.CamSpaceOut);
        }
    }
}