﻿Shader "Custom/ZWrite Only" {

    SubShader {

        Tags { "Queue" = "Geometry+499" "IgnoreProjector" = "True" "RenderType" = "Transparent"}

        ZWrite On
        ColorMask 0

        Pass { }
    }

    Fallback "VertexLit"
}