 ~ 7-Zip LZMA SDK (Software Development Kit) ~

Code sourced from: https://www.7-zip.org/sdk.html

License: LZMA SDK is placed in the public domain. 

		 Anyone is free to copy, modify, publish, use, compile, sell, or distribute the original LZMA SDK code, either in source code form or as a compiled binary, for any purpose, commercial or non-commercial, and by any means.

~ NormalSolver.cs ~

Code sourced from: http://schemingdeveloper.com

Visit their game studio website: http://stopthegnomes.com

License: You may use this code however you see fit, as long as you include this notice
         without any modifications.

         You may not publish a paid asset on Unity store if its main function is based on
         the following code, but you may publish a paid asset that uses this code.

         If you intend to use this in a Unity store asset or a commercial project, it would
         be appreciated, but not required, if you let me know with a link to the asset. If I
         don't get back to you just go ahead and use it anyway!

~ Triangulator ~

Code sourced from: https://github.com/nickgravelyn/Triangulator

Description: Triangulator is an implementation of Dave Eberly's ear clipping algorithm as described here: http://www.geometrictools.com/Documentation/TriangulationByEarClipping.pdf. The project allows you to simply input a list of vertices and get back the required vertices (in order) and indices needed to construct a VertexBuffer and IndexBuffer for rendering the particular shape. The library is able to cut holes inside of polygons without error (the only caveat to this is that the library assumes that the hole to be cut lies completely within the shape so erroneous data given as a hole will result in invalid output data).

		     Note: Triangulator is set up to write a good amount of verbose output in Debug mode. This will affect performance. For optimal performance, make sure you are building the library in Release mode or modify the source to remove the logging functionality.

License: The MIT License (MIT)
		 
		 Copyright (c) 2017, Nick Gravelyn
		 
		 Permission is hereby granted, free of charge, to any person obtaining a copy
		 of this software and associated documentation files (the "Software"), to deal
		 in the Software without restriction, including without limitation the rights
		 to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
		 copies of the Software, and to permit persons to whom the Software is
		 furnished to do so, subject to the following conditions:
		 
		 The above copyright notice and this permission notice shall be included in
		 all copies or substantial portions of the Software.
		 
		 THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
		 IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
		 FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
		 AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
		 LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
		 OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
		 THE SOFTWARE.