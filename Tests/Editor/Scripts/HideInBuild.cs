using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public sealed class HideInBuild : MonoBehaviour {

private void Awake() {

#if UNITY_EDITOR
#else
        gameObject.SetActive(false);
        Destroy(this);
#endif
    }

}
