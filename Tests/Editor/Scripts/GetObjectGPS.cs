#if UNITY_EDITOR

using System.Collections;
using System.Collections.Generic;
using UnityEngine;

using System.Runtime.CompilerServices;

using static PhantomTech.Location.Coordinates;

namespace PhantomTech.Interface {
    
    /// <summary>
    /// Script for providing visual debug information about an object's global (GPS) and cartesian coordinates.
    /// </summary>
    public sealed class GetObjectGPS : MonoBehaviour {

        [Header("Debug")]
        [Tooltip("Size of the marker to be drawn.")] [SerializeField] private float m_MarkerSize = 1.0f;

        private void OnDrawGizmos() {

            Vector3D worldAsGPS = WorldToGeo(transform.position);
            Vector3D gpsAsWorld = GeoToWorld(worldAsGPS);

            Vector3D globalRotation = ToGlobalRotation(transform.eulerAngles);
            Vector3D euclidRotation = ToEuclidRotation(globalRotation);

            transform.name = worldAsGPS.ToString() + " " + globalRotation.ToString();

            Gizmos.color = Color.green;

            Gizmos.matrix = UnityEngine.Matrix4x4.TRS(gpsAsWorld, Quaternion.Euler(euclidRotation), new Vector3(1, 1, 1));

            var origin = new Vector3(0, 0, 0);

            Gizmos.DrawCube(
                origin,
                new Vector3(m_MarkerSize, m_MarkerSize, m_MarkerSize)
            );

            DrawRay(origin, new Vector3(0, 1, 0), Color.green);
            DrawRay(origin, new Vector3(0, 0, 1), Color.blue);
            DrawRay(origin, new Vector3(1, 0, 0), Color.red);
        }

        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        private void DrawRay(Vector3 _origin, Vector3 _dir, Color _color) {

            var defaultColor = Gizmos.color;

            Gizmos.color = _color;
            Gizmos.DrawRay(_origin, _dir);
            Gizmos.color = defaultColor;
        }
    }
}

#endif