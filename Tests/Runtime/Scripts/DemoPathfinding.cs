using PhantomTech.Navigation;
using UnityEngine;

namespace PhantomTech {
    
    public sealed class DemoPathfinding : MonoBehaviour {

        [SerializeField] private Pathfinding m_Pathfinding;
        [SerializeField] private FlyingRobotStateController m_Animator;

        private GameObject m_Target;

        private void Start() {
            m_Target = GameObject.FindGameObjectWithTag("Player");
            m_Pathfinding.SetTarget(m_Target);
            m_Pathfinding.m_Speed = 0.5f;
            m_Pathfinding.m_RotationSpeed = 5;
        }

        private void Update() {
            if (m_Pathfinding.IsMoving) {
                m_Animator.Idle = false;
                m_Animator.Moving = true;
            }
            else {
                m_Animator.Idle = true;
                m_Animator.Moving = false;
            }
        }
    }
}
