using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Serialization;

namespace PhantomTech.Examples {

    public sealed class ThrowFetch : MonoBehaviour {

        public GameObject m_Rigidbody;
        public float m_Force;

        [FormerlySerializedAs("m_fetcher")][SerializeField] private Fetching m_Fetcher;

        /// <summary>
        /// Throw the assigned Rigidbody.
        /// </summary>
        public void ThrowRigidbody() {

            var device = PScene.Instance.m_Device.transform;

            var obj = GameObject.Instantiate(m_Rigidbody, device.position, device.rotation, null);

            obj.GetComponent<Rigidbody>()?.AddForce(device.forward * m_Force, ForceMode.VelocityChange);

            if (m_Fetcher) {
                m_Fetcher.PlayFetch(obj);
            }
        }
    }
}