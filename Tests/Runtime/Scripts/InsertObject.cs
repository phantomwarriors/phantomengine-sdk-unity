﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

using PhantomTech.Spatial;

using System.Runtime.CompilerServices;

namespace PhantomTech {

    /// <summary>
    /// Script for testing the insertion of spatial data into the OctreeManager.
    /// </summary>
    public sealed class InsertObject : MonoBehaviour {

        [Header("Insertion")]
        [SerializeField] private IMode m_InsertionMode = IMode.DepthMap;

        [Header("Mesh")]
        [SerializeField] private Mesh m_PointCloud = null;

        [Header("Depth Map")]
        [SerializeField] private Texture2D m_DepthMap = null;

        private static bool s_FirstRun = true;

        private enum IMode {
            PointCloud,
            DepthMap,
        }

        private void Awake() {
#if !UNITY_EDITOR
            enabled = false;
#endif
        }

        private void OnEnable() {
            StartCoroutine(CoroutineOnEnable());
        }

        private IEnumerator CoroutineOnEnable() {

            // There are other asynchronous operations in the background during startup, which can mess with benchmarking, so we'll try to wait them out.
            if (s_FirstRun) {
                yield return new WaitForSeconds(1.0f);
            }

            Vector3D[] points = null;

            switch (m_InsertionMode) {

                case IMode.PointCloud: {
                    points = SpatialMapping.MeshToPointCloud(m_PointCloud);

                    break;
                }
                case IMode.DepthMap: {

                    float rescalingFactor = 1.0f;

                    // Calculate an optimum texture size for lossless downscaling.
                    if (PhantomSettings.SpatialMappingSettings.m_Scanning.m_OptimiseTextureResolution) {

                        rescalingFactor = SpatialMapping.DetermineRescalingFactor(
                            m_DepthMap.width,
                            PScene.Instance.m_DeviceSensorData.m_MaxDepth
                        );
                    }

                    // Apply a lossy downscale.
                    rescalingFactor *= PhantomSettings.SpatialMappingSettings.m_Scanning.m_FinalTextureScale;

                    int width  = (int)(m_DepthMap.width  * rescalingFactor);
                    int height = (int)(m_DepthMap.height * rescalingFactor);

                    points = new Vector3D[width * height];

                    using (var cs = new SpatialMappingCSWrapped(new SpatialMappingCSWrapped.SpatialMappingParameters() {
                        HalfFOV  = PScene.Instance.m_DeviceSensorData.m_HalfFOVRadians,
                        CamPosIn = PScene.Instance.m_Device.transform.position,
                        CamRotIn = PScene.Instance.m_Device.transform.rotation
                    })) {

                        var texIn = RenderTexture.GetTemporary(width, height, 0, RenderTextureFormat.RFloat);
                        texIn.Assign(m_DepthMap);

                        var asTex2D = new Texture2D(width, height, TextureFormat.RFloat, false);
                        texIn.Copy(asTex2D);
                        
                        RenderTexture.ReleaseTemporary(texIn);

                        var camSpaceOut = UnityHelpers.CreateCB(typeof(Vector3D), width * height);

                        var pointsForProcessing = SpatialMapping.PreprocessDepthPoints(
                            asTex2D,
                            PScene.Instance.m_DeviceSensorData.m_MaxDepth,
                            PhantomSettings.SpatialMappingSettings.m_Scanning.m_ScanningTruncation
                        );

                        Destroy(asTex2D);

                        camSpaceOut.SetData(pointsForProcessing, 0, 0, pointsForProcessing.Length);

                        cs.InitK1(camSpaceOut, pointsForProcessing.Length);

                        var sw = new System.Diagnostics.Stopwatch();
                        sw.Start();

                        var request = cs.ExecuteK1();

                        yield return new WaitUntil(() => request.done);

                        sw.Stop();
                        PhantomTech.Diagnostics.Debug.Log("TestInsertObject.cs: Finished projecting GPU points. (" + sw.ElapsedMilliseconds + "ms)");

                        if (request.hasError == false) {
                            points = request.GetData<Vector3D>().ToArray();
                        }
                        else {
                            PhantomTech.Diagnostics.Debug.Log("ERROR (TestInsertObject.cs): Unknown error retrieving GPU data asyncronously!", Diagnostics.Debug.LogType.Error);
                        }
                    }

                    break;
                }
            }

            if (points.Length != 0) {

                TryInsert(points);

                yield return null;

                Scanning.FrameScanned?.Invoke(new Scanning.FrameScannedArgs());
            }

            enabled = false;
        }

        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        private static bool TryInsert(Vector3D[] _points, bool _bypassTrim = false) {

            bool result = false;

            if (OctreeManager.Initialised) {

                var sw = new System.Diagnostics.Stopwatch();
                sw.Start();

                OctreeManager.Instance.AddPoints(_points, s_FirstRun || _bypassTrim);

                sw.Stop();

                PhantomTech.Diagnostics.Debug.Log("TestInsertObject.cs: Insertion completed. (" + sw.ElapsedMilliseconds + "ms)");

                result = true;
            }
            else {
                PhantomTech.Diagnostics.Debug.Log("TestInsertObject.cs: Insertion failed. REASON: The OctreeManager is not initialised.");
            }

            s_FirstRun = false;

            return result;
        }
    }
}