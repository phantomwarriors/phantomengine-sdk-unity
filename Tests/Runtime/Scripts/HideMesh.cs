using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace PhantomTech.Examples {

    public class HideMesh : MonoBehaviour {

        /// <summary>
        /// Show the mesh.
        /// </summary>
        public void Show() {

            var cameraLayers = PScene.Instance.m_Device.cullingMask;
            var    meshLayer = LayerMask.NameToLayer(PhantomSettings.Instance.m_ScanningSettings.m_MeshGeneration.m_Layer);

            cameraLayers.SetBit(meshLayer, 1);

            PScene.Instance.m_Device.cullingMask = cameraLayers;
        }

        /// <summary>
        /// Hide the mesh.
        /// </summary>
        public void Hide() {

            var cameraLayers = PScene.Instance.m_Device.cullingMask;
            var    meshLayer = LayerMask.NameToLayer(PhantomSettings.Instance.m_ScanningSettings.m_MeshGeneration.m_Layer);

            cameraLayers.SetBit(meshLayer, 0);

            PScene.Instance.m_Device.cullingMask = cameraLayers;
        }

        /// <summary>
        /// Toggle the visibility of the mesh.
        /// </summary>
        public void Toggle() {

            var cameraLayers = PScene.Instance.m_Device.cullingMask;
            var    meshLayer = LayerMask.NameToLayer(PhantomSettings.Instance.m_ScanningSettings.m_MeshGeneration.m_Layer);

            var toggle = cameraLayers.GetBit(meshLayer) ? 0 : 1;

            cameraLayers.SetBit(meshLayer, toggle);

            PScene.Instance.m_Device.cullingMask = cameraLayers;
        }
    }
}
