using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace PhantomTech.Examples {

    public sealed class Throw : MonoBehaviour {

        public GameObject m_Rigidbody;
        public float m_Force;

        /// <summary>
        /// Throw the assigned Rigidbody.
        /// </summary>
        public void ThrowRigidbody() {

            var device = PScene.Instance.m_Device.transform;

            var obj = GameObject.Instantiate(m_Rigidbody, device.position, device.rotation, null);

            obj.GetComponent<Rigidbody>()?.AddForce(device.forward * m_Force, ForceMode.VelocityChange);
        }
    }
}

