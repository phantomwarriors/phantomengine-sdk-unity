Phantom Technology Ltd.

Non-Commercial End-User Licence Agreement (EULA).



Last updated date: 13th April 2021



IMPORTANT - READ CAREFULLY:



Phantom Technology Ltd. (“Phantom Technology”) licenses this software and all updates and related materials (collectively, the “Software”), excluding any open-source software that is distributed with the Software, subject to the terms and conditions of this Agreement. BY SELECTING “ACCEPT”, OR BY DOWNLOADING, INSTALLING OR USING THE SOFTWARE, ALL OF THE TERMS AND CONDITIONS CONTAINED IN THIS AGREEMENT BECOME LEGALLY BINDING ON YOU AS AN INDIVIDUAL OR ON THE ORGANISATION THAT YOU REPRESENT.



IF YOU THE INDIVIDUAL (A) DO NOT AGREE TO THE TERMS AND CONDITIONS OF THIS AGREEMENT, OR (B) ARE NOT AUTHORISED TO DOWNLOAD OR INSTALL THE SOFTWARE OR TO AGREE TO BE BOUND BY THIS AGREEMENT ON BEHALF OF YOUR ORGANISATION, YOU ARE REQUIRED TO REFRAIN FROM PROCEEDING FURTHER, IN WHICH CASE YOU ARE NOT PERMITTED TO DOWNLOAD, INSTALL OR USE THE SOFTWARE.



Note:

The terms and conditions of this Agreement were last updated, and are effective as of, the “Last Updated Date” indicated above. Any downloading, installation or Use of the Software or an earlier version of the Software that was licensed prior to such date is governed by prior terms and conditions, which differ from those set out in this Agreement.



1. GENERAL

1.1 Formation of legally binding contract.

A legally binding contract is immediately formed upon your acceptance of this Agreement. The licensee who is bound by this Agreement (“Licensee”) is: (i) the individual accepting this Agreement, if the individual is licensing the Software for his or her personal use or use as a sole proprietor; or (ii) the corporation, institution, partnership, organisation or other entity (“Organisation”) on whose behalf the individual accepting this Agreement is acting. Where a Transaction Confirmation is provided to Licensee, Licensee's name will be confirmed in the Transaction Confirmation. Where no Transaction Confirmation is provided to Licensee and Licensee obtains an Entitlement(s) or License(s) through the online process, Licensee's name will be the name provided as part of the online process or, where no name is provided, the name associated with the email address provided or used as part of the online process. The Entitlement(s) and License(s) issued to Licensee will be associated with such name. Licensee represents that the name provided to Phantom Technology, if any, is its full and correct legal name.



1.2 Application and priority of terms.

The terms and conditions of this Agreement apply regardless of, and take priority over, any terms and conditions applicable to Licensee's software or any third-party software which may be used in conjunction with the Software.



1.3 Communication of usage data.

Licensee acknowledges that the Software may communicate to Phantom Technology certain technical, non-personal information concerning Licensee's Use of the Software. In addition, User's names and email addresses may be communicated to Phantom Technology if the Software detects the Use of fraudulent Licences. Licensee hereby consents to such communication.



2. RIGHT TO USE

2.1 Grant of Rights.

Phantom Technology hereby grants to Licensee a limited, non-transferable, non-commercial, non-exclusive, non-sublicensable right for Authorised Users to install and Use the Software: (i) solely in runtime environment format; (ii) solely in the applicable Territory; (iii) solely for the Usage Purpose; and (iv) subject to the terms and conditions of the Non-Commercial Licence (as set out in Section 3).



2.2 Entitlements and Licences.

Licensee acknowledges that Use of the Software requires: (i) the issuance by Phantom Technology of an Entitlement(s); and (ii) the installation of such License(s) on the applicable Computer following the “Download” link provided by Phantom Technology. Phantom Technology will issue Licensee an Entitlement(s) to Non-Commercial Licence and permitted Use.



3. LICENCE AND SCOPE OF USE

3.1 Computer Installation.

Non-Commercial Licence provides for Computer Installation. “Computer Installation” means that the Licence may be installed on a dedicated Computer and the Software may only be Used on that Computer. The Software can be relocated (i.e. installed on a different Computer) on the strict condition that the relocated Computer is owned by Authorised Licensee.



3.2 Non-Concurrent Use.

Only one individual may Use the Software interactively (i.e. operating the Software with Third-Party software graphical user interface) on a Computer (including, for certainty, a Client Computer or a dedicated Computer) at any given time. Without limiting the foregoing in this Section, where the Software is being Used interactively on a Client Computer or a dedicated Computer by an individual, a second individual may not Use the Software on that same dedicated Computer or Client Computer indirectly through a separate Computer, terminal or monitor.



3.3 Open-Source Software.

The Software may be accompanied by certain open-source software (in source code and executable forms, as applicable) (the “Open-Source Software”) that works with the Software. The Open-Source Software forms, and is distributed as, a separate and independent software program from the Software (and the Software is not a modification of, or a work based on, the Open-Source Software), even though the Open-Source Software may have been aggregated or packaged with the Software for purposes of distribution. The Open-Source Software is distributed under and subject to the terms and conditions of the applicable open-source licences.

​

4. RESTRICTIONS ON USE

4.1 Restrictions on Use of Software.

Subject to the terms and conditions for Non-Commercial Licence, Licensee agrees that it will not, and will not permit any third party to, directly or indirectly:

(i) assign, transfer, lease, rent, sublicense, distribute or otherwise make available the Software, any Entitlement, any Licence or any right granted under this Agreement, in whole or in part, to any other Person, including on a timesharing, software-as-a-service or other similar basis;

(ii) permit any third party, other than an Authorised User, to redeem or un-redeem Entitlements, install Licences or Use the Software;

(iii) share any user ids or passwords with anyone other than Authorised Users;

(iv) Use the Software to provide any service bureau services or any services on a similar basis;

(v) Use the Software under a Non-Commercial Licence for Commercial purposes, or receive any form of compensation for work product created or work performed Using the Software under a Non-Commercial License;

(vi) reverse engineer, decompile, disassemble, or otherwise attempt to discover the source code of any portion of the Software;

(vii) Attempt to remove, obscure, alter or tamper with the Digital Watermark of Phantom Technology Logo or related Logo;

(viii) remove, obscure or alter any copyright, trade-mark, patent or proprietary notice affixed to the media or packaging of the Software or displayed by or in the Software;

(ix) disassemble, reverse engineer or use the file format of any file generated by the Software for purposes of by-passing any restrictions or requirements of the Software; or

(x) install or Use the Software in any way that would subject the Software, in whole in or in part, to governmental regulation that would not have otherwise applied but for such installation or Use.



4.2 Restrictions on Open-Source Software.

Licensee's use of the Open-Source Software is governed by the Open-Source Licences, as applicable.



5. OWNERSHIP AND RESERVATION OF RIGHTS

5.1 Ownership.

The Software is not sold; it is licensed to Licensee under the terms and conditions of this Agreement. Phantom Technology and its licensors are the owners of the Software, including all intellectual property rights (including trade secrets rights) relating thereto. No title to the Software or such rights is transferred to Licensee by this Agreement. All rights not expressly granted pursuant to this Agreement are reserved by Phantom Technology.



6. DISCLAIMER OF WARRANTIES AND LIMITATION OF LIABILITY

6.1 Warranty Disclaimer.

THE SOFTWARE IS PROVIDED “AS IS” WITHOUT WARRANTY OR CONDITION OF ANY KIND, EXPRESS OR IMPLIED, STATUTORY OR OTHERWISE IN LAW OR EQUITY, INCLUDING ANY IMPLIED WARRANTIES OR CONDITIONS OF TITLE, NON-INFRINGEMENT, MERCHANTABLE QUALITY, FITNESS FOR A PARTICULAR PURPOSE OR THOSE ARISING OTHERWISE FROM A COURSE OF DEALING OR USAGE OF TRADE, ALL OF WHICH ARE SPECIFICALLY DISCLAIMED TO THE MAXIMUM EXTENT PERMITTED BY LAW. WITHOUT LIMITING THE FOREGOING, PHANTOM TECHNOLOGY DOES NOT WARRANT THAT: (i) THE SOFTWARE WILL MEET LICENSEE'S NEEDS OR REQUIREMENTS; ii) THE SOFTWARE WILL RUN WITHOUT INTERRUPTION OR BE ERROR FREE; (iii) THE SOFTWARE IS IMPENETRABLE OR OTHERWISE MEETS ANY SECURITY STANDARDS; OR (iv) THE FUNCTIONS CONTAINED IN THE SOFTWARE WILL OPERATE IN ALL COMBINATIONS WHICH MAY BE SELECTED FOR USE BY LICENSEE. REFERENCES TO SOFTWARE INCLUDE THE OPEN-SOURCE SOFTWARE. PHANTOM TECHNOLOGY MAKES NO REPRESENTATIONS OR WARRANTIES, AND THERE ARE NO CONDITIONS, REGARDING THE OPEN-SOURCE SOFTWARE.



6.2 Limitation on Types of Recoverable Damages.

IN NO EVENT WILL PHANTOM TECHNOLOGY BE LIABLE TO LICENSEE OR ANY OTHER PERSON FOR ANY LOST PROFITS, LOST OR DAMAGED DATA, OR ANY SPECIAL, INCIDENTAL, INDIRECT, CONSEQUENTIAL OR PUNITIVE DAMAGES OF ANY KIND, WHETHER BASED ON BREACH OF CONTRACT OR WARRANTY, TORT (INCLUDING NEGLIGENCE), PRODUCT LIABILITY OR OTHERWISE, EVEN IF PHANTOM TECHNOLOGY IS INFORMED OR OTHERWISE HAS KNOWLEDGE OF THE POSSIBILITY OF SUCH DAMAGES AND EVEN IF SUCH DAMAGES WERE FORESEEABLE.



7. GOVERNING LAW

This Agreement and any related action or dispute shall be subject to English law and to the exclusive jurisdiction of the courts of England.