Version 0.6.0

+ Simplified, automated installation process.
+ Multiple general stability and performance improvements.
+ Multiple bug fixes and optimisations to Scanning.
+ Subtraction bug fixes and quality improvements.
+ General code reformatting and cleanup.
+ ObjectRecognition performance improvements.
+ ObjectRecognition now utilises events for access instead of singletons. 
+ ObjectRecognition is now included as an experimental feature.
+ Added template Scenes for authoring and testing.
+ Changed depth maps and depth-map processing code to use HDR texture formats.
+ Pathfinding stability improvements and minor code refactor.