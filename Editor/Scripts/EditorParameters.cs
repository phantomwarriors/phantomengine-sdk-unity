﻿using System.Collections;
using System.Collections.Generic;

using UnityEngine;

namespace PhantomTech.Interface {

    /// <summary>
    /// Parameters for easily customising elements of the PhantomEngine UI which utilise Unity's Editor.
    /// </summary>
    [CreateAssetMenu(fileName = "Editor Parameters", menuName = "New Editor Parameters")]
    public sealed class EditorParameters : ScriptableObject {

        private static EditorParameters s_Instance;
        public static EditorParameters Instance {
            get {
                if (!s_Instance) {
                    s_Instance = Resources.Load("Editor Parameters") as EditorParameters;
                }

                return s_Instance;
            }
        }

        public void MarkDirty() {

               m_Colors.MarkDirty();
            m_Materials.MarkDirty();
            m_Gradients.MarkDirty();
                m_Logos.MarkDirty();
                m_Icons.MarkDirty();
                m_Fonts.MarkDirty();
          m_ORInterface.MarkDirty();
        }

        public bool TryGetElement<T>(string _name, out T _result) {

            bool success;

            var type = typeof(T);

            _result = default(T);

            if (type == typeof(Color)) {

                Color val;

                if (success = m_Colors.TryGetElement(_name, out val)) { _result = (T)System.Convert.ChangeType(val, type); }
            }
            else if (type == typeof(Material)) {

                Material val;

                if (success = m_Materials.TryGetElement(_name, out val)) { _result = (T)System.Convert.ChangeType(val, type); }
            }
            else if (type == typeof(Texture2D)) {

                Texture2D val;

                if      (success = m_Gradients.  TryGetElement(_name, out val)) { _result = (T)System.Convert.ChangeType(val, type); }
                else if (success = m_Logos.      TryGetElement(_name, out val)) { _result = (T)System.Convert.ChangeType(val, type); }
                else if (success = m_Icons.      TryGetElement(_name, out val)) { _result = (T)System.Convert.ChangeType(val, type); }
                else if (success = m_ORInterface.TryGetElement(_name, out val)) { _result = (T)System.Convert.ChangeType(val, type); }
            }
            else if (type == typeof(Font)) {
                
                Font val;

                if (success = m_Fonts.TryGetElement(_name, out val)) { _result = (T)System.Convert.ChangeType(val, type); }
            }
            else {
                success = false;
            }

            if (success == false) {
                PhantomTech.Diagnostics.Debug.Log(string.Format(
                    "ERROR (EditorParameters.cs [GetElement<T>(string)]): Object of type \"{0}\" and name \"{1}\" could not be found!",
                    type,
                    _name
                ));
            }

            return success;
        }

        private void OnValidate() {
            MarkDirty();
        }
        
        public interface ICategorisedList {
            string GetName();
            IList  GetElements();
        }

        [System.Serializable]
        public class UIElement<T> {

            [SerializeField] private string m_Name = "New Element";
            [SerializeField] private T m_Value;

            public string Name  { get { return m_Name;  } }
            public T      Value { get { return m_Value; } }

            public static implicit operator T(UIElement<T> _element)  => _element.Value;
        }
        
        [System.Serializable]
        public class UICategory<T> : ICategorisedList {

            private readonly string m_Name;
            [SerializeField] private UIElement<T>[] m_Elements;
            
            private Dictionary<string, UIElement<T>> m_Cache;
            
            public string GetName()     { return m_Name;     }
            public IList  GetElements() { return m_Elements; }

            public bool TryGetElement(string _name, out T _result) {

                bool success;

                if (!AssertCache()) {
                    RebuildCache(); 
                }

                if (success = m_Cache.TryGetValue(_name, out var item)) {
                    _result = item;
                }
                else {
                    _result = default(T);
                }

                return success;
            }

            public UICategory(string _name) {
                m_Name = _name;
            }

            public void MarkDirty() {
                m_Cache = null;
            }

            private bool AssertCache() {
                return m_Cache != null;
            }

            private void RebuildCache() {
                m_Cache = new Dictionary<string, UIElement<T>>();

                foreach (var item in m_Elements) {
                    m_Cache.Add(item.Name, item);
                }
            }
        }

        [SerializeField] private UICategory<Color>     m_Colors       = new UICategory<Color>     ("Colors");
        [SerializeField] private UICategory<Material>  m_Materials    = new UICategory<Material>  ("Materials");
        [SerializeField] private UICategory<Texture2D> m_Gradients    = new UICategory<Texture2D> ("Gradients");
        [SerializeField] private UICategory<Texture2D> m_Logos        = new UICategory<Texture2D> ("Logos");
        [SerializeField] private UICategory<Texture2D> m_Icons        = new UICategory<Texture2D> ("Icons");
        [SerializeField] private UICategory<Texture2D> m_ORInterface  = new UICategory<Texture2D> ("ORInterface");
        [SerializeField] private UICategory<Font>      m_Fonts        = new UICategory<Font>      ("Fonts");
    }
}