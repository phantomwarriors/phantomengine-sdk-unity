﻿#if UNITY_EDITOR

using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;

using System.IO;
using System.Linq;
using System.Diagnostics;

using static PhantomTech.NativeUtilities;

namespace PhantomTech.Interface { 
    
    /// <summary>
    /// Script implementing a custom console window for PhantomTech.Diagnostics functionality.
    /// </summary>
    public sealed class PConsole : EditorWindow {

        /* PRIVATE */

        private static GUIStyle s_LogStyle;

        private static Vector2 s_ScrollPosition;
        private static Rect    s_ScrollView;

        private static int s_ActiveElement = -1;

        private static string s_SearchQuery = string.Empty;

        private static bool s_Collapse = false;
        private static bool s_ShowMessage = true;
        private static bool s_ShowWarning = true;
        private static bool s_ShowError   = true;

        private static bool s_ApplicationPlayingLastFrame = false;

        [MenuItem("Phantom Technology/PhantomEngine/Console")]
        private static void Init() {

            // Get existing open window or if none, make a new one:
            var window = EditorWindow.GetWindow(typeof(PConsole)) as PConsole;
            window.titleContent.text = "PhantomEngine Console";
            window.Show();
        }

        private void Start() {
            s_SearchQuery = string.Empty;
        }

        private void Update() {

            if (Application.isPlaying && !s_ApplicationPlayingLastFrame) {
                Start();
            }

            Repaint();

            s_ApplicationPlayingLastFrame = Application.isPlaying;
        }

        private void OnGUI() {
            
            if (s_LogStyle == null) {
                s_LogStyle = new GUIStyle();
                s_LogStyle.hover.background = EditorUtilities.Instance.m_GradientSelection;
                s_LogStyle.normal.textColor = Color.white;
                s_LogStyle.alignment = TextAnchor.MiddleLeft;
                s_LogStyle.padding = new RectOffset(0, 100, 0, 0);
                s_LogStyle.wordWrap = true;
            }

            float barHeight = 20.0f;

            var buttonBar = new GUIStyle(EditorStyles.toolbarButton);
            buttonBar.fixedHeight = 20.0f;

            var buttonRect = new Rect(0, 0, 60.0f, barHeight);

            if (GUI.Button(buttonRect, "Clear", buttonBar)) {
                Diagnostics.Debug.Clear();
            }
            
            bool     resetScrollPosition = false;
            bool interruptScrollPosition = false;

            var pressed = new GUIStyle(buttonBar);
            pressed.normal.background = EditorUtilities.Instance.m_GradientSelection_Dark;

            buttonRect.x += buttonRect.width;
            buttonRect.width = 75.0f;

            if (s_Collapse) {
                if (GUI.Button(buttonRect, "Collapse", pressed))   { s_Collapse = false; resetScrollPosition = true; }
            }
            else {
                if (GUI.Button(buttonRect, "Collapse", buttonBar)) { s_Collapse = true; }
            }
            
            buttonRect.x += buttonRect.width;

            if (PScene.s_SimulateInEditor) {
                if (GUI.Button(buttonRect, "Simulate", pressed))   { PScene.s_SimulateInEditor = false; }
            }
            else {
                if (GUI.Button(buttonRect, "Simulate", buttonBar)) { PScene.s_SimulateInEditor = true; }
            }

            buttonRect.x += buttonRect.width;
            buttonRect.width = 90.0f;

            if (PScene.s_SyncEditorCamera) {
                if (GUI.Button(buttonRect, "Sync Camera", pressed))   { PScene.s_SyncEditorCamera = false; }
            }
            else {
                if (GUI.Button(buttonRect, "Sync Camera", buttonBar)) { PScene.s_SyncEditorCamera = true; }
            }

            buttonRect.x += buttonRect.width;
            buttonRect.width = Math.Max(position.width - 455.0f, 0.0f);

            if (buttonRect.width > 20.0f) {

                buttonRect.x += 4;
                buttonRect.y  = 3;

                s_SearchQuery = GUI.TextField(buttonRect, s_SearchQuery, EditorStyles.toolbarSearchField);
            }

            buttonRect.width = 50.0f;
            buttonRect.y = 0;
            buttonRect.x = position.width - buttonRect.width;

            if (s_ShowError) {
                if (GUI.Button(buttonRect, string.Empty, pressed))   { s_ShowError = false; }
            }
            else {
                if (GUI.Button(buttonRect, string.Empty, buttonBar)) { s_ShowError = true; }
            }

            const float symbolLeftOffset = 13.0f;

            var errorRect = new Rect(buttonRect);
            errorRect.width  *= 0.66f; 
            errorRect.height *= 0.66f;
            errorRect.x += (buttonRect.width  - errorRect.width)  * 0.5f - symbolLeftOffset;
            errorRect.y += (buttonRect.height - errorRect.height) * 0.5f;
            GUI.DrawTexture(errorRect, EditorUtilities.Instance.m_LogErrorIcon, ScaleMode.ScaleToFit);

            buttonRect.x -= buttonRect.width;

            if (s_ShowWarning) {
                if (GUI.Button(buttonRect, string.Empty, pressed))   { s_ShowWarning = false; }
            }
            else {
                if (GUI.Button(buttonRect, string.Empty, buttonBar)) { s_ShowWarning = true; }
            }

            var warningRect = new Rect(buttonRect);
            warningRect.width  *= 0.66f; 
            warningRect.height *= 0.66f;
            warningRect.x += (buttonRect.width  - warningRect.width)  * 0.5f - symbolLeftOffset;
            warningRect.y += (buttonRect.height - warningRect.height) * 0.5f;
            GUI.DrawTexture(warningRect, EditorUtilities.Instance.m_LogWarningIcon, ScaleMode.ScaleToFit);

            buttonRect.x -= buttonRect.width;

            if (s_ShowMessage) {
                if (GUI.Button(buttonRect, string.Empty, pressed))   { s_ShowMessage = false; }
            }
            else {
                if (GUI.Button(buttonRect, string.Empty, buttonBar)) { s_ShowMessage = true; }
            }

            var messageRect = new Rect(buttonRect);
            messageRect.width  *= 0.66f; 
            messageRect.height *= 0.66f;
            messageRect.x += (buttonRect.width  - messageRect.width)  * 0.5f - symbolLeftOffset;
            messageRect.y += (buttonRect.height - messageRect.height) * 0.5f;
            GUI.DrawTexture(messageRect, EditorUtilities.Instance.m_LogMessageIcon, ScaleMode.ScaleToFit);

            List<KeyValuePair<Diagnostics.Debug.LogArgs, RefInt>> entries;

            int max = 0;

            if (s_Collapse) {
                var logs = new Dictionary<Diagnostics.Debug.LogArgs, RefInt>();

                var unsortedLogs = Diagnostics.Debug.GetLogs();
                for (int i = 0; i < unsortedLogs.Length; i++) {

                    var unsortedLog = unsortedLogs[i];

                    if (logs.TryGetValue(unsortedLog, out RefInt count)) {
                        count.m_Val += 1;

                        if (count.m_Val > max) {
                            max = count.m_Val;
                        }
                    }
                    else {
                        logs.Add(unsortedLog, new RefInt(1));
                    }
                }

                entries = logs.ToList();
            }
            else {
                var logs = new List<KeyValuePair<Diagnostics.Debug.LogArgs, RefInt>>();

                var unsortedLogs = Diagnostics.Debug.GetLogs();
                for (int i = 0; i < unsortedLogs.Length; i++) {

                    var unsortedLog = unsortedLogs[i];

                    logs.Add(new KeyValuePair<Diagnostics.Debug.LogArgs, RefInt>(unsortedLog, new RefInt(1)));
                }

                entries = logs;
            }

            int messageCount = 0, warningCount = 0, errorCount = 0;

            for (int i = 0; i < entries.Count; ) {

                var log   = entries[i].Key;
                var count = entries[i].Value.m_Val;

                var type = log.m_LogType;

                bool shouldRemove = false;

                if (type == Diagnostics.Debug.LogType.Message) {
                    shouldRemove = !s_ShowMessage;
                    messageCount += count;
                }
                else if (type == Diagnostics.Debug.LogType.Warning) { 
                    shouldRemove = !s_ShowWarning;
                    warningCount += count;
                }
                else if (type == Diagnostics.Debug.LogType.Error  ) { 
                    shouldRemove = !s_ShowError;
                    errorCount += count;
                }
                    
                if (!shouldRemove && string.IsNullOrEmpty(s_SearchQuery) == false) {
                    shouldRemove = log.m_Message.ToString().Contains(s_SearchQuery) == false;
                }

                if (shouldRemove) {
                    entries.RemoveAt(i);
                }
                else {
                    i++;
                }
            }

            var labelStyle = new GUIStyle(EditorStyles.boldLabel);
            labelStyle.alignment = TextAnchor.MiddleCenter;

            const float countTextOffset = 9.5f;

            messageRect.x += symbolLeftOffset + countTextOffset;
            warningRect.x += symbolLeftOffset + countTextOffset;
              errorRect.x += symbolLeftOffset + countTextOffset;

            string messageCountText;
            string warningCountText;
            string   errorCountText;

            if (messageCount > 999) {
                messageCountText = "999+";
            }
            else {
                messageCountText = messageCount.ToString();
            }

            if (warningCount > 999) {
                warningCountText = "999+";
            }
            else {
                warningCountText = warningCount.ToString();
            }

            if (errorCount > 999) {
                errorCountText = "999+";
            }
            else {
                errorCountText = errorCount.ToString();
            }

            GUI.Label(messageRect, messageCountText, labelStyle);
            GUI.Label(warningRect, warningCountText, labelStyle);
            GUI.Label(  errorRect,   errorCountText, labelStyle);

            var scrollViewRect = new Rect(0, barHeight, position.width, position.height - barHeight);
            s_ScrollPosition = GUI.BeginScrollView(scrollViewRect, s_ScrollPosition, s_ScrollView);
            s_ScrollView.height = 0;
            
            const float padding = 5.0f;

            float symbolWidth = Math.Clamp(20.0f, 0, s_LogStyle.CalcHeight(GUIContent.none, position.width));

            s_LogStyle.contentOffset = new Vector2(symbolWidth + padding, 0);
            GUIStyle style = new GUIStyle(s_LogStyle);

            int index = 0;

            float top = 0.0f, bottom = 0.0f, gap = 0.0f, lastHeight = 0.0f;

            try {

                foreach (var kvp in entries) {

                    top    = s_ScrollPosition.y;
                    bottom = (scrollViewRect.height + (s_ScrollPosition.y - s_ScrollView.height)) + s_ScrollView.height - scrollViewRect.y;

                    var log = kvp.Key;

                    bool isActive = s_ActiveElement == index;

                    bool shouldOcclude =
                        s_ScrollView.height + lastHeight < top ||
                        s_ScrollView.height              > bottom;

                    GUIContent content;

                    if (!isActive && shouldOcclude) {

                        lastHeight = style.CalcHeight(GUIContent.none, scrollViewRect.width);

                        s_ScrollView.height += lastHeight;
                                        gap += lastHeight;
                    }
                    else {

                        var count = kvp.Value;

                        Texture2D icon;

                        switch (log.m_LogType) {
                            case Diagnostics.Debug.LogType.Message: { icon = EditorUtilities.Instance.m_LogMessageIcon; break; }
                            case Diagnostics.Debug.LogType.Warning: { icon = EditorUtilities.Instance.m_LogWarningIcon; break; }
                            case Diagnostics.Debug.LogType.Error:   { icon = EditorUtilities.Instance.m_LogErrorIcon;   break; }
                            default: { 
                                icon = EditorUtilities.Instance.m_LogMessageIcon; break;
                            }
                        }

                        string logText = string.Empty; 

                        if (s_Collapse) {
                            if (count.m_Val > 999) {
                                logText += "999+\t";
                            }
                            else {
                                logText += count.m_Val.ToString() + "\t";
                            }
                        }
                
                        logText += "[" + log.m_Timestamp.ToLongTimeString() + "] " + log.m_Message.ToString();

                        if (isActive) {
                            logText += "\n" + log.m_StackTrace.ToString();
                        }

                        content = new GUIContent(logText);

                        lastHeight = style.CalcHeight(content, scrollViewRect.width);
                        s_ScrollView.height += lastHeight;

                        var fileButtonWidth = 65.0f;

                        var rect = GUILayoutUtility.GetRect(content, style);
                        rect.y += gap;

                        var buttonStyle = new GUIStyle(EditorStyles.miniButton);
                        buttonStyle.fixedHeight = 20.0f;

                        var fileButtonRect = new Rect(
                            rect.width - (fileButtonWidth + padding),
                            rect.y + padding,
                            fileButtonWidth,
                            buttonStyle.fixedHeight
                        );

                        bool fileButton = default;

                        if (isActive) {
                            fileButton = GUI.Button(fileButtonRect, "Open File", buttonStyle);
                        }

                        bool logButton = GUI.Button(rect, content.text, s_LogStyle);

                        if (isActive) {
                            if (!fileButton) {
                                fileButton = GUI.Button(fileButtonRect, "Open File", buttonStyle);
                            }
                        }

                        GUI.DrawTexture(
                            new Rect(rect.x, rect.y, symbolWidth, symbolWidth),
                            icon,
                            ScaleMode.ScaleToFit
                        );

                        if (fileButton) {
                            OpenFile(log.m_StackTrace);
                        }
                        else if (logButton) {
                            s_ActiveElement = s_ActiveElement == index ? -1 : index;

                            interruptScrollPosition = true;
                        }
                    }

                    index++;
                }
            }
            catch { }

            if (s_ScrollView.height > scrollViewRect.height && !interruptScrollPosition) {

                var scrollDelta = Math.Abs(bottom - (s_ScrollView.height - scrollViewRect.y));

                if (scrollDelta < 60.0f || resetScrollPosition) {
                    s_ScrollPosition.y = (((s_ScrollView.height) - top) + gap) - scrollViewRect.height;
                }
            }

            GUI.EndScrollView();
        }

        private void OpenFile(StackTrace _stackTrace) { 
        
            try {
                var frame      = _stackTrace.GetFrame(0);
                var dataPath   = Path.GetFullPath(Application.dataPath);
                var systemPath = Path.GetFullPath(frame.GetFileName());

                var assetPath  = systemPath.Replace(dataPath, "Assets");

                AssetDatabase.OpenAsset(AssetDatabase.LoadMainAssetAtPath(assetPath), frame.GetFileLineNumber());
            }
            catch (System.Exception _e) {
                UnityEngine.Debug.LogError(_e);
            }
        }
    }
}

#endif