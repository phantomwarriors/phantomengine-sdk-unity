//#if UNITY_EDITOR

using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace PhantomTech.Interface {

    /// <summary>
    /// A singleton database that loads the specified references of items from within the EditorParameters instance as Unity assets.
    /// </summary>
    [CreateAssetMenu(fileName = "Editor Utilities", menuName = "New Editor Utilities")]
    public sealed class EditorUtilities : ScriptableObject {

        /* SINGLETON INSTANCE */
        private static EditorUtilities s_Instance;
        public static EditorUtilities Instance {
            get {
                if (s_Instance == null) {
                    s_Instance = Resources.Load("Editor Utilities") as EditorUtilities;
                    s_Instance.Init();
                }

                return s_Instance;
            }
        }

        /* PUBLIC */
        private GUIStyle s_HeaderStyle;
        public GUIStyle HeaderStyle {
            get {
                if (s_HeaderStyle == null) {
                    throw new System.NotImplementedException();
                }

                return s_HeaderStyle;
            }
        }

        [Header("Materials")]
        public string m_Material_Transparent;
        [HideInInspector] public Material m_Transparent;

        [Header("Fonts")]
        public string m_Font_Title;
        [HideInInspector] public Font m_Title;
        public string m_Font_Header;
        [HideInInspector] public Font m_Header;
        public string m_Font_Body;
        [HideInInspector] public Font m_Body;
        public string m_Font_Link;
        [HideInInspector] public Font m_Link;

        [Header("Gradients")]
        public string m_Texture_GradientPurple;
        [HideInInspector] public Texture2D m_GradientPurple;
        public string m_Texture_GradientSilver;
        [HideInInspector] public Texture2D m_GradientSilver;
        public string m_Texture_GradientSpace;
        [HideInInspector] public Texture2D m_GradientSpace;
        public string m_Texture_GradientSelection;
        [HideInInspector] public Texture2D m_GradientSelection;
        public string m_Texture_GradientSelection_Dark;
        [HideInInspector] public Texture2D m_GradientSelection_Dark;

        [Header("Logos")]
        public string m_Texture_PhantomEngine_Light;
        [HideInInspector] public Texture2D m_PhantomEngine_Light;
        public string m_Texture_PhantomEngine_Full;
        [HideInInspector] public Texture2D m_PhantomEngine_Full;
        public string m_Texture_PhantomTech;
        [HideInInspector] public Texture2D m_PhantomTech;

        [Header("Icons")]
        public string m_Texture_InstallIcon;
        [HideInInspector] public Texture2D m_InstallIcon;
        public string m_Texture_InstallIcon_Highlighted;
        [HideInInspector] public Texture2D m_InstallIcon_Highlighted;
        public string m_Texture_DocumentIcon;
        [HideInInspector] public Texture2D m_DocumentIcon;
        public string m_Texture_LogMessageIcon;
        [HideInInspector] public Texture2D m_LogMessageIcon;
        public string m_Texture_LogWarningIcon;
        [HideInInspector] public Texture2D m_LogWarningIcon;
        public string m_Texture_LogErrorIcon;
        [HideInInspector] public Texture2D m_LogErrorIcon;

        [Header("Colors")]
        public string m_Color_DefaultTextColor;
        [HideInInspector] public Color m_DefaultTextColor;

        [Header("ORInterface")]
        public string m_Texture_BoundingBox;
        [HideInInspector] public Texture2D m_BoundingBox;
        public string m_Texture_BoundingBoxLabel;
        [HideInInspector] public Texture2D m_BoundingBoxLabel;

        void Init() {

            var instance = EditorParameters.Instance;
            
            /* MATERIALS */
            instance.TryGetElement(m_Material_Transparent, out m_Transparent);

            /* FONTS */
            instance.TryGetElement(m_Font_Title,  out m_Title);
            instance.TryGetElement(m_Font_Header, out m_Header);
            instance.TryGetElement(m_Font_Body,   out m_Body);
            instance.TryGetElement(m_Font_Link,   out m_Link);

            /* GRADIENTS */
            instance.TryGetElement(m_Texture_GradientPurple,         out m_GradientPurple);
            instance.TryGetElement(m_Texture_GradientSilver,         out m_GradientSilver);
            instance.TryGetElement(m_Texture_GradientSpace,          out m_GradientSpace);
            instance.TryGetElement(m_Texture_GradientSelection,      out m_GradientSelection);
            instance.TryGetElement(m_Texture_GradientSelection_Dark, out m_GradientSelection_Dark);

            /* LOGOS */
            instance.TryGetElement(m_Texture_PhantomEngine_Light, out m_PhantomEngine_Light);
            instance.TryGetElement(m_Texture_PhantomEngine_Full,  out m_PhantomEngine_Full);
            instance.TryGetElement(m_Texture_PhantomTech,         out m_PhantomTech);

            /* ICONS */
            instance.TryGetElement(m_Texture_InstallIcon,  out m_InstallIcon);
            instance.TryGetElement(m_Texture_InstallIcon_Highlighted,  out m_InstallIcon_Highlighted);
            instance.TryGetElement(m_Texture_DocumentIcon, out m_DocumentIcon);

            instance.TryGetElement(m_Texture_LogMessageIcon, out m_LogMessageIcon);
            instance.TryGetElement(m_Texture_LogWarningIcon, out m_LogWarningIcon);
            instance.TryGetElement(m_Texture_LogErrorIcon,   out m_LogErrorIcon);

            /* COLORS */
            instance.TryGetElement(m_Color_DefaultTextColor, out m_DefaultTextColor);

            /* ORINTERFACE */
            instance.TryGetElement(m_Texture_BoundingBox,      out m_BoundingBox);
            instance.TryGetElement(m_Texture_BoundingBoxLabel, out m_BoundingBoxLabel);
        }

        public static void MarkDirty() {
            Instance.Init();
        }
    }
}

//#endif