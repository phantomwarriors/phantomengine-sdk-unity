#if UNITY_EDITOR

using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;

using System.Linq;
using System.Reflection;

namespace PhantomTech.Interface {

    /// <summary>
    /// Implements a custom Unity inspector for PhantomTech.Interface.EditorParameters script.
    /// </summary>
    [CustomEditor(typeof(EditorParameters))]
    public sealed class EditorParametersEditor : Editor {

        public override void OnInspectorGUI() {

            try {
                var t = target as EditorParameters;

                EditorGUI.indentLevel--;

                EditorGUILayout.LabelField("Elements", EditorStyles.boldLabel);

                var categories = t.GetType().GetFields(BindingFlags.Instance | BindingFlags.NonPublic).Where(
                    field => field.FieldType.GetInterfaces().Contains(typeof(EditorParameters.ICategorisedList))
                );

                EditorGUI.indentLevel += 2;

                foreach (var i in categories) {

                    var property = serializedObject.FindProperty(i.Name);

                    var elements = i.FieldType.GetFields(BindingFlags.Instance | BindingFlags.NonPublic).Where(
                        field => field.FieldType.GetInterfaces().Contains(typeof(IList))
                    );

                    foreach (var j in elements) {

                        EditorGUILayout.PropertyField(
                            property.FindPropertyRelative(j.Name), 
                            new GUIContent() {
                                text = property.displayName
                            }
                        );
                    }
                }

                EditorGUI.indentLevel--;

                EditorUtility.SetDirty(t);
            }
            catch (System.Exception e) {
                EditorGUILayout.HelpBox(e.ToString(), MessageType.Error);   
            }
        }
    }
}

#endif