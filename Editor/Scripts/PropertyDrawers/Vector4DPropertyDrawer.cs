﻿#if UNITY_EDITOR

using System.Collections;
using System.Collections.Generic;

using UnityEngine;
using UnityEditor;

namespace PhantomTech.Interface {

    /// <summary>
    /// Implements a custom Unity Property Drawer for the PhantomTech.Vector4D type.
    /// </summary>
    [CustomPropertyDrawer(typeof(PhantomTech.Vector4D))]
    public sealed class Vector4DPropertyDrawer : PropertyDrawer {

        public override void OnGUI(Rect position, SerializedProperty property, GUIContent label) {

            EditorGUI.BeginProperty(position, label, property);

            // Get the X, Y, Z, and W values of the array.
            SerializedProperty x = property.FindPropertyRelative("x");
            SerializedProperty y = property.FindPropertyRelative("y");
            SerializedProperty z = property.FindPropertyRelative("z");
            SerializedProperty w = property.FindPropertyRelative("w");

            // Represent the 4D PhantomTech.Vector4D object as a 4D UnityEngine.Vector4:
            var val = EditorGUI.Vector4Field(position, label, new Vector4(x.floatValue, y.floatValue, z.floatValue, w.floatValue));

            // Assign the values from our input to the object:
            x.floatValue = val.x;
            y.floatValue = val.y;
            z.floatValue = val.z;
            w.floatValue = val.w;

            EditorGUI.EndProperty();

            // Tells Unity to reserialise the changes we made to the object's SerialisedProperties.
            property.serializedObject.ApplyModifiedProperties();
        }
    }
}

#endif