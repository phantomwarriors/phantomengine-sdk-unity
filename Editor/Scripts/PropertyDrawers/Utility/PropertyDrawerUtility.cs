﻿#if UNITY_EDITOR

using System.Collections;
using System.Collections.Generic;

using UnityEngine;
using UnityEditor;

using System;
using System.Linq;
using System.Reflection;
using System.Text.RegularExpressions;
using System.Runtime.CompilerServices;

namespace PhantomTech.Interface {

    /// <summary>
    /// Static class providing utilities for implementing custom Unity Property Drawers.
    /// </summary>
    public static class PropertyDrawerUtility {

        /// <summary>
        /// Attempts to retrieve an instance of the target type using the provided FieldInfo and SerializedProperty.
        /// </summary>
        /// <typeparam name="T">Type to return result as.</typeparam>
        /// <param name="_fieldInfo">Reflection field info for the type.</param>
        /// <param name="_property">SerializedProperty representing the type.</param>
        /// <param name="_result">Result out value.</param>
        /// <returns>True if successful. False otherwise.</returns>
        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        public static bool TryGetTarget<T>(FieldInfo _fieldInfo, SerializedProperty _property, out T _result) where T : class {

            _result = default(T);

            object obj = GetFieldNested<T>(_fieldInfo, _property);

            if (obj != null) {

                if (obj.GetType().IsArray) {
                    var index = Convert.ToInt32(new string(_property.propertyPath.Where(c => char.IsDigit(c)).ToArray()));

                    _result = ((T[])obj)[index];
                }
                else {
                    _result = obj as T;
                }
            }

            return obj != null;
        }

        /// <summary>
        /// Tries to find a SerializedProperty belonging to a SerializedObject by name.
        /// </summary>
        /// <param name="_name">Name of the property.</param>
        /// <param name="_serializedObject">Reference to parent.</param>
        /// <param name="_result">Result out value.</param>
        /// <returns>True if successful. False otherwise.</returns>
        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        public static bool TryFindProperty(string _name, SerializedObject _serializedObject, out SerializedProperty _result) {

            // Try to get the property.
            _result = _serializedObject.FindProperty(_name);

            // If the property is null...
            if (_result == null) {

                // Update the object it belongs to and try again.
                _serializedObject.Update();
                _result = _serializedObject.FindProperty(_name);
            }

            return _result != null;
        }

        /// <summary>
        /// Tries to find a SerializedProperty relative to a parent SerializedProperty by name.
        /// </summary>
        /// <param name="_name">Name of the target property.</param>
        /// <param name="_serializedProperty">Reference to parent.</param>
        /// <param name="_result">Result out value.</param>
        /// <returns>True if successful. False otherwise.</returns>
        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        public static bool TryFindPropertyRelative(string _name, SerializedProperty _serializedProperty, out SerializedProperty _result) {

            // Try to get the property.
            _result = _serializedProperty.FindPropertyRelative(_name);

            // If the property is null...
            if (_result == null) {

                // Update the object it belongs to and try again.
                _serializedProperty.serializedObject.Update();
                _result = _serializedProperty.FindPropertyRelative(_name);
            }

            return _result != null;
        }

        /// <summary>
        /// Attempts to retrieve a SerializedProperty from a parent by index.
        /// </summary>
        /// <param name="_index">Index of the property.</param>
        /// <param name="_serializedProperty">Reference to parent.</param>
        /// <param name="_result">Result out value.</param>
        /// <returns>True if successful. False otherwise.</returns>
        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        public static bool TryGetArrayElementAtIndex(int _index, SerializedProperty _serializedProperty, out SerializedProperty _result) {

            // Try to get the property.
            _result = _serializedProperty.GetArrayElementAtIndex(_index);

            // If the property is null...
            if (_result == null) {

                // Update the object it belongs to and try again.
                _serializedProperty.serializedObject.Update();
                _result = _serializedProperty.GetArrayElementAtIndex(_index);
            }

            return _result != null;
        }

        /// <summary>
        /// Attempts to retrieve an item stored within a SerializedProperty that represents an enumerable collection.
        /// </summary>
        /// <typeparam name="T">Type to return result as.</typeparam>
        /// <param name="_fieldInfo">Reflection field info for the type the property represents.</param>
        /// <param name="_property">SerializedProperty representing enumerable collection.</param>
        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        public static T GetFieldNested<T>(FieldInfo _fieldInfo, SerializedProperty _property) where T : class {

            object obj = null;

            var fullPath = _property.propertyPath.Split('.');

            // If the path to the field is longer than 1 then it is nested...
            if (fullPath.Length > 1) {

                // Get the path to the field's parent.
                var parentPath = string.Empty;

                for (int i = 0; i < fullPath.Length - 1; i++) {
                    parentPath += fullPath[i];

                    if (i != fullPath.Length - 2) {
                        parentPath += ".";
                    }
                }

                // Remove the "Array" string from the end of the parent's path as collections need to be handled differently from fields.
                bool isArray = false;
                if (Regex.IsMatch(parentPath, @"\.Array$")) {
                    parentPath = Regex.Replace(parentPath, @"\.Array$", string.Empty);

                    isArray = true;
                }
                else if (Regex.IsMatch(parentPath, @"\.Array\.data\[\d+\]$")) {
                    parentPath = Regex.Replace(parentPath, @"\.Array\.data\[\d+\]$", string.Empty);

                    isArray = true;
                }

                // Get the value of the parent object.
                var parentField = _property.serializedObject.targetObject.GetType().GetField(parentPath);
                var parentValue = parentField.GetValue(_property.serializedObject.targetObject);

                // Handle the value differently if it is within an array:
                if (isArray) {

                    // Get the collection the element belongs to:
                    var collection = parentValue as IEnumerable<object>;

                    // Determine if the collection is empty or not (If it's empty just do nothing).
                    if (collection.Count() != 0) {

                        // Get the last two directories of the path.
                        var targetPath = fullPath[fullPath.Length - 2] + "." + fullPath[fullPath.Length - 1];

                        // Get the index of the element
                        int index = int.Parse(Regex.Match(targetPath, @"\[\d+\]").Value.TrimEnd(']').TrimStart('['));

                        var element = collection.ElementAt(index);

                        // If the target value is a direct element of the array:
                        if (Regex.Match(targetPath, @"\[\d+\]$").Success) {
                            obj = element;
                        }

                        // Otherwise if the target is nested within an element of the array:
                        else {
                            var elementField = element.GetType().GetField(fullPath[fullPath.Length - 1]);
                            obj = elementField.GetValue(element);
                        }
                    }
                }
                else {

                    // Find the target field from the value of the parent.
                    var targetField = parentValue.GetType().GetField(fullPath[fullPath.Length - 1]);
                    obj = targetField.GetValue(parentValue);
                }
            }

            // If the path to the field is less than 1 then the field isn't nested.
            else {

                // Attempt to get the target field from the provided FieldInfo.
                try {
                    obj = _fieldInfo.GetValue(_property.serializedObject.targetObject);
                }
                catch {

                    // Try again but this time get the FieldInfo ourselves.
                    try {
                        var field = _property.serializedObject.targetObject.GetType().GetField(_property.name);

                        obj = field.GetValue(_property.serializedObject.targetObject);
                    }

                    // Last few attempts failed. Throw an exception.
                    catch (System.Exception e) {
                        throw e;
                    }
                }
            }

            return obj as T;
        }
    } 
}

#endif