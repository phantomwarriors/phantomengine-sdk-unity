﻿#if UNITY_EDITOR

using System.Collections;
using System.Collections.Generic;

using UnityEngine;
using UnityEditor;

namespace PhantomTech {
    
    [CustomPropertyDrawer(typeof(PhantomTech.Vector3DInt))]
    public class Vector3DIntPropertyDrawer : PropertyDrawer {

        public override void OnGUI(Rect position, SerializedProperty property, GUIContent label) {

            EditorGUI.BeginProperty(position, label, property);

            // Get the X, Y, Z, and W values of the array.
            SerializedProperty x = property.FindPropertyRelative("x");
            SerializedProperty y = property.FindPropertyRelative("y");
            SerializedProperty z = property.FindPropertyRelative("z");

            // Represent the 3D PhantomTech.Vector3D object as a 3D UnityEngine.Vector3:
            UnityEngine.Vector3Int val = EditorGUI.Vector3IntField(position, label, new UnityEngine.Vector3Int(x.intValue, y.intValue, z.intValue));

            // Assign the values from our input to the object:
            x.intValue = val.x;
            y.intValue = val.y;
            z.intValue = val.z;

            EditorGUI.EndProperty();

            // Tells Unity to reserialise the changes we made to the object's SerialisedProperties.
            property.serializedObject.ApplyModifiedProperties();
        }

    }

}


#endif